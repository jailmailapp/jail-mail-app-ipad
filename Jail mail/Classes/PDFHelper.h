//
//  PDFHelper.h
//  PDFDemo
//
//  Created by Friendlydeveloper on 29.07.11.
//  Copyright 2011 www.codingsessions.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class JMPackage;

#define kDefLetterName @"letter.pdf"
#define kDefContactName @"contact.pdf"

@interface PDFHelper : NSObject

- (NSString *)defaltPathWithFileName:(NSString *)saveFileName;

- (NSInteger)createLetterFromLetter:(JMPackage *)letter withFileName:(NSString *)fileName;

- (void)createContactFromLetter:(JMPackage *)letter withFileName:(NSString *)fileName;

@end