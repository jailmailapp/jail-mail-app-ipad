//
//  JMSendersDelegate.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JMSendersDelegate <NSObject>

- (void)selectedSender:(id)sender;

@end
