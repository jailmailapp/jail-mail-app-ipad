//
//  JMTermsOfServiceController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMTermsOfServiceController.h"

#import "UIViewController+SideMenu.h"

#import "JMSendingController.h"

@interface JMTermsOfServiceController ()

@property (strong, nonatomic) IBOutlet UIWebView *informationWeb;

@end

@implementation JMTermsOfServiceController

+ (NSString *)storyboardID
{
    return @"idTermsOfServiceController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Information", @"Information");
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_check"]
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(doneTap:)];
    self.navigationItem.rightBarButtonItem = sendButton;
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"TermsOfService" ofType:@"html"]];
    [self loadHTMLFromUrl:url];
}


- (void)loadHTMLFromUrl:(NSURL *)url
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.informationWeb loadRequest:urlRequest];
}


- (void)doneTap:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"kTermsOfService"];
    [defaults synchronize];
    
    JMSendingController *sendingController = [JMSendingController controller];
    [sendingController newLetterPackage:_letterPackage];
    [self.navigationController pushViewController:sendingController animated:YES];
}


@end