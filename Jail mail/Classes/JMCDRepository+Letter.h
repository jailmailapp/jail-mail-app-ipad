//
//  JMCDRepository+Letter.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"

#import "JMLetter.h"

@interface JMCDRepository (Letter)

- (NSArray *)allLetters;

- (NSInteger)countMassages;

- (NSInteger)countPhotos;

@end
