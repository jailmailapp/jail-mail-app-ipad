//
//  UIImage+Save.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Save)

- (NSString *)saveWithFileName:(NSString *)fileName;
- (BOOL)deleteWithFileName:(NSString *)fileName;

- (BOOL)saveToFile:(NSString *)path;

@end
