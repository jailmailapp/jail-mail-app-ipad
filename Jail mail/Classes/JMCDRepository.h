//
//  CDRepository.h
//  Jail mail
//
//  Created by Alexander on 29.03.15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMAbstractRepository.h"

#import "JMCoreDataManager.h"

@interface JMCDRepository : JMAbstractRepository

- (JMCoreDataManager *)manager;

@end