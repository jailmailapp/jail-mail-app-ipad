//
//  DataManager.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCoreDataManager.h"

#import "JMState.h"

@interface JMCoreDataManager ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;

@end

@implementation JMCoreDataManager

#pragma mark - Core Data stack

@synthesize managedObjectContext       = _managedObjectContext;
@synthesize managedObjectModel         = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (id)sharedManager
{
    static JMCoreDataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


- (id)init
{
    if (self = [super init])
    {
        
        [self preload];
    }
    
    return self;
}


- (void)dealloc
{
    // Should never be called, but just here for clarity really.
}


#pragma mark -

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark -

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)
    {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"JailmailModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL         = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"JailmailModel.sqlite"];
    NSError *error          = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL result = [userDefaults boolForKey:@"CoreDataUpdated_2"];
    
    if (result == NO) {
        [[NSFileManager defaultManager] removeItemAtPath:storeURL.path error:&error];
        
        [userDefaults setBool:YES forKey:@"CoreDataUpdated_2"];
        [userDefaults synchronize];
    }
    
    
    NSLog(@"%@",error);
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey]        = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey]             = error;
        error                                  = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (!coordinator)
    {
        return nil;
    }
    
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}


- (NSManagedObjectContext *)context
{
    return self.managedObjectContext;
}


- (NSPersistentStoreCoordinator *)persistent
{
    return self.persistentStoreCoordinator;
}


#pragma mark - Core Data Saving support

- (NSError *)saveContext
{
    __block NSError *error = nil;
    
    if (self.managedObjectContext != nil)
    {
        [self.managedObjectContext performBlockAndWait:^{
            if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error])
            {
                NSLog (@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }];
    }
    else {
        error = [[NSError alloc] initWithDomain:@"com.yanpix.coreDataError" code:1000 userInfo:nil];
    }
    
    return error;
}


- (void)rollback
{
    [self.managedObjectContext rollback];
}


- (void)preload
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMState entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.context executeFetchRequest:request error:&error];
    
    if (result == nil || result.count == 0)
    {
        NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"State" ofType:@"plist"];
        
        NSArray *state = [[NSArray alloc] initWithContentsOfFile:dataPath];
        NSLog(@"Imported state: %@", state);
        
        [state enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSEntityDescription *entity = [NSEntityDescription entityForName:[JMState entityName] inManagedObjectContext:self.context];
            JMState *newEntity           = [[JMState alloc] initWithEntity:entity insertIntoManagedObjectContext:self.context];
            newEntity.name = (NSString *)obj;
        }];
        
        [self saveContext];
    }
}

@end