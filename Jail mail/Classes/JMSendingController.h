//
//  JMSendingController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 28/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@class JMPackage;

@interface JMSendingController : UIViewController <StoryboardProtocol>

- (void)newLetterPackage:(JMPackage *)aLetter;

- (void)continueLetterPackage:(JMPackage *)aLetter;

@end