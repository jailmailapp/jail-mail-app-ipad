//
//  JMFacebookProfile.m
//  Jail mail
//
//  Created by Oleksiy Kovtun on 4/1/15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMFacebookProfile.h"
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>

@implementation JMFacebookProfile

+ (void)downloadProfileInformationWithCompletion:(void (^)(NSError *error, id profile))completion {
    FBSDKAccessToken *currentAccessToken = [FBSDKAccessToken currentAccessToken];
    
    if (currentAccessToken == nil) {
        if (completion != nil) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"[FBSDKAccessToken currentAccessToken] returns nil. Please use this method only after user had logged in."};
            NSError *error = [NSError errorWithDomain:@"JMFacebookProfile (download profile information)"
                                                 code:0
                                             userInfo:userInfo];
            
            completion(error, nil);
        }
        
        return;
    }
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"me"
                                  parameters:nil
                                  HTTPMethod:@"GET"];
    
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        NSLog (@"FBSDKGraphRequest %@", error);
        
        if (completion != nil)
        {
            completion(error, result);
        }
    }];
}

+ (void)downloadProfilePictureOfType:(ProfilePictureType)type
                          completion:(void (^)(NSError *error, UIImage *profilePicture))completion {
    FBSDKAccessToken *currentAccessToken = [FBSDKAccessToken currentAccessToken];
    
    if (currentAccessToken == nil) {
        if (completion != nil) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"[FBSDKAccessToken currentAccessToken] returns nil. Please use this method only after user had logged in."};
            NSError *error = [NSError errorWithDomain:@"JMFacebookProfile (download profile picture)"
                                                 code:0
                                             userInfo:userInfo];
            
            completion(error, nil);
        }
        
        return;
    }
    
    NSString *userID = currentAccessToken.userID;
    NSString *URLString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture",
                           userID];
    
    switch (type) {
        case ProfilePictureTypeDefault:
            break;
        case ProfilePictureTypeSmall:
            URLString = [URLString stringByAppendingString:@"?type=small"];
            break;
        case ProfilePictureTypeNormal:
            URLString = [URLString stringByAppendingString:@"?type=normal"];
            break;
        case ProfilePictureTypeAlbum:
            URLString = [URLString stringByAppendingString:@"?type=album"];
            break;
        case ProfilePictureTypeLarge:
            URLString = [URLString stringByAppendingString:@"?type=large"];
            break;
        case ProfilePictureTypeSquare:
            URLString = [URLString stringByAppendingString:@"?type=square"];
            break;
    }
    
    NSURL *URL = [NSURL URLWithString:URLString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *connectionError) {
                               if (completion != nil) {
                                   UIImage *profilePicture = [UIImage imageWithData:data];
                                   
                                   completion(connectionError, profilePicture);
                               }
                           }];
}

@end
