//
//  JMSettingsController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 10/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMSettingsController.h"

#import "JMCardsController.h"
#import "JMRecipientsController.h"
#import "JMSendersController.h"
#import "UIViewController+SideMenu.h"

@implementation JMSettingsController

+ (NSString *)storyboardID
{
    return @"idSettingsController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Settings", @"Settings");
    
    [self.navigationItem setHidesBackButton:YES];
}


- (IBAction)selectSenderTap:(id)sender
{
    JMSendersController *senderController = [JMSendersController controller];
    [self.navigationController pushViewController:senderController animated:YES];
}


- (IBAction)selectRecipientTap:(id)sender
{
    JMRecipientsController *recipientController = [JMRecipientsController controller];
    [self.navigationController pushViewController:recipientController animated:YES];
}


- (IBAction)selectCardsTap:(id)sender
{
    JMCardsController *controller = [JMCardsController controller];
    [self.navigationController pushViewController:controller animated:YES];
}


@end