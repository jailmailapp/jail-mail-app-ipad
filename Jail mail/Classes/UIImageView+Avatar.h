//
//  UIImageView+Avatar.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 02/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Avatar)

- (void)circle;
- (void)blueBorder;
- (void)whiteBorder;
- (void)setAvatarImage:(UIImage *)avatarImage;

@end
