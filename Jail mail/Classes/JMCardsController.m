//
//  JMCardsViewController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 12/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCardsController.h"

#import "JMCDRepository+Card.h"
#import "JMCard.h"
#import "JMLetter.h"
#import "JMPackage.h"
#import "JMProfileCell.h"
#import "JMProfileView.h"

#import "JMCardEditController.h"
#import "JMCoreDataManager.h"
#import "JMNavigationController.h"
#import "JMReviewController.h"
#import "JMSendersController.h"
#import "JMSendingController.h"
#import "PSTAlertController.h"

@interface JMCardsController ()<EditableCellDelegate, JMSendersDelegate> {
    JMLetter *_letter;
}

@property (strong, nonatomic) IBOutlet UIImageView *blurlineView;

@end

@implementation JMCardsController

+ (NSString *)xibID
{
    return @"JMCardsController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title       = NSLocalizedString(@"Payment", @"Payment");
    self.prompt.text = NSLocalizedString(@"Choose saved card", @"Choose saved card");
    [self.createButton setBackgroundImage:[UIImage imageNamed:@"icon_add_card"] forState:UIControlStateNormal];
    [self.createButton setTitle:NSLocalizedString(@"Create new card", @"Create new card") forState:UIControlStateNormal];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_letter == nil)
    {
        self.blurlineView.translatesAutoresizingMaskIntoConstraints = YES;
        [self.blurlineView setFrame:CGRectMake(0, CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.blurlineView.frame))];
    }
    
    [self reloadItems];
}


- (void)updateEditButtonsWithEditing:(BOOL)isEditin
{
    UIBarButtonItem *addCardButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_add_simple"]
                                                                      style:UIBarButtonItemStyleDone
                                                                     target:self
                                                                     action:@selector(createCards:)];
    UIBarButtonItem *editButton = nil;
    
    if (isEditin)
    {
        editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_done"]
                                                      style:UIBarButtonItemStyleBordered
                                                     target:self
                                                     action:@selector(editTap:)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    else {
        editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_edit"]
                                                      style:UIBarButtonItemStyleBordered
                                                     target:self
                                                     action:@selector(editTap:)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:editButton, addCardButton, nil];
}


- (void)editTap:(id)sender
{
    self.editing = !self.editing;
    
    [self updateEditButtonsWithEditing:self.editing];
}


- (void)setLetter:(JMLetter *)letter
{
    _letter = letter;
    
    if ([self isViewLoaded])
    {
        [self reloadItems];
    }
}


- (void)reloadItems
{
    NSMutableArray *items = [[[JMCDRepository newRepository] allCard] mutableCopy];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    items = [[items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] mutableCopy];
    
    JMCard *card = [self lastCard];
    
    if (card)
    {
        [items removeObject:card];
        [items insertObject:card atIndex:0];
    }
    
    self.items = items;
    
    [self.galleryCollection reloadData];
    
    [super reloadItems];
}


//- (void)goContinueSendingPackage:(JMPackage *)package
//{
//    JMSendingController *sendingController = [JMSendingController controller];
//    [sendingController continueLetterPackage:package];
//    [self.navigationController pushViewController:sendingController animated:YES];
//}


- (void)goReviewPackage:(JMPackage *)package
{
    //    if ((_letter.sentAt == nil) && (_letter.letterID == nil) && (_letter.submittedAttachments.count == 0))
    //    {
    JMReviewController *sendingController = [JMReviewController controller];
    [sendingController setLetterPackage:package];
    [self.navigationController pushViewController:sendingController animated:YES];
    //    }
    //    else {
    //        [self goContinueSendingPackage:package];
    //    }
}


- (IBAction)createTap:(id)sender
{
    
}

- (IBAction)createCards:(id)sender
{
    if ((_letter == nil) || (_letter.sender == nil))
    {
        JMSendersController *sendersController = [JMSendersController controller];
        [sendersController setDelegate:self];
        JMNavigationController *navigation = [[JMNavigationController alloc] initWithRootViewController:sendersController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
        
        return;
    }
    
    [self createCardWithSender:_letter.sender];
}


- (IBAction)payPalTap:(id)sender
{
    JMPackage *package = [[JMPackage alloc] initWithLetter:_letter];
    package.card        = nil;
    package.typePayment = JMTypePaymentPayPal;
    
    [self goReviewPackage:package];
}


- (IBAction)applePayTap:(id)sender
{
    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:@"Coming soon" message:nil preferredStyle:PSTAlertControllerStyleAlert];
    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"OK", @"OK") style:PSTAlertActionStyleCancel handler:nil]];

    [alertController showWithSender:sender controller:self animated:YES completion:NULL];
    
//    JMPackage *package = [[JMPackage alloc] initWithLetter:_letter];
//    package.card        = nil;
//    package.typePayment = JMTypePaymentApplePay;
//    
//    [self goReviewPackage:package];
}


- (void)createCardWithSender:(JMUser *)user
{
    JMCardEditController *cardEditController = [JMCardEditController controller];
    [cardEditController creationWithUser:user];
    [self.navigationController pushViewController:cardEditController animated:YES];
}


- (void)selectedSender:(id)sender
{
    [self createCardWithSender:sender];
}


- (void)deleteCellAttachments:(id)attachments
{
    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"Delete", @"Delete") style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        JMCard *inmate = (JMCard *)attachments;
        
        if (inmate && [inmate isKindOfClass:JMCard.class])
        {
            NSLog (@"Delete");
            
            JMCDRepository *repository = [JMCDRepository newRepository];
            [repository deleteEntity:attachments];
            [repository save];
            
            [self reloadItems];
        }
    }]];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
    [alertController showWithSender:nil controller:self animated:YES completion:NULL];
}

- (void)storeLastCard:(JMCard *)card
{
    if (card != nil)
    {
        NSManagedObjectID *objectId = card.objectID;
        [[NSUserDefaults standardUserDefaults] setURL:[objectId URIRepresentation] forKey:@"someObjectIdKey"];
        
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (JMCard *)lastCard
{
    NSURL *uri = [[NSUserDefaults standardUserDefaults] URLForKey:@"someObjectIdKey"];
    
    NSManagedObjectID *objectId = [[[JMCoreDataManager sharedManager] persistent] managedObjectIDForURIRepresentation:uri];
    
    if (objectId != nil)
    {
        JMCard *object = (JMCard *)[[[JMCoreDataManager sharedManager] context] existingObjectWithID:objectId error:nil];
        return object;
    }
    
    return nil;
}


#pragma mark - UICollectionView

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMProfileCell *photoCell = (JMProfileCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JMCard *card = [self.items objectAtIndex:indexPath.row];
    
    [photoCell.profileView setAvatar:[UIImage imageNamed:@"card"]];
    
    NSString *name = @"";
    
    if (card.name && card.name.length > 0)
    {
        name = [NSString stringWithFormat:@"%@\n",card.name];
    }
    
    name = [NSString stringWithFormat:@"%@%@",name, card.numberName];
    
    [photoCell.profileView setName:name];
    [photoCell setAttachments:card];
    [photoCell setDelegate:self];
    photoCell.profileView.circle = NO;
    
    return photoCell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMCard *card = [self.items objectAtIndex:indexPath.row];
    
    if (self.editing)
    {
        JMCardEditController *cardEditController = [JMCardEditController controller];
        [self.navigationController pushViewController:cardEditController animated:YES];
        [cardEditController editCard:card];
    }
    else {
        if (_letter)
        {
            [self storeLastCard:card];
            
            JMPackage *package = [[JMPackage alloc] initWithLetter:_letter];
            package.card        = card;
            package.typePayment = JMTypePaymentCard;
            
            [self goReviewPackage:package];
        }
    }
}


@end