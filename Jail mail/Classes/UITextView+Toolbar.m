//
//  UITextView+DoneButton.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 23/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UITextView+Toolbar.h"

@implementation UITextView (Toolbar)

- (void)keyboardToolbarSetup
{
    if (self.inputAccessoryView == nil)
    {
        UIToolbar *keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done")
                                                                       style:UIBarButtonItemStyleDone
                                                                      target:self
                                                                      action:@selector(closeAction)];
        
        NSArray *toolbarButtons = [[NSArray alloc] initWithObjects:extraSpace, doneButton, nil];
        
        [keyboardToolbar setItems:toolbarButtons];
        
        self.inputAccessoryView = keyboardToolbar;
    }
}


- (void)closeAction
{
    [self resignFirstResponder];
}


@end