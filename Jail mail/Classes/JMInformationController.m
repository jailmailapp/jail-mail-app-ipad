//
//  JMInformationController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 10/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInformationController.h"

#import "UIViewController+SideMenu.h"
#import <MessageUI/MessageUI.h>

@interface JMInformationController ()<MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *informationWeb;
@property (strong, nonatomic) IBOutlet UISegmentedControl *control;

@end

@implementation JMInformationController

+ (NSString *)storyboardID
{
    return @"idInformationController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Information", @"Information");
    
    [self addMenuBarButton];
    
    [self informationTypeCanged:self.control];
}


- (IBAction)informationTypeCanged:(id)sender
{
    UISegmentedControl *control = sender;
    
    switch (control.selectedSegmentIndex) {
        case 0: {
            NSURL *url = [self urlAboutInformation];
            [self loadHTMLFromUrl:url];
        }
            break;
        case 1: {
            NSURL *url = [self urlFAQInformation];
            [self loadHTMLFromUrl:url];
        }
            break;
        case 2: {
            NSURL *url = [self urlTermsOfServiceInformation];
            [self loadHTMLFromUrl:url];
        }
            break;
        case 3: {
            NSURL *url = [self urlPrivacyPolicyInformation];
            [self loadHTMLFromUrl:url];
        }
            break;
        default:
            break;
    }
}


- (void)loadHTMLFromUrl:(NSURL *)url
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.informationWeb loadRequest:urlRequest];
}


- (NSURL *)urlAboutInformation
{
    return [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"About" ofType:@"html"]];
}


- (NSURL *)urlFAQInformation
{
    return [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FAQ" ofType:@"html"]];
}


- (NSURL *)urlTermsOfServiceInformation
{
    return [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"TermsOfService" ofType:@"html"]];
}

- (NSURL *)urlPrivacyPolicyInformation
{
    return [NSURL URLWithString:@"https://www.iubenda.com/privacy-policy/557770"];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] scheme] isEqual:@"mailto"])
    {
        [self showEmail:[[request URL] resourceSpecifier]];
        return NO;
    }
    
    return YES;
}


- (IBAction)showEmail:(NSString *)mail
{
    NSLog(@"%@", mail);
    
    NSString *emailTitle = @"JailMail App Customer Support";
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString *messageBody = [NSString stringWithFormat:@"Device name: %@ \nModel: %@ \nSystem: %@ %@ \nApp version: %@", device.name, device.model, device.systemName, device.systemVersion, [self version]];
    NSArray *toRecipents  = [NSArray arrayWithObject:mail];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
}


- (NSString *)version
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build   = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"%@ build %@", version, build];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end