//
//  JMRecipientsDelegate.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JMRecipientsDelegate <NSObject>

- (void)selectedRecipient:(id)sender;

@end
