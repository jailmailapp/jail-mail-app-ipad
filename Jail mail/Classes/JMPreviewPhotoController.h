//
//  JMPreviewPhotoController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 09/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@class JMLetter;

@interface JMPreviewPhotoController : UIViewController <StoryboardProtocol>

- (void)setLetter:(JMLetter *)letter;

@end
