//
//  JMSendersController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMGalleryController.h"

#import "JMSendersDelegate.h"

@interface JMSendersController : JMGalleryController

@property (nonatomic, weak) id <JMSendersDelegate> delegate;

@end
