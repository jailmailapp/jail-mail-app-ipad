//
//  JMPhotoCell.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 04/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "EditableCollectionCell.h"

@class JMAttachments;

@interface JMPhotoCell : EditableCollectionCell

@property (nonatomic, retain) UIImageView *photo;

@end
