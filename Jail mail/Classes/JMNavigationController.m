/*
 RMNavigationController.m
 RedmineMobile
 
 Created by Alexander Shvetsov on 31.01.14.
 Copyright (c) 2014 Yanpix. All rights reserved.
 */

#import "JMNavigationController.h"

#import "MFSideMenu.h"

@interface JMNavigationController (){
    UIColor *_colorBlue;
    UIColor *_colorGray;
}


@end

@implementation JMNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  
	if (self)
	{
		[self initialization];
	}
  
	return self;
}


- (id)initWithRootViewController:(UIViewController *)rootViewController
{
	self = [super initWithRootViewController:rootViewController];
  
	if (self)
	{
		[self initialization];
	}
  
	return self;
}


- (void)initialization
{
    _colorBlue = [UIColor colorWithRed:0.243 green:0.471 blue:0.835 alpha:1.000];
    _colorGray = [UIColor colorWithRed:0.247 green:0.282 blue:0.361 alpha:1.000];
    
	[[self navigationBar] setTintColor:[UIColor whiteColor]];
    [[self navigationBar] setTranslucent:NO];
    [[self navigationBar] setBarTintColor:_colorBlue];
    [[self navigationBar] setShadowImage:[[UIImage alloc] init]];
    [[self navigationBar] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
  
	[[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (void)setColorBlue
{
    [[self navigationBar] setBarTintColor:_colorBlue];
}

- (void)setColorGray
{
    [[self navigationBar] setBarTintColor:_colorGray];
}

@end