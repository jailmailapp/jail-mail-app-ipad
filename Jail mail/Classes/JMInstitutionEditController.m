//
//  JMInstitutionEditController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 21/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInstitutionEditController.h"

#import "JMCDEntityFactory+Institution.h"
#import "JMCDRepository+State.h"
#import "JMCDRepository.h"
#import "JMInstitution.h"
#import "JMState.h"
#import "JMTextFieldCell.h"
#import "UIViewController+ImageBackButton.h"

static NSString *const kName    = @"Name";
static NSString *const kStreet  = @"Street Address";
static NSString *const kCity    = @"City";
static NSString *const kZipCode = @"Zip Code";
static NSString *const kState   = @"State";

@interface JMInstitutionEditController ()
<UIPickerViewDelegate,
UIPickerViewDataSource>{
    NSArray *_states;
    UIView *statesView;
}

@property (nonatomic, retain) JMInstitution *institution;

@end

@implementation JMInstitutionEditController

+ (NSString *)storyboardID
{
    return @"idInstitutionEditController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"New Institution", @"New Institution");
    
    self.headers = [NSArray arrayWithObjects:
                    kName,
                    kStreet,
                    kCity,
                    kZipCode,
                    kState, nil];
    
    [self setCustomBackButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_institution)
    {
        self.navigationItem.title = NSLocalizedString(@"Edit Institution", @"Edit Institution");
    }
}


- (void)editInstitution:(JMInstitution *)institution
{
    if (institution && [institution isKindOfClass:JMInstitution.class])
    {
        _institution = institution;
    }
}


- (JMInstitution *)institution
{
    if (_institution == nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        _institution = [[JMCDEntityFactory newFactory] newInstitution];
        [repository addEntity:_institution];
    }
    
    return _institution;
}


- (IBAction)backButtonTap:(id)sender
{
    [self.tableView reloadData];
    
    if ([self validateAll] == YES)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)doneTap:(id)sender
{
    [super doneTap:sender];
    
    [self.tableView reloadData];
    
    if ([self validateAll] == YES)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

#pragma mark - Validate

- (NSArray *)wrongField
{
    NSMutableArray *wrongFields = [NSMutableArray new];
    
    if (_institution == nil)
    {
        return wrongFields;
    }
    
    if (_institution.name.length == 0)
    {
        [wrongFields addObject:kName];
    }
    
    if (_institution.address.length == 0)
    {
        [wrongFields addObject:kStreet];
    }
    
    if (_institution.city.length == 0)
    {
        [wrongFields addObject:kCity];
    }
    
    if (_institution.zipCode.length == 0)
    {
        [wrongFields addObject:kZipCode];
    }
    
    if (_institution.state == nil)
    {
        [wrongFields addObject:kState];
    }
    
    return wrongFields;
}


- (BOOL)validateAll
{
    NSArray *wrongFields = [self wrongField];
    
    if ([wrongFields containsObject:kState])
    {
        [self errorAlertWithText:NSLocalizedString(@"Select institution.", @"Select institution.")];
    }
    else {
        if (wrongFields.count > 0)
        {
            [self errorAlertWithText:NSLocalizedString(@"Сorrect the fields marked in red.", @"Сorrect the fields marked in red.")];
        }
    }
    
    [self.tableView reloadData];
    
    return wrongFields.count == 0;
}


- (BOOL)validateRow:(NSInteger)row
{
    NSArray *wrongFields = [self wrongField];
    
    return ![wrongFields containsObject:[self.headers objectAtIndex:row]];
}


#pragma mark -

- (void)header:(NSString *)header didEndEditing:(NSString *)string
{
    NSString *text = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([header isEqualToString:kName] && (text.length > 0))
    {
        self.institution.name = text;
    }
    else if ([header isEqualToString:kStreet] && (text.length > 0)) {
        self.institution.address = text;
    }
    else if ([header isEqualToString:kCity] && (text.length > 0)) {
        self.institution.city = text;
    }
    else if ([header isEqualToString:kZipCode] && (text.length > 0)) {
        self.institution.zipCode = text;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JMTextFieldCell *cell = (JMTextFieldCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    NSString *header = [self.headers objectAtIndex:indexPath.section];
    
    cell.field.keyboardType = UIKeyboardTypeDefault;
    
    if ([header isEqualToString:kName])
    {
        cell.field.text = _institution.name;
    }
    else if ([header isEqualToString:kStreet]) {
        cell.field.text = _institution.address;
    }
    else if ([header isEqualToString:kCity]) {
        cell.field.text = _institution.city;
    }
    else if ([header isEqualToString:kZipCode]) {
        cell.field.text         = _institution.zipCode;
        cell.field.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    else if ([header isEqualToString:kState]) {
        cell.field.text = _institution.state.name;
        cell.field.userInteractionEnabled = NO;
    } else {
        cell.field.userInteractionEnabled = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self pickerState];
}


#pragma mark - UIPickerView and UIPickerViewDataSource

- (UIToolbar *)accessoryView
{
    UIToolbar *accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(doneStateSelection)];
    [accessoryView setItems:[NSArray arrayWithObject:doneButton]];
    
    return accessoryView;
}


- (void)doneStateSelection
{
    [self.tableView reloadData];
    [self closeStateSelection];
}


- (void)closeStateSelection
{
    [statesView removeFromSuperview];
    statesView = nil;
}


- (void)pickerState
{
    [[self view] endEditing:YES];
    
    if (_states == nil)
    {
        NSArray *items = [[JMCDRepository newRepository] allState];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        _states = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    }
    
    if (statesView == nil)
    {
        float screenWidth  = self.view.bounds.size.width;
        float screenHeight = self.view.bounds.size.height;
        
        statesView                 = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight/2, screenWidth, screenHeight/2)];
        statesView.backgroundColor = [UIColor whiteColor];
        [statesView addSubview:self.accessoryView];
        [self.view addSubview:statesView];
        
        UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, screenWidth, 280.0f)];
        pickerView.backgroundColor = [UIColor whiteColor];
        [pickerView setDataSource:self];
        [pickerView setDelegate:self];
        pickerView.showsSelectionIndicator = YES;
        
        [statesView addSubview:pickerView];
        
        id obj = _institution.state;
        
        if ([obj isKindOfClass:[JMState class]])
        {
            NSInteger index = [[_states valueForKey:@"name"] indexOfObject:[obj name]];
            
            [pickerView selectRow:index inComponent:0 animated:YES];
        } else {
            JMState *state = [_states objectAtIndex:0];
            self.institution.state = state;
        }
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_states count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    JMState *state = [_states objectAtIndex:row];
    return state.name;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    JMState *state = [_states objectAtIndex:row];
    self.institution.state = state;
}


@end