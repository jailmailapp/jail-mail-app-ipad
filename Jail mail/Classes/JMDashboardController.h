//
//  JMDashboardController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 26/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@interface JMDashboardController : UIViewController <StoryboardProtocol>

- (IBAction)createMailTap:(id)sender;
- (IBAction)createPostcardTap:(id)sender;
- (IBAction)createPhotoTap:(id)sender;

@end
