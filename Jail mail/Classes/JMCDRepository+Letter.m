//
//  JMCDRepository+Letter.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+Letter.h"

#import "JMCoreDataManager.h"

@implementation JMCDRepository (Letter)

- (NSArray *)allLetters
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMLetter entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}


- (NSInteger)countMassages
{
    NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"message.length > 0"];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMLetter entityName]];
    [request setPredicate:predicate];
    NSError *error  = nil;
    NSArray *result = [self.manager.context executeFetchRequest:request error:&error];
    
    return result.count;
}


- (NSInteger)countPhotos
{
    NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"attachments.@count > 0"];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMLetter entityName]];
    [request setPredicate:predicate];
    NSError *error  = nil;
    NSArray *result = [self.manager.context executeFetchRequest:request error:&error];
    
    NSInteger count = 0;
    
    for (JMLetter *letter in result) {
        count +=[letter.attachments count];
    }
    
    return count;
}


@end