//
//  UserRepository.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+User.h"

#import "JMCoreDataManager.h"

@implementation JMCDRepository (User)

- (NSArray *)allUsers
{
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"plays.@count > 0"];
    //    [request setPredicate:predicate];
    //    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MYSong" inManagedObjectContext:context];
    //    NSDictionary *entityProperties = [entity propertiesByName];
    //    [request setReturnsDistinctResults:YES];
    //    [request setPropertiesToFetch:[NSArray arrayWithObject:[entityProperties objectForKey:@"artist"]]];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMUser entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}


@end