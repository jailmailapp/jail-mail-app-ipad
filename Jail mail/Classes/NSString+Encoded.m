//
//  NSString+Encoded.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 28/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "NSString+Encoded.h"

@implementation NSString (Encoded)

+ (NSMutableString *)convertSpecialCharacterIntoHTMLEncoded:(NSString *)data1 andURL:(BOOL)isURL
{
    NSMutableString *convertedString = [data1 mutableCopy];
    
    if (!isURL)
    {
        [convertedString replaceOccurrencesOfString:@"&" withString:@"and"    options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    }
    else {
        [convertedString replaceOccurrencesOfString:@"&" withString:@"&#38;"    options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    }
    
    [convertedString replaceOccurrencesOfString:@"<" withString:@"&#60;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@">" withString:@"&#62;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"\"" withString:@"&#34;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"\'" withString:@"&#39;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"'" withString:@"&#39;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"," withString:@"&#44;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"^" withString:@"&#94;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"/" withString:@"&#47;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"@" withString:@"&#64;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"(" withString:@"&#40;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@")" withString:@"&#41;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"[" withString:@"&#91;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"]" withString:@"&#93;"  options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"{" withString:@"&#123;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"}" withString:@"&#125;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"~" withString:@"&#126;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"&nbsp;"withString:@"&#160;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¡" withString:@"&#161;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¢" withString:@"&#162;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"£" withString:@"&#163;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¤" withString:@"&#164;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¥" withString:@"&#165;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¦" withString:@"&#166;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"§" withString:@"&#167;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¨" withString:@"&#168;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"©" withString:@"&#169;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"ª" withString:@"&#170;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"«" withString:@"&#171;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¬" withString:@"&#172;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@""  withString:@"&#173;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"®" withString:@"&#174;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¯" withString:@"&#175;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"°" withString:@"&#176;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"±" withString:@"&#177;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"²" withString:@"&#178;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"³" withString:@"&#179;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"´" withString:@"&#180;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"µ" withString:@"&#181;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¶" withString:@"&#182;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"·" withString:@"&#183;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¸" withString:@"&#184;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¹" withString:@"&#185;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"º" withString:@"&#186;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"»" withString:@"&#187;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¼" withString:@"&#188;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"½" withString:@"&#189;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¾" withString:@"&#190;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¿" withString:@"&#191;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"×" withString:@"&#215;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"÷" withString:@"&#247;" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length] )];
    [convertedString replaceOccurrencesOfString:@"¿" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [convertedString length])];
    return convertedString;
}

@end
