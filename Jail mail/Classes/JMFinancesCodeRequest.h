//
//  JMPromotionalCodeRequest.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 29/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@class JMPackage;

@interface JMFinancesCodeRequest : NSObject

+ (void)verificationCode:(NSString *)code
                 package:(JMPackage *)package
                 success:(void (^)(id response))success
                 failure:(void (^)(NSError *error))failure;

// Only for PayPal and Apple Pay payment modes
+ (void)updateRewardWithDeviceID:(NSString *)deviceID
                         package:(JMPackage *)package
                         success:(void (^)(id response))success
                         failure:(void (^)(NSError *error))failure;

+ (void)rewardsWithDeviceID:(NSString *)deviceID
                    success:(void (^)(id response))success
                    failure:(void (^)(NSError *error))failure;

+ (AFURLConnectionOperation *)totalPriceWithDeviceID:(NSString *)deviceID
                                             package:(JMPackage *)package
                                             success:(void (^)(id response))success
                                             failure:(void (^)(NSError *error))failure;

@end