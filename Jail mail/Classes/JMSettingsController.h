//
//  JMSettingsController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 10/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@interface JMSettingsController : UIViewController <StoryboardProtocol>

@end
