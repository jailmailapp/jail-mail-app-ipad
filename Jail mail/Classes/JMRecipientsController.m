//
//  JMRecipientsController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMRecipientsController.h"

#import "JMCDRepository+Recipient.h"
#import "JMInmate.h"
#import "JMProfileCell.h"
#import "JMProfileView.h"

#import "JMRecipientEditController.h"
#import "PSTAlertController.h"

@interface JMRecipientsController ()<EditableCellDelegate>

@end

@implementation JMRecipientsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Contacts", @"Contacts");
    self.prompt.text = NSLocalizedString(@"Choose saved contact", @"Choose saved contact");
    [self.createButton setTitle:NSLocalizedString(@"Create new contact", @"Create new contact") forState:UIControlStateNormal];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadItems];
}


- (void)reloadItems
{
    NSArray *items = [[JMCDRepository newRepository] allRecipient];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    self.items = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    [self.galleryCollection reloadData];
    
    [super reloadItems];
}


- (IBAction)createTap:(id)sender
{
    JMRecipientEditController *recipientEditController = [JMRecipientEditController controller];
    [self.navigationController pushViewController:recipientEditController animated:YES];
}


- (void)deleteCellAttachments:(id)attachments
{
    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        JMInmate *inmate = (JMInmate *)attachments;
        
        if (inmate && [inmate isKindOfClass:JMInmate.class])
        {
            NSLog (@"Delete");
            
            JMCDRepository *repository = [JMCDRepository newRepository];
            [repository deleteEntity:attachments];
            [repository save];
            
            [self reloadItems];
        }
    }]];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
    [alertController showWithSender:nil controller:self animated:YES completion:NULL];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMProfileCell *photoCell = (JMProfileCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JMInmate *inmate = [self.items objectAtIndex:indexPath.row];
    [photoCell.profileView setAvatar:inmate.avatar];
    [photoCell.profileView setName:inmate.fullName];
    [photoCell setAttachments:inmate];
    [photoCell setDelegate:self];
    
    return photoCell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMInmate *inmate = [self.items objectAtIndex:indexPath.row];
    
    if (self.editing)
    {
        JMRecipientEditController *recipientEditController = [JMRecipientEditController controller];
        [recipientEditController setRecipient:inmate];
        [self.navigationController pushViewController:recipientEditController animated:YES];
    }
    else {
        if (_delegate && [_delegate respondsToSelector:@selector(selectedRecipient:)])
        {
            [_delegate selectedRecipient:inmate];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


@end