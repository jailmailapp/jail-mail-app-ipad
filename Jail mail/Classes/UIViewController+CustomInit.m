//
//  UIViewController+Storyboard.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 02/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UIViewController+CustomInit.h"

static NSString *const kStoryboard = @"Shvetcov";

@implementation UIViewController (CustomInit)

+ (id)controller
{
    if ([self respondsToSelector:@selector(storyboardID)]) {
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:kStoryboard bundle:nil];
        UIViewController* vc = [sb instantiateViewControllerWithIdentifier:[[self class] storyboardID]];
        return vc;
    }
    
    if ([self respondsToSelector:@selector(xibID)]) {
        UIViewController *vc = [[[self class] alloc] initWithNibName:[[self class] xibID] bundle:nil];
        return vc;
    }
    
    NSLog(@"Error init controller");
    
    return nil;
}

@end
