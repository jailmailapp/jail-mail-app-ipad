//
//  UIViewController+ImageBackButton.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UIViewController+ImageBackButton.h"

@implementation UIViewController (ImageBackButton)

- (void)setCustomBackButton
{
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_left"]
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(backButtonTap:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.hidesBackButton   = YES;
}

- (void)backButtonTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
