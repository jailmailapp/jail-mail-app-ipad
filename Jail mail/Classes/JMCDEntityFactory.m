//
//  CDEntityFactory.m
//  Jail mail
//
//  Created by Alexander on 29.03.15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMEntity.h"
#import "JMCoreDataManager.h"

@interface JMCDEntityFactory ()

@property (nonatomic, retain) JMCoreDataManager *manager;

@end

@implementation JMCDEntityFactory

+ (id)newFactory
{
    JMCDEntityFactory *entityFactory = [[JMCDEntityFactory alloc] init];
    
    return entityFactory;
}


- (JMEntity *)newEntityForClass:(Class)classEntity
{
    NSString *entityName        = [classEntity entityName];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self.manager context]];
    JMEntity *newEntity           = [[JMEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    
    return newEntity;
}


- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.manager = [JMCoreDataManager sharedManager];
    }
    
    return self;
}


@end