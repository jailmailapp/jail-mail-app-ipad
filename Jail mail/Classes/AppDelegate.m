//
//  AppDelegate.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 25/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "AppDelegate.h"

#import "AFNetworkActivityIndicatorManager.h"
#import "JMDashboardController.h"
#import "JMInboxesController.h"
#import "JMMenuController.h"
#import "JMNavigationController.h"
#import "JMRootViewController.h"
#import "MFSideMenu.h"
#import "WalkthroughsViewController.h"
#import <FBSDKCoreKit/FBSDKAppEvents.h>
#import <FBSDKCoreKit/FBSDKApplicationDelegate.h>
#import <GoogleSignIn/GoogleSignIn.h>

#import "AppboyKit.h"
#import "Flurry.h"
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>
#import <Parse/Parse.h>

// Run script
// ./Fabric.framework/run 683a8a0774b8db518c79863c2d95d6a804552bca f161e71e81553f2b3a72aaed05210d91249bac790be46702ee81d1f9d4020675

#define FLURRY_API_KEY          @"9D65XQKR8TYMRYPMBNXV"
#define PARSE_APPLICATION_ID    @"vKNs7pXn1QPdU5SNfgCZhG6H8VfwRpQM02kIgwsm"
#define PARSE_CLIENT_KEY        @"uL9GgqO4wLyhmuUjecYWIfGhpp1m2Hh7iI47OJ2R"
#define APPBOY_API_KEY          @"5f2e6989-9b54-465e-9fa8-6916967d41dd"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // -------------------------------------------------

    [Flurry startSession:FLURRY_API_KEY];

    // =====

    [Fabric with:@[CrashlyticsKit]];

    // =====

    [Parse setApplicationId:PARSE_APPLICATION_ID clientKey:PARSE_CLIENT_KEY];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];

    // =====

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0

    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }

#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#endif

    // =====

    [Appboy startWithApiKey:APPBOY_API_KEY inApplication:application withLaunchOptions:launchOptions];

    // -------------------------------------------------

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.rootController = [JMRootViewController controller];

    JMInboxesController *inboxesController  = [JMInboxesController controller];
    JMNavigationController *_rootNavigation = [[JMNavigationController alloc] initWithRootViewController:inboxesController];
    [_rootNavigation setColorGray];

    JMDashboardController *composeController = [JMDashboardController controller];
    self.detailNavigation = [[JMNavigationController alloc] initWithRootViewController:composeController];
    [self.detailNavigation setColorBlue];

    inboxesController.detail = composeController;

    _rootController.viewControllers = [NSArray arrayWithObjects:_rootNavigation, _detailNavigation, nil];

    self.window.rootViewController = _rootController;
    [self.window makeKeyAndVisible];

    self.rootController.delegate = _rootController;

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL result                  = [userDefaults boolForKey:@"Walkthroughs"];

    if (result == NO)
    {
        [userDefaults setBool:YES forKey:@"Walkthroughs"];
        [userDefaults synchronize];

        [self showWalkthroughs];
    }

    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}


- (void)showWalkthroughs
{
    UIStoryboard *storyboard                 = [UIStoryboard storyboardWithName:@"Walkthroughs" bundle:nil];
    WalkthroughsViewController *walkthroughs = [storyboard instantiateViewControllerWithIdentifier:@"idWalkthroughsViewController"];
    [self.window.rootViewController presentViewController:walkthroughs animated:YES completion:nil];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current Installation and save it to Parse
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [PFPush handlePush:userInfo];
}


- (BOOL)  application:(UIApplication *)application
              openURL:(NSURL *)url
    sourceApplication:(NSString *)sourceApplication
           annotation:(id)annotation
{
    NSString *urlAbsoluteString = url.absoluteString;

    if ([urlAbsoluteString hasPrefix:@"fb398629510303796"])
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    else {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }

    return NO;
}


@end