//
//  DataManager.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

@interface JMCoreDataManager : NSObject

+ (id)sharedManager;

- (NSManagedObjectContext *)context;

- (NSPersistentStoreCoordinator *)persistent;

- (NSError *)saveContext;

- (void)rollback;

@end