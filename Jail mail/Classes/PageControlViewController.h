//
//  PageControlViewController.h
//  Tibetan Singing Bowls
//
//  Created by Alexander Shvetsov on 24/Sep/2015.
//  Copyright © 2015 Yanpix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageControlViewController : UIPageViewController

@end
