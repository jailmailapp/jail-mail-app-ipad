//
//  JMFacebookLogin.m
//  Jail mail
//
//  Created by Oleksiy Kovtun on 3/27/15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMFacebookLogin.h"
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>

@interface JMFacebookLogin () {
    fbLogInCompletionBlock _logInCompletionBlock;
    fbLogInCancelBlock _logInCancelBlock;
}

@end

@implementation JMFacebookLogin

+ (instancetype)sharedInstance {
    static JMFacebookLogin *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    
    if (self != nil) {
        _logInCompletionBlock = nil;
        _logInCancelBlock = nil;
    }
    
    return self;
}

#pragma mark -

- (void)logInWithCompletionBlock:(fbLogInCompletionBlock)completionBlock
                     cancelBlock:(fbLogInCancelBlock)cancelBlock {
    _logInCompletionBlock = completionBlock;
    _logInCancelBlock = cancelBlock;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    
    [login logInWithReadPermissions:@[@"public_profile", @"email"]
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (result.isCancelled) {
                                    if (_logInCancelBlock != nil) {
                                        _logInCancelBlock();
                                    }
                                } else {
                                    completionBlock(error);
                                }
                            }];
}

@end
