//
//  JMInstitutionController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMGalleryController.h"

@protocol JMInstitutionControllerDelegate <NSObject>

- (void)selectedInstitution:(id)institution;

@end

@interface JMInstitutionController : JMGalleryController

@property (nonatomic, weak) id <JMInstitutionControllerDelegate> delegate;

@end
