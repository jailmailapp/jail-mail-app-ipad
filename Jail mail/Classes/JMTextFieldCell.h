//
//  JMTextFieldCell.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMTextField : UITextField

@property (nonatomic, retain) id attachments;

@end

@interface JMTextFieldCell : UITableViewCell

@property (nonatomic, retain) JMTextField *field;

@end
