//
//  JMInboxesCell.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 03/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMInboxesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UIImageView *type;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UILabel *draftLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@end
