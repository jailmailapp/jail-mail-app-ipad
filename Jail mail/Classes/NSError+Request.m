//
//  NSError+Request.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/07/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "NSError+Request.h"

#define ERROR_CODE 2000

@implementation NSError (Request)

+ (NSError *)errorWithDescription:(NSString *)aDescription
{
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setValue:aDescription forKey:NSLocalizedDescriptionKey];
    
    NSError *error = [NSError errorWithDomain:@"JMRequestError" code:ERROR_CODE userInfo:details];
    
    return error;
}

+ (NSError *)errorWithCode:(NSInteger)code description:(NSString *)aDescription
{
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setValue:aDescription forKey:NSLocalizedDescriptionKey];
    
    NSError *error = [NSError errorWithDomain:@"JMRequestError" code:code userInfo:details];
    
    return error;
}

@end
