//
//  JMSenderEditController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEditProfileController.h"

#import "JMSendersDelegate.h"

@interface JMSenderEditController : JMEditProfileController

@property (nonatomic, weak) id <JMSendersDelegate> delegate;

- (void)setSender:(id)sender;

@end
