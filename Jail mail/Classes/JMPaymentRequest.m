//
//  JMFingerprintRequest.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/07/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPaymentRequest.h"

#import "AFNetworking.h"
#import "JMConst.h"
#import "JMUser.h"
#import "JMPackage.h"
#import "NSError+Request.h"
#import "JMAttachments.h"
#import "CardIOCreditCardInfo.h"


@implementation JMPaymentRequest

+ (void)creditCardPaymentPackage:(JMPackage *)package deviceID:(NSString *)deviceID success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    NSLog(@"Send credit card");
    // http://openxcellaus.info/InMate/payment/action/payment.php
    // http://notforgotten.biz/webservice/payment/action/payment.php
    // http://notforgotten.biz/webservice/testing/payment/action/payment.php
    
    
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<RequestCarrier>"];
    [xmlString appendFormat:@"<userId>-1</userId>"];
    [xmlString appendFormat:@"<messageId>%@</messageId>", package.letter.letterID];
    [xmlString appendFormat:@"<deviceId>%@</deviceId>", deviceID];
    [xmlString appendFormat:@"<transactionId>-1</transactionId>"];
    [xmlString appendFormat:@"<command>INMATE-1-2-1</command>"];
    [xmlString appendFormat:@"<userEmail>%@</userEmail>", package.letter.sender.email];
    
    [xmlString appendFormat:@"<modelListWarapper>"];
    [xmlString appendFormat:@"<list>"];
    [xmlString appendFormat:@"<com.notforgotten.promo.business.model.InmateTransaction>"];
    
    [xmlString appendFormat:@"<transactionId>-1</transactionId>"];
    [xmlString appendFormat:@"<moneyOrder>0</moneyOrder>"];
    
    if (package.letter.type == JMLetterTypePostcard)
    {
        [xmlString appendFormat:@"<letterPages>0</letterPages>"];
        [xmlString appendFormat:@"<photCount>0</photCount>"];
    } else {
        [xmlString appendFormat:@"<letterPages>%ld</letterPages>", (long)package.letterPages];
        [xmlString appendFormat:@"<photCount>%ld</photCount>", (long)package.letterSubmittedPhotos];
    }
    
    [xmlString appendFormat:@"<couponId>%@</couponId>", package.discountID];
    
    [xmlString appendFormat:@"<firstName>%@</firstName>", package.card.user.fullName];
    [xmlString appendFormat:@"<cardNumber>%@</cardNumber>", package.card.number];
    
    CardIOCreditCardInfo *card = [[CardIOCreditCardInfo alloc] init];
    card.cardNumber = package.card.number;
    NSString *name = [CardIOCreditCardInfo displayStringForCardType:card.cardType usingLanguageOrLocale:@"en"];
    [xmlString appendFormat:@"<cardType>%@</cardType>", name];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"MM"];
    [xmlString appendFormat:@"<expiryMonth>%@</expiryMonth>", [formatter stringFromDate:package.card.date]];
    
    [formatter setDateFormat:@"yyyy"];
    [xmlString appendFormat:@"<expiryYear>%@</expiryYear>", [formatter stringFromDate:package.card.date]];
    
    [xmlString appendFormat:@"<cardCode>%@</cardCode>", package.card.cvcode];
    [xmlString appendFormat:@"<userId>-1</userId>"];
    [xmlString appendFormat:@"<messageId>%@</messageId>", package.letter.letterID];
    
    [xmlString appendFormat:@"</com.notforgotten.promo.business.model.InmateTransaction>"];
    [xmlString appendFormat:@"</list>"];
    [xmlString appendFormat:@"</modelListWarapper>"];
    
    [xmlString appendFormat:@"</RequestCarrier>"];
    
    NSLog(@"%@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/uploadInMateItemAndCoupon.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Request Request (../inmate/uploadInMateItemAndCoupon.html) Success: %@", [operation responseString]);
                                                                  
                                                                  NSArray *token = [[operation responseString] componentsSeparatedByString:@":"];
                                                                  
                                                                  if (token.count >= 2) {
                                                                      [JMPaymentRequest checkTransaction:[token objectAtIndex:1] package:package success:^(id response) {
                                                                          if (success)
                                                                          {
                                                                              dispatch_async (dispatch_get_main_queue (), ^{
                                                                                  success (response);
                                                                              });
                                                                          }
                                                                      } failure:^(NSError *error) {
                                                                          if (failure)
                                                                          {
                                                                              dispatch_async (dispatch_get_main_queue (), ^{
                                                                                  failure (error);
                                                                              });
                                                                          }
                                                                      }];
                                                                  } else {
                                                                      if (failure)
                                                                      {
                                                                          dispatch_async (dispatch_get_main_queue (), ^{
                                                                              failure ([NSError errorWithDescription:NSLocalizedString (@"Received an invalid response from the server.", @"Received an invalid response from the server.")]);
                                                                          });
                                                                      }
                                                                  }
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Request Request (../inmate/uploadInMateItemAndCoupon.html) Error: %@", error);
                                                                  
                                                                  if (failure)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          failure ([NSError errorWithDescription:NSLocalizedString (@"Problem send card info.", @"Problem send card info.")]);
                                                                      });
                                                                  }
                                                                  
                                                                  ;
                                                              }];
    [manager.operationQueue addOperation:operation];
}

+ (void)checkTransaction:(NSString *)transactionID package:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<RequestCarrier>"];
    [xmlString appendFormat:@"<userId>-1</userId>"];
    [xmlString appendFormat:@"<messageId>-1</messageId>"];
    [xmlString appendFormat:@"<transactionId>%@</transactionId>", transactionID];
    [xmlString appendFormat:@"<command>INMATE-1-2-2</command>"];
    [xmlString appendFormat:@"<userEmail>%@</userEmail>", package.letter.sender.email];
    [xmlString appendFormat:@"</RequestCarrier>"];
    
    NSLog(@"Check Transaction: %@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/inmateService.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Request (.../inmate/inmateService.html) Success: %@",  [operation responseString]);
                                                                  
                                                                  if (success)
                                                                  {
                                                                      success ([operation responseString]);
                                                                  }
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Request (.../inmate/inmateService.html) Error: %@", error);
                                                                  
                                                                  if (failure)
                                                                  {
                                                                      failure ([NSError errorWithDescription:NSLocalizedString (@"Error check transaction status.", @"Error check transaction status.")]);
                                                                  }
                                                              }];
    
    [manager.operationQueue addOperation:operation];
}


+ (void)fingerprintWithAmount:(NSString *)totalAmount success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;
{
    if (totalAmount == nil)
    {
        dispatch_async (dispatch_get_main_queue (), ^{
            failure ([NSError errorWithDescription:NSLocalizedString (@"Error counting the amount of the letter.", @"Error counting the amount of the letter.")]);
        });
        
        return;
    }
    
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<root>"];
    [xmlString appendFormat:@"<amount>%@</amount>", totalAmount];
    [xmlString appendFormat:@"</root>"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/obtainFingerPrint.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Request (.../inmate/obtainFingerPrint.html) success: %@", [operation responseString]);
                                                                  
                                                                  if (success)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          success (responseObject);
                                                                      });
                                                                  }
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Request (.../inmate/obtainFingerPrint.html) error: %@", error);
                                                                  
                                                                  if (failure)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          failure ([NSError errorWithDescription:NSLocalizedString (@"Error receipt of the Fingerprint Hash.", @"Error receipt of the Fingerprint Hash.")]);
                                                                      });
                                                                  }
                                                              }];
    
    [manager.operationQueue addOperation:operation];
}

+ (void)verifyAuthorizeNetTransaction:(NSString *)transactionID package:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<root>"];
    [xmlString appendFormat:@"<letterID>%@</letterID>", package.letter.letterID];
    [xmlString appendFormat:@"<payments>"];
    [xmlString appendFormat:@"<success>%d</success>", (transactionID != nil)];
    [xmlString appendFormat:@"<transactionID>%@</transactionID>", transactionID];
    [xmlString appendFormat:@"<type>AuthorizeNet</type>"];
    [xmlString appendFormat:@"</payments>"];
    [xmlString appendFormat:@"<promoCode>%@</promoCode>", package.discountID];
    [xmlString appendFormat:@"<userID>%@</userID>", package.letter.userID];
    [xmlString appendFormat:@"</root>"];
    
    NSLog(@"%@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/verifyAndSaveTransaction.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Verify AuthorizeNet Request Success: %@ %@", responseObject,  [operation responseString]);
                                                                  
                                                                  if (success)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          success ([operation responseString]);
                                                                      });
                                                                  }
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Verify AuthorizeNet Request Error: %@", error);
                                                                  
                                                                  if (failure)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          failure ([NSError errorWithDescription:NSLocalizedString (@"Error verify Authorize Net transaction.", @"Error verify Authorize Net transaction.")]);
                                                                      });
                                                                  }
                                                              }];
    
    [manager.operationQueue addOperation:operation];
}

#pragma mark - PayPal

// <root>
// <letterID>24705</letterID>
// <payments>
// <success>true</success>
// <transactionID>11111111111111111</transactionID>
// <type>AuthorizeNet</type>
// </payments>
// <promoCode>0a62018ec506d7dfd40e</promoCode>
// <userID>4905</userID>
// </root>

+ (void)verifyPayPalTransaction:(NSString *)transactionID package:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<root>"];
    [xmlString appendFormat:@"<letterID>%@</letterID>", package.letter.letterID];
    [xmlString appendFormat:@"<payments>"];
    [xmlString appendFormat:@"<success>%d</success>", (transactionID != nil)];
    [xmlString appendFormat:@"<transactionID>%@</transactionID>", transactionID];
    [xmlString appendFormat:@"<type>PayPal</type>"];
    [xmlString appendFormat:@"</payments>"];
    [xmlString appendFormat:@"<promoCode>%@</promoCode>", package.discountID];
    [xmlString appendFormat:@"<userID>%@</userID>", package.letter.userID];
    [xmlString appendFormat:@"</root>"];
    
    NSLog(@"%@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/verifyAndSaveTransaction.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Verify PayPal Request success: %@ %@", responseObject,  [operation responseString]);
                                                                  
                                                                  if (success)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          success ([operation responseString]);
                                                                      });
                                                                  }
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Verify PayPal Request error: %@", error);
                                                                  
                                                                  if (failure)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          failure ([NSError errorWithDescription:NSLocalizedString (@"Error verify PayPal transaction.", @"Error verify PayPal transaction.")]);
                                                                      });
                                                                  }
                                                              }];
    
    [manager.operationQueue addOperation:operation];
}

@end