//
//  PDFHelper.m
//  PDFDemo
//
//  Created by Friendlydeveloper on 29.07.11.
//  Copyright 2011 www.codingsessions.com. All rights reserved.
//

#import "PDFHelper.h"

#import "JMAttachments.h"
#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMPackage.h"
#import "JMState.h"
#import "JMUser.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>

#define LEFT_MARGIN 26
#define RIGHT_MARGIN 26
#define TOP_MARGIN 35
#define BOTTOM_MARGIN 50
#define BOTTOM_FOOTER_MARGIN 32
#define DOC_WIDTH 560
#define DOC_HEIGHT 840

#define DOC_WIDTH_POSTCARD 288
#define DOC_HEIGHT_POSTCARD 432

static inline double radians (double degrees) {return degrees * M_PI/180;}

@interface PDFHelper () {
    BOOL done;
    NSDateFormatter *_dateFormatter;
}

@property (nonatomic, retain) UITextView *invisibleTextView;
@property (nonatomic, retain) NSMutableArray *textArray;

@end

@implementation PDFHelper

- (NSString *)applicationDocumentsDirectory
{
    NSArray *paths               = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}


- (NSString *)defaltPathWithFileName:(NSString *)fileName
{
    if (fileName && fileName.length)
    {
        NSString *directoryPath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"/PDF"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:directoryPath])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        
        NSString *path = [directoryPath stringByAppendingPathComponent:fileName];
        
        return path;
    }
    
    return nil;
}


- (NSInteger)createLetterFromLetter:(JMPackage *)package withFileName:(NSString *)fileName
{
    
    CGFloat docWidth = 0;
    CGFloat docHeight = 0;
    
    if (package.letter.type == JMLetterTypePostcard)
    {
        docWidth = DOC_WIDTH_POSTCARD;
        docHeight = DOC_HEIGHT_POSTCARD;
    } else {
        docWidth = DOC_WIDTH;
        docHeight = DOC_HEIGHT;
    }
    
    if (package.letter.type == JMLetterTypePhoto)
    {
        return 0;
    }
    
    NSString *pathString = [self defaltPathWithFileName:fileName];
    
    NSLog(@"Created letter PDF:%@", pathString);
    
    BOOL allowCopy     = YES;
    BOOL allowPrint    = YES;
    NSString *password = nil;
    NSString *content  = (package.letter.message.length > 0)?package.letter.message : @"";
    UIFont *font       = [UIFont systemFontOfSize:14];
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"MM/dd/YY"];
    
    NSString *strUName = package.letter.sender.fullName;
    NSString *strUadd  = package.letter.sender.street;
    NSString *strCSZ   = [NSString stringWithFormat:@"%@, %@ %@", package.letter.sender.city, package.letter.sender.state.name, package.letter.sender.zipCode];
    
    NSString *strIName = package.letter.recipient.fullName;
    NSString *strIadd  = package.letter.recipient.institution.address;
    NSString *strId    = package.letter.recipient.identifier;
    NSString *strICSZ  = [NSString stringWithFormat:@"%@, %@ %@", package.letter.recipient.institution.city, package.letter.recipient.institution.state.name, package.letter.recipient.institution.zipCode];
    
    _textArray         = [[NSMutableArray alloc] init];
    _invisibleTextView      = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    _invisibleTextView.font = font;
    
    if (package.letter.type == JMLetterTypeMail)
    {
        content = [NSString stringWithFormat:@" From:\n  %@\n  %@\n  %@\n  %@\n\n To:\n  %@\n  %@\n  %@\n  %@\n\n\n%@\n",
                   strUName,
                   strUadd,
                   strCSZ,
                   [_dateFormatter stringFromDate:package.letter.createdAt],
                   
                   strIName,
                   strId,
                   strIadd,
                   strICSZ,
                   
                   content];
    }
    else if (package.letter.type == JMLetterTypePostcard)
    {
        font       = [UIFont systemFontOfSize:12];
        
        content = [NSString stringWithFormat:@" From:\n  %@\n  %@\n  %@\n  %@\n\n To:\n  %@\n  %@\n  %@\n  %@\n\n%@\n",
                   strUName,
                   strUadd,
                   strCSZ,
                   [_dateFormatter stringFromDate:package.letter.createdAt],
                   
                   strIName,
                   strId,
                   strIadd,
                   strICSZ,
                   
                   content];
    }
    else {
        NSString *lesstab = @"\t \t \t ";
        
        content = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n\n%@\n%@\n%@\n%@\n\n\n%@  %@\n\n\t\t\t\t\t\t\t%@\n",
                   strUName,
                   strUadd,
                   strCSZ,
                   package.letter.createdAt,
                   strIName,
                   strId,
                   strIadd,
                   strICSZ,
                   lesstab,
                   @"",
                   content];
    }
    
    [_textArray setArray:[content componentsSeparatedByString:@" "]];
    [_invisibleTextView setText:content];
    
    CGContextRef pdfContext;
    CFStringRef path;
    CFURLRef url;
    CFStringRef passwordString          = (CFStringRef)CFBridgingRetain(password);
    CGRect pageRect                     = CGRectMake(0, 0, docWidth, docHeight);
    CFMutableDictionaryRef myDictionary = NULL;
    const char *filename                = [pathString UTF8String];
    path = CFStringCreateWithCString (NULL, filename,
                                      kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath (NULL, path,
                                         kCFURLPOSIXPathStyle, 0);
    myDictionary = CFDictionaryCreateMutable(NULL, 0,
                                             &kCFTypeDictionaryKeyCallBacks,
                                             &kCFTypeDictionaryValueCallBacks);
    CFDictionarySetValue(myDictionary, kCGPDFContextTitle, (CFStringRef)path);
    CFDictionarySetValue(myDictionary, kCGPDFContextCreator, (CFStringRef)path);
    
    if (password != nil)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextOwnerPassword, passwordString);
    }
    
    if (password != nil)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextUserPassword, passwordString);
    }
    
    if (!allowCopy)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextAllowsCopying, kCFBooleanFalse);
    }
    
    if (!allowPrint)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextAllowsPrinting, kCFBooleanFalse);
    }
    
    pdfContext = CGPDFContextCreateWithURL (url, &pageRect, myDictionary);
    
    CFRelease(myDictionary);
    CFRelease(url);
    
    NSInteger i = 0;
    
    
    // Add image for Postcard PDF
    if (package.letter.type == JMLetterTypePostcard)
    {
        CGContextBeginPage (pdfContext, &pageRect);
        
        UIGraphicsPushContext(pdfContext);
        CGContextSaveGState(pdfContext);
        
        UIImage *image = [(JMAttachments *)[package.letter.attachments anyObject] image];
        
        if (image.size.width > image.size.height)
        {
            CGContextTranslateCTM (pdfContext, 0, docHeight);
            CGContextRotateCTM (pdfContext, radians(-90.));
            [image drawInRect:CGRectMake(0, 0, docHeight, docWidth)];
        } else {
            CGRect bounds = CGRectMake(0,
                                       0,
                                       docWidth,
                                       docHeight);
            CGContextTranslateCTM(pdfContext, 0, bounds.origin.y);
            CGContextScaleCTM(pdfContext, 1, -1);
            CGContextTranslateCTM(pdfContext, 0, -(bounds.origin.y + bounds.size.height));
            
            CGRect frame = AVMakeRectWithAspectRatioInsideRect(image.size, CGRectMake(0, 0, docWidth, docHeight));
            
            NSLog(@"%@", NSStringFromCGRect(frame));
            
            [image drawInRect:frame];
        }
        
        CGContextRestoreGState(pdfContext);
        i++;
        UIGraphicsPopContext();
        
        CGContextEndPage (pdfContext);
    }
    
    do {
        CGContextBeginPage (pdfContext, &pageRect);
        
        CGRect bounds = CGRectMake(LEFT_MARGIN,
                                   TOP_MARGIN,
                                   docWidth - RIGHT_MARGIN - LEFT_MARGIN,
                                   docHeight - TOP_MARGIN - BOTTOM_MARGIN);
        
        UIGraphicsPushContext(pdfContext);
        CGContextSaveGState(pdfContext);
        CGContextTranslateCTM(pdfContext, 0, bounds.origin.y);
        CGContextScaleCTM(pdfContext, 1, -1);
        CGContextTranslateCTM(pdfContext, 0, -(bounds.origin.y + bounds.size.height));
        
        // On 18/08/2015, at 5:21 pm, Walter  Price wrote: I think the text on the back side should be horizontal instead of portrait
        if (package.letter.type == JMLetterTypePostcard)
        {
            CGContextTranslateCTM (pdfContext, 0, docHeight);
            CGContextRotateCTM (pdfContext, radians(-90.));
            
            bounds = CGRectMake(LEFT_MARGIN, TOP_MARGIN, docHeight-RIGHT_MARGIN, docWidth);
        }
        
        
        if ([_invisibleTextView.text length] > 0)
        {
            NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            [style setAlignment:NSTextAlignmentJustified];
            NSDictionary *attr = [NSDictionary dictionaryWithObjectsAndKeys:
                                  style, NSParagraphStyleAttributeName,
                                  font, NSFontAttributeName, nil];
            
            [[self stringToDrawWithFont:font] drawInRect:bounds withAttributes:attr];
        }
        
        
        
        // Add footer
        if (i == 0 && package.letter.type != JMLetterTypePostcard)
        {
            UIImage *image = [UIImage imageNamed:@"pdf_footer"];
            
            CGRect boundsimage = CGRectMake(0,
                                            docHeight-BOTTOM_MARGIN-TOP_MARGIN,
                                            docWidth,
                                            BOTTOM_MARGIN);
            CGContextTranslateCTM(pdfContext, 0, boundsimage.origin.y);
            CGContextScaleCTM(pdfContext, 1, -1);
            CGContextTranslateCTM(pdfContext, 0, -(boundsimage.origin.y + boundsimage.size.height));
            
            [image drawInRect:CGRectMake(0, docHeight-BOTTOM_MARGIN-TOP_MARGIN, docWidth, BOTTOM_MARGIN)];
        }
        
        CGContextRestoreGState(pdfContext);
        i++;
        UIGraphicsPopContext();
        
        CGContextEndPage (pdfContext);
        
        
    }
    while (!done);
    
    //Finish
    
    CGContextRelease (pdfContext);
    CFRelease(path);
    
    return i;
}



- (void)createContactFromLetter:(JMPackage *)package withFileName:(NSString *)fileName
{
    NSString *pathString = [self defaltPathWithFileName:fileName];
    
    NSLog(@"%@", pathString);
    
    _invisibleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    _textArray         = [[NSMutableArray alloc] init];
    
    NSInteger photos;
    
    NSString *content  = (package.letter.message.length > 0)?package.letter.message : @"";
    NSString *password = nil;
    BOOL allowCopy     = YES;
    BOOL allowPrint    = YES;
    UIFont *font       = [UIFont systemFontOfSize:14];
    
    photos = package.letterPhotos;
    
    content = [NSString stringWithFormat:@"\t \t Inmates Mailing Info \n \n First Name: %@\n Last Name: %@ \n Inmate ID: %@ \n Bed Assignment: %@ \n Institution Name: %@ \n Institution Address: %@ \n City: %@ \n State: %@ \n Zip Code: %@ \n \n \n \t \t Sender's info \n \n First Name: %@ \n Last Name: %@ \n Street Address: %@ \n City: %@ \n State: %@ \n Zip Code:%@ \n Email: %@ \n \n  \t \t Order's Info \n \n package.letter: %ld  \n No of Photos:%ldd",
               package.letter.recipient.firstName,
               package.letter.recipient.lastName,
               package.letter.recipient.identifier,
               package.letter.recipient.bed,
               package.letter.recipient.institution.name,
               package.letter.recipient.institution.address,
               package.letter.recipient.institution.city,
               package.letter.recipient.institution.state.name,
               package.letter.recipient.institution.zipCode,
               package.letter.sender.firstName,
               package.letter.sender.lastName,
               package.letter.sender.street,
               package.letter.sender.city,
               package.letter.sender.state.name,
               package.letter.sender.zipCode,
               package.letter.sender.email,
               (long)package.letterPages,
               (long)photos];
    
    [_textArray setArray:[content componentsSeparatedByString:@" "]];
    [_invisibleTextView setText:content];
    
    CGContextRef pdfContext;
    CFStringRef path;
    CFURLRef url;
    CFStringRef passwordString          = (CFStringRef)CFBridgingRetain(password);
    CGRect pageRect                     = CGRectMake(0, 0, DOC_WIDTH, DOC_HEIGHT);
    CFMutableDictionaryRef myDictionary = NULL;
    const char *filename                = [pathString UTF8String];
    
    path = CFStringCreateWithCString (NULL, filename,
                                      kCFStringEncodingUTF8);
    
    url = CFURLCreateWithFileSystemPath (NULL, path,
                                         kCFURLPOSIXPathStyle, 0);
    
    myDictionary = CFDictionaryCreateMutable(NULL, 0,
                                             &kCFTypeDictionaryKeyCallBacks,
                                             &kCFTypeDictionaryValueCallBacks);
    CFDictionarySetValue(myDictionary, kCGPDFContextTitle, (CFStringRef)path);
    CFDictionarySetValue(myDictionary, kCGPDFContextCreator, (CFStringRef)path);
    
    if (password != nil)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextOwnerPassword, passwordString);
    }
    
    if (password != nil)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextUserPassword, passwordString);
    }
    
    if (!allowCopy)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextAllowsCopying, kCFBooleanFalse);
    }
    
    
    if (!allowPrint)
    {
        CFDictionarySetValue(myDictionary, kCGPDFContextAllowsPrinting, kCFBooleanFalse);
    }
    
    pdfContext = CGPDFContextCreateWithURL (url, &pageRect, myDictionary);
    
    CFRelease(myDictionary);
    CFRelease(url);
    
    int i = 0;
    
    do {
        CGContextBeginPage (pdfContext, &pageRect);
        
        CGRect bounds = CGRectMake(LEFT_MARGIN,
                                   TOP_MARGIN,
                                   DOC_WIDTH - RIGHT_MARGIN - LEFT_MARGIN,
                                   DOC_HEIGHT - TOP_MARGIN - BOTTOM_MARGIN);
        
        UIGraphicsPushContext(pdfContext);
        CGContextSaveGState(pdfContext);
        CGContextTranslateCTM(pdfContext, 0, bounds.origin.y);
        CGContextScaleCTM(pdfContext, 1, -1);
        CGContextTranslateCTM(pdfContext, 0, -(bounds.origin.y + bounds.size.height));
        
        if ([_invisibleTextView.text length] > 0)
        {
            NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            [style setAlignment:NSTextAlignmentJustified];
            NSDictionary *attr = [NSDictionary dictionaryWithObjectsAndKeys:
                                  style, NSParagraphStyleAttributeName,
                                  font, NSFontAttributeName, nil];
            
            [[self stringToDrawWithFont:font] drawInRect:bounds withAttributes:attr];
        }
        
        CGContextRestoreGState(pdfContext);
        i++;
        UIGraphicsPopContext();
        
        CGContextEndPage (pdfContext);
    }
    
    while (!done);
    
    CGContextRelease (pdfContext);
    CFRelease(path);
}


- (NSString *)stringToDrawWithFont:(UIFont *)font
{
    CGSize tempSize;
    CGSize theTextSize;
    tempSize.width  = DOC_WIDTH - RIGHT_MARGIN - LEFT_MARGIN;
    tempSize.height = 10000000; // DOC_HEIGHT - TOP_MARGIN - BOTTOM_MARGIN;
    
    CGRect rect = CGRectZero;
    rect = [_invisibleTextView.text boundingRectWithSize:tempSize
                                                 options:(NSStringDrawingUsesLineFragmentOrigin)
                                              attributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName]
                                                 context:nil];
    theTextSize = rect.size;
    
    if (theTextSize.height > DOC_HEIGHT - TOP_MARGIN - BOTTOM_MARGIN)
    {
        BOOL match                 = NO;
        int wordCount              = 0;
        float currentHeight        = 0.0f;
        float previousHeight       = 0.0f;
        NSString *returnString     = [[NSString alloc] init];
        NSString *tempReturnString = [[NSString alloc] init];
        //NSLog(@"text array is  %@", _textArray);
        
        do {
            if ([_textArray count] > wordCount + 1)
            {
                returnString = (wordCount > 0)?[returnString stringByAppendingString:[NSString stringWithFormat:@" %@", [_textArray objectAtIndex:wordCount]]] :[NSString stringWithFormat:@"%@", [_textArray objectAtIndex:wordCount]];
                //NSLog(@"ret %@", returnString);
                
                CGRect rect = CGRectZero;
                rect = [returnString boundingRectWithSize:CGSizeMake(DOC_WIDTH - RIGHT_MARGIN - LEFT_MARGIN, DOC_HEIGHT - TOP_MARGIN - BOTTOM_MARGIN)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin)
                                               attributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName]
                                                  context:nil];
                theTextSize = rect.size;
                
                currentHeight = theTextSize.height;
                
                if (theTextSize.height >= DOC_HEIGHT - TOP_MARGIN - BOTTOM_MARGIN)
                {
                    match = YES;
                }
                
                if ((currentHeight == previousHeight) && (currentHeight > 700.0f)) // sometimes the above is not accurate and the height is shorter than we think (because of large fonts)
                {
                    match = YES;
                    wordCount--;
                    returnString = tempReturnString;
                }
                
                wordCount++;
            }
            else {
                match = YES;
            }
            
            previousHeight   = theTextSize.height;
            tempReturnString = returnString;
        }
        
        while (!match);
        
        for (int i = 0; i < wordCount; i++)
        {
            [_textArray removeObjectAtIndex:0];
        }
        
        if ([_textArray count])
        {
            _invisibleTextView.text = [_textArray componentsJoinedByString:@" "];
        }
        else {
            _invisibleTextView.text = @"";
            done                    = YES;
        }
        
        if ([returnString length] == 0)
        {
            returnString = @" ";
        }
        
        return returnString;
    }
    else {
        done = YES;
        return _invisibleTextView.text;
    }
    
    return _invisibleTextView.text;
}


@end