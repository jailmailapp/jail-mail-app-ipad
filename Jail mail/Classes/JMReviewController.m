//
//  JMReviewController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 23/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMReviewController.h"

#import "JMFinancesCodeRequest.h"
#import "JMInmate.h"
#import "JMNavigationController.h"
#import "JMPackage.h"
#import "JMUser.h"
#import "UIImageView+Avatar.h"
#import "UIViewController+ImageBackButton.h"
#import "MRProgress.h"
#import <MRProgress/MRProgressView+AFNetworking.h>

#import "JMPreviewPhotoController.h"
#import "JMRewardsController.h"
#import "JMSendingController.h"
#import "JMTermsOfServiceController.h"
#import "MJPPdfViewer.h"
#import "PDFHelper.h"
#import "PSTAlertController.h"

@interface TitleCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *totalLabel;
@property (nonatomic, retain) IBOutlet UILabel *couponLabel;
@property (nonatomic, retain) IBOutlet UILabel *rewardsLabel;
@property (nonatomic, retain) IBOutlet UILabel *cardLabel;
@property (nonatomic, retain) IBOutlet UIButton *couponButton;

@end

@implementation TitleCell
@end

@interface PreviewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIButton *messageButton;
@property (nonatomic, retain) IBOutlet UIButton *photoButton;

@end

@implementation PreviewCell
@end

@interface ProfileCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *senderAvatar;
@property (strong, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *recipientAvatar;
@property (strong, nonatomic) IBOutlet UILabel *recipientNameLabel;
@property (nonatomic, retain) IBOutlet UIButton *loyaltyButton;

@end

@implementation ProfileCell
@end

@interface PriceCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *pricePhoto;
@property (nonatomic, retain) IBOutlet UILabel *priceMessage;
@property (nonatomic, retain) IBOutlet UILabel *countPhoto;
@property (nonatomic, retain) IBOutlet UILabel *countMessage;

@end

@implementation PriceCell
@end

@interface JMReviewController ()<UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    JMPackage *_package;
    UIAlertView *_couponAlert;
}

@property (strong, nonatomic) IBOutlet UITableView *table;

@end

@implementation JMReviewController

+ (NSString *)storyboardID
{
    return @"idReviewController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Review", @"Review");
    
    [self setCustomBackButton];
    
    [self observeKeyboardForTable:self.table];
    
    self.navigationItem.rightBarButtonItem = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
    [self updateUI];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_package.priceTotal ==  0) {
        [self updateTotalPrice];
    }
}


#pragma mark -

- (void)updateUI
{
    [self.table reloadData];
}


- (void)setLetterPackage:(JMPackage *)package
{
    _package = package;
    
    [self updateUI];
}


- (IBAction)checkoutTap:(id)sender
{
    id controller = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults valueForKey:@"kTermsOfService"] == NO)
    {
        controller = [JMTermsOfServiceController controller];
        [controller setLetterPackage:_package];
    }
    else {
        controller = [JMSendingController controller];
        
        if (_package.letter.sentAt == nil && _package.letter.letterID == nil)
        {
            [controller newLetterPackage:_package];
        } else {
            [controller continueLetterPackage:_package];
        }
        
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}


- (void)previewMessageTap:(id)sender
{
    PDFHelper *pdfHelper = [[PDFHelper alloc] init];
    
    MJPPdfViewer *pdfViewer = [[MJPPdfViewer alloc] init];
    pdfViewer.path   = [pdfHelper defaltPathWithFileName:kDefLetterName];
    pdfViewer.page   = 1;
    pdfViewer.margin = 10.0;
    
    NSString *title = @"Preview";
    
    if (_package.letter.type == JMLetterTypeMail) {
        title = NSLocalizedString(@"Preview Letter", @"Preview Letter");
    } else {
        title = NSLocalizedString(@"Preview Postcard", @"Preview Postcard");
    }
    
    pdfViewer.title = title;
    
    JMNavigationController *navigation = [[JMNavigationController alloc] initWithRootViewController:pdfViewer];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}


- (void)previewPhotoTap:(id)sender
{
    JMPreviewPhotoController *previewPhotoController = [JMPreviewPhotoController controller];
    JMNavigationController *navigation               = [[JMNavigationController alloc] initWithRootViewController:previewPhotoController];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
    [previewPhotoController setLetter:_package.letter];
}


- (void)loyaltyTap:(id)sender
{
    JMRewardsController *loyaltyController = [JMRewardsController controller];
    JMNavigationController *navigation     = [[JMNavigationController alloc] initWithRootViewController:loyaltyController];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}


- (void)updateTotalPrice
{
    NSString *UUID = [NSString stringWithFormat:@"%@", [UIDevice currentDevice].identifierForVendor.UUIDString];
    
    if (UUID.length <= 4) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:NSLocalizedString (@"Error getting UUID", @"Error getting UUID")
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        return;
    }
    
    AFURLConnectionOperation *operation = [JMFinancesCodeRequest totalPriceWithDeviceID:UUID
                                          package:_package
                                          success:^(id response) {
                                              _package.priceTotal = [[response valueForKey:@"priceTotal"] floatValue];
                                              _package.priceRewards = [[response valueForKey:@"priceRewards"] floatValue];
                                              _package.pricePhotos = [[response valueForKey:@"pricePhotos"] floatValue];
                                              _package.priceMessages = [[response valueForKey:@"priceMessages"] floatValue];
                                              _package.priceDiscount = [[response valueForKey:@"priceDiscount"] floatValue];
                                              
                                              [self updateUI];
                                              
                                              UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_done"]
                                                                                                             style:UIBarButtonItemStyleBordered
                                                                                                            target:self
                                                                                                            action:@selector(checkoutTap:)];
                                              self.navigationItem.rightBarButtonItem = doneButton;
                                              
                                          } failure:^(NSError *error) {
                                              
                                              self.navigationItem.rightBarButtonItem = nil;
                                              
                                              NSString *message = nil;
                                              
                                              if (error != nil) {
                                                  message = [NSString stringWithFormat:@"Error: %@", error.localizedDescription];
                                              } else {
                                                  message = [NSString stringWithFormat:@"Error: Internal Server Error"];
                                              }
                                              
                                              
                                              PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:@"Oops didn't calculate the price, click the refresh button."
                                                                                                                       message:message
                                                                                                                preferredStyle:PSTAlertControllerStyleAlert];
                                              
                                              [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Back", @"Back")
                                                                                                  style:PSTAlertActionStyleDefault
                                                                                                handler:^(PSTAlertAction *action) {
                                                                                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                                                                                }]];
                                              
                                              [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Refresh", @"Refresh")
                                                                                                  style:PSTAlertActionStyleDefault
                                                                                                handler:^(PSTAlertAction *action) {
                                                                                                    [self updateTotalPrice];
                                                                                                }]];
                                              
                                              [alertController showWithSender:nil controller:self animated:YES completion:NULL];
                                          }];
    [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgressWithUploadProgressOfOperation:operation animated:YES];
}


- (void)verificationCoupon:(NSString *)code
{
    if (code.length > 0)
    {
        [JMFinancesCodeRequest verificationCode:code
                                        package:_package
                                        success:^(id response) {
                                            NSLog (@"Verification coupon response: %@", response);
                                            
                                            _package.discountID = code;
                                            
                                            [self updateTotalPrice];
                                            
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Congratulations"
                                                                                                message:NSLocalizedString (@"Coupon applied", @"Coupon applied")
                                                                                               delegate:nil
                                                                                      cancelButtonTitle:@"OK"
                                                                                      otherButtonTitles:nil];
                                            [alertView show];
                                        } failure:^(NSError *error) {
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error verification сoupon code"
                                                                                                message:error.localizedDescription
                                                                                               delegate:nil
                                                                                      cancelButtonTitle:@"OK"
                                                                                      otherButtonTitles:nil];
                                            [alertView show];
                                        }];
    }
}


- (void)couponTapped:(id)sender
{
    _couponAlert = [[UIAlertView alloc] initWithTitle:@"Coupon Code"
                                              message:nil
                                             delegate:self
                                    cancelButtonTitle:@"Done"
                                    otherButtonTitles:nil];
    _couponAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    _couponAlert.delegate       = self;
    [_couponAlert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == _couponAlert)
    {
        [self verificationCoupon:[alertView textFieldAtIndex:0].text];
    }
}


#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 155;
    }
    else if (indexPath.row == 1) {
        if (_package.letter.type == JMLetterTypePostcard)
        {
            return 0;
        }
        else {
            return 155;
        }
    }
    else if (indexPath.row == 2) {
        return 164;
    }
    else if (indexPath.row == 3) {
        return 105;
    }
    
    return 100;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"CellIdentifier";
    
    switch (indexPath.row) {
        case 0:
            cellIdentifier = @"ProfileCellIdentifier";
            break;
        case 1:
            cellIdentifier = @"PriceCellIdentifier";
            break;
        case 2:
            cellIdentifier = @"TitleCellIdentifier";
            break;
        case 3:
            cellIdentifier = @"PreviewCellIdentifier";
            break;
        default:
            break;
    }
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell setClipsToBounds:YES];
    
    if (!cell)
    {
        [tableView registerClass:[TitleCell class] forCellReuseIdentifier:@"TitleCellIdentifier"];
        [tableView registerClass:[ProfileCell class] forCellReuseIdentifier:@"ProfileCellIdentifier"];
        [tableView registerClass:[PriceCell class] forCellReuseIdentifier:@"PriceCellIdentifier"];
        [tableView registerClass:[PreviewCell class] forCellReuseIdentifier:@"PreviewCellIdentifier"];
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    }
    
    if ([cell isKindOfClass:[ProfileCell class]])
    {
        ProfileCell *aCell = (ProfileCell *)cell;
        
        [aCell.senderAvatar setAvatarImage:_package.letter.sender.avatar];
        aCell.senderNameLabel.text = _package.letter.sender.fullName;
        [aCell.recipientAvatar setAvatarImage:_package.letter.recipient.avatar];
        aCell.recipientNameLabel.text = _package.letter.recipient.fullName;
        
        [aCell.senderAvatar circle];
        [aCell.senderAvatar blueBorder];
        [aCell.recipientAvatar circle];
        [aCell.recipientAvatar blueBorder];
        
        if (_package.priceRewards > 0)
        {
            [[aCell loyaltyButton] setHidden:NO];
            [[aCell loyaltyButton] addTarget:self action:@selector(loyaltyTap:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            [[aCell loyaltyButton] setHidden:YES];
        }
    }
    
    if ([cell isKindOfClass:[PriceCell class]])
    {
        PriceCell *aCell = (PriceCell *)cell;
        
        aCell.countMessage.layer.borderColor  = [UIColor colorWithRed:0.000 green:0.400 blue:0.824 alpha:1.000].CGColor;
        aCell.countMessage.layer.borderWidth  = 1;
        aCell.countMessage.layer.cornerRadius = aCell.countMessage.frame.size.width/2;
        aCell.countPhoto.layer.borderColor    = aCell.countMessage.layer.borderColor;
        aCell.countPhoto.layer.borderWidth    = aCell.countMessage.layer.borderWidth;
        aCell.countPhoto.layer.cornerRadius   = aCell.countMessage.layer.cornerRadius;
        
        aCell.priceMessage.text = [NSString stringWithFormat:@"$ %.2f", [_package priceMessages]];
        aCell.countMessage.text = [NSString stringWithFormat:@"%ld", (long)[_package letterPages]];
        
        aCell.pricePhoto.text = [NSString stringWithFormat:@"$ %.2f", [_package pricePhotos]];
        aCell.countPhoto.text = [NSString stringWithFormat:@"%ld", (long)[_package letterPhotos]];
    }
    
    if ([cell isKindOfClass:[TitleCell class]])
    {
        UIButton *button = [(TitleCell *) cell couponButton];
        button.layer.borderColor = [UIColor colorWithRed:0.988 green:0.322 blue:0.275 alpha:1.000].CGColor;
        button.layer.borderWidth = 1;
        [button addTarget:self action:@selector(couponTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [(TitleCell *) cell couponLabel].text  = [NSString stringWithFormat:@"$ -%.2f", [_package priceDiscount]];
        [(TitleCell *) cell rewardsLabel].text = [NSString stringWithFormat:@"$ -%.2f", [_package priceRewards]];
        [(TitleCell *) cell cardLabel].text    = _package.card.numberName;
        [(TitleCell *) cell totalLabel].text   = [NSString stringWithFormat:@"$ %.2f", [_package priceTotal]];
        
        if ([_package priceDiscount] == 0)
        {
            [(TitleCell *) cell couponLabel].textColor = [UIColor grayColor];
        }
        
        if ([_package priceRewards] == 0)
        {
            [(TitleCell *) cell rewardsLabel].textColor = [UIColor grayColor];
        }
    }
    
    if ([cell isKindOfClass:[PreviewCell class]])
    {
        [[(PreviewCell *) cell messageButton] addTarget:self action:@selector(previewMessageTap:) forControlEvents:UIControlEventTouchUpInside];
        [[(PreviewCell *) cell photoButton] addTarget:self action:@selector(previewPhotoTap:) forControlEvents:UIControlEventTouchUpInside];
        
        if (_package.letter.type == JMLetterTypePostcard)
        {
            [(PreviewCell *) cell messageButton].hidden = NO;
            [(PreviewCell *) cell messageButton].translatesAutoresizingMaskIntoConstraints = YES;
            [(PreviewCell *) cell messageButton].center = CGPointMake(cell.frame.size.width/2, cell.frame.size.height/2);
            [(PreviewCell *) cell photoButton].hidden = YES;
        }
        else if (_package.letterPages == 0) {
            [(PreviewCell *) cell messageButton].enabled = NO;
        }
        else if (_package.letterPhotos == 0) {
            [(PreviewCell *) cell photoButton].enabled = NO;
        }
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (void)observeKeyboardForTable:(UITableView *)tableView
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        id _obj = [note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect _keyboardFrame = CGRectNull;
        
        if ([_obj respondsToSelector:@selector(getValue:)])
        {
            [_obj getValue:&_keyboardFrame];
        }
        
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [tableView setContentInset:UIEdgeInsetsMake (0.f, 0.f, _keyboardFrame.size.height+10, 0.f)];
        } completion:nil];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [tableView setContentInset:UIEdgeInsetsZero];
        } completion:nil];
    }];
}

@end