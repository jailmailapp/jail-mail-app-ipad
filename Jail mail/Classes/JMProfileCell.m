//
//  JMProfileCell.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 09/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMProfileCell.h"

@implementation JMProfileCell

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self.profileView setFrame:self.bounds];
}


- (JMProfileView *)profileView
{
    if (_profileView == nil)
    {
        _profileView = [[JMProfileView alloc] initWithFrame:self.bounds];
        [self addSubview:_profileView];
    }
    
    return _profileView;
}


@end