//
//  JMTermsOfServiceController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@class JMPackage;

@interface JMTermsOfServiceController : UIViewController <StoryboardProtocol>

@property (nonatomic,retain) JMPackage *letterPackage;

@end
