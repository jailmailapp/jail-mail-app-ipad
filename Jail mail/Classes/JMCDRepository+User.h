//
//  UserRepository.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"

#import "JMUser.h"

@interface JMCDRepository (User)

- (NSArray *)allUsers;

@end