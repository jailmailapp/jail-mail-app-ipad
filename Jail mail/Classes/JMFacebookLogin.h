//
//  JMFacebookLogin.h
//  Jail mail
//
//  Created by Oleksiy Kovtun on 3/27/15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^fbLogInCompletionBlock) (NSError *error);
typedef void (^fbLogInCancelBlock) ();

/**
 * The `JMFacebookLogin` class is responsible for authentication on Facebook
 */
@interface JMFacebookLogin : NSObject

+ (instancetype)sharedInstance;

/**
 * \param completionBlock The block which will be executed on the completion of the authentication. The authentication has finished and is successful if `error` is nil.
 * \param cancelBlock The block which will be executed when user taps on close button
 */
- (void)logInWithCompletionBlock:(fbLogInCompletionBlock)completionBlock
                     cancelBlock:(fbLogInCancelBlock)cancelBlock;

@end
