//
//  JMPreviewPhotoController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 09/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPreviewPhotoController.h"

#import "JMLetter.h"
#import "JMAttachments.h"

@interface JMPreviewPhotoController () {
    JMLetter *_letter;
    NSMutableArray *_images;
}


@property (nonatomic,retain) IBOutlet UIPageControl *pageControl;


@end

@implementation JMPreviewPhotoController

+ (NSString *)storyboardID
{
    return @"idPreviewPhotoController";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Preview Photo", @"Preview Photo");
    
    _pageControl.hidesForSinglePage = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.navigationController isBeingPresented])
    {
        UIBarButtonItem *flexibleButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done")
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(dismiss:)];
        self.navigationItem.leftBarButtonItem = flexibleButton;
    }
    
    _images = [NSMutableArray array];
    
    for (JMAttachments *att in _letter.attachments) {
        [_images addObject:att.image];
    }
    
    self.pageControl.numberOfPages = _images.count;
    self.pageControl.currentPage = 0;
}

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setLetter:(JMLetter *)letter
{
    _letter = letter;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _images.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kCellIdentifier = @"CellIdentifier";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    UIImageView *view = (UIImageView *)[cell viewWithTag:1000];
    view.contentMode = UIViewContentModeScaleAspectFit;
    view.image = [_images objectAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


@end
