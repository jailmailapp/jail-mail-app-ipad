//
//  JMRootViewController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 19/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMRootViewController.h"

@interface JMRootViewController ()

@end

@implementation JMRootViewController

+ (NSString *)storyboardID
{
    return @"idRootViewController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (BOOL)splitViewController:(UISplitViewController*)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return NO;
}

@end