//
//  JMCDRepository+Recipient.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+Recipient.h"

@implementation JMCDRepository (Recipient)

- (NSArray *)allRecipient
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMInmate entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}

@end
