//
//  JMCDEntityFactory+Inmate.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMInmate.h"

@interface JMCDEntityFactory (Inmate)

- (JMInmate *)newInmate;

@end
