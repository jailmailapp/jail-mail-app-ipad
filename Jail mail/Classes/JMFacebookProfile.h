//
//  JMFacebookProfile.h
//  Jail mail
//
//  Created by Oleksiy Kovtun on 4/1/15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    ProfilePictureTypeDefault,
    ProfilePictureTypeSmall,
    ProfilePictureTypeNormal,
    ProfilePictureTypeAlbum,
    ProfilePictureTypeLarge,
    ProfilePictureTypeSquare
} ProfilePictureType;

/**
 * This class can be used for downloading profile information of the currently logged user and his profile picture
 */
@interface JMFacebookProfile : NSObject

/**
 * Downloads logged user's profile information.
 * \param completion The block which will be execuedd on the completion of the request or if there's an error
 */
+ (void)downloadProfileInformationWithCompletion:(void (^)(NSError *error, id profile))completion;

/**
 * Downloads logged user's profile picture.
 * \param type determines size of the profile picture. Can be small, normal, album, large, square or default
 * \param completion The block which will be execuedd on the completion of the request or if there's an error
 */
+ (void)downloadProfilePictureOfType:(ProfilePictureType)type
                          completion:(void (^)(NSError *error, UIImage *profilePicture))completion;

@end
