//
//  JMСompletedLetter.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMLetter.h"
#import "JMCard.h"

typedef NS_ENUM(NSUInteger, JMTypePayment)
{
    JMTypePaymentCard = 0,
    JMTypePaymentPayPal = 1,
    JMTypePaymentApplePay = 2
};

@class JMLetterManager;

@interface JMPackage : NSObject

@property (nonatomic, retain) JMCard *card;
@property (nonatomic, retain) JMLetter *letter;

@property (nonatomic, retain) NSString *discountID;

@property (nonatomic) CGFloat priceTotal;
@property (nonatomic) CGFloat priceRewards;
@property (nonatomic) CGFloat pricePhotos;
@property (nonatomic) CGFloat priceMessages;
@property (nonatomic) CGFloat priceDiscount;

@property (nonatomic) JMTypePayment typePayment;

- (instancetype)initWithLetter:(JMLetter *)aLetter;

- (NSInteger)letterPages;
- (NSInteger)letterPhotos;
- (NSInteger)letterSubmittedPhotos;

@end
