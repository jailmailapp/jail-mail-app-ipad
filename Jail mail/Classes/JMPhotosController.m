//
//  JMPhotosController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 29/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPhotosController.h"

#import "JMAttachments.h"
#import "JMCDEntityFactory+Attachments.h"
#import "JMCDRepository.h"
#import "JMPhotoCell.h"
#import <MRProgress/MRProgress.h>

#import "QBAssetsViewController.h"
#import "TOCropViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>

const CGFloat cropGuideWidth  = 300.0f;
const CGFloat cropGuideHeight = 200.0f;

// -----------------------------------------------------------

@interface JMPhotosController ()<
UICollectionViewDataSource,
UICollectionViewDelegate,
EditableCellDelegate,
UINavigationControllerDelegate,
UIActionSheetDelegate,
UIImagePickerControllerDelegate,
TOCropViewControllerDelegate,
QBImagePickerControllerDelegate>
{
    CGRect _frameDefault;
    NSArray *selectedImage;
    QBAssetsViewController *controller;
    NSInteger _index;
}

@property (strong, nonatomic) UICollectionView *photosCollection;

@end

@implementation JMPhotosController

- (void)loadView
{
    [super loadView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    self.photosCollection = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    [self.photosCollection registerClass:[JMPhotoCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    self.photosCollection.backgroundColor = [UIColor clearColor];
    self.photosCollection.delegate        = self;
    self.photosCollection.dataSource      = self;
    [self.view addSubview:self.photosCollection];
    
    self.photos = [NSMutableArray array];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setFrame:_frameDefault];
}


- (void)setFrame:(CGRect)rect
{
    _frameDefault = rect;
    
    self.view.frame = _frameDefault;
    
    [self.photosCollection setFrame:CGRectMake(0, 0, _frameDefault.size.width, _frameDefault.size.height)];
}


- (void)setPhotos:(NSMutableArray *)photos
{
    if (photos != nil)
    {
        _photos = photos;
    }
    
    [self reloadData];
}


- (void)addPhoto:(UIImage *)image
{
    JMAttachments *attachments = [[JMCDEntityFactory newFactory] newAttachments];
    [attachments setImage:image];
    [self.photos insertObject:attachments atIndex:0];
}


- (void)deleteCellAttachments:(JMAttachments *)attachments
{
    JMCDRepository *repository = [JMCDRepository newRepository];
    [repository deleteEntity:attachments];
    [repository save];
    
    [self.photos removeObject:attachments];
    
    if ([_delegate respondsToSelector:@selector(photosDidUpdated:)])
    {
        [_delegate photosDidUpdated:self];
    }
    
    [self.photosCollection reloadData];
}


- (IBAction)changeAvatarTap
{
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate                    = self;
    imagePickerController.allowsMultipleSelection     = YES;
    //imagePickerController.maximumNumberOfSelection    = 20;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    imagePickerController.filterType                  = QBImagePickerControllerFilterTypePhotos;
    
    [self.view.window.rootViewController presentViewController:imagePickerController animated:YES completion:NULL];
}

- (void)qb_imagePickerController:(QBImagePickerController *)pickerController controller:(QBAssetsViewController *)aController selectedAsset:(ALAsset *)asset
{
    
    
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    CGImageRef iref            = [rep fullResolutionImage];
    UIImage *image             = [UIImage imageWithCGImage:iref];
    
    controller = aController;
    
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    [pickerController presentViewController:cropViewController animated:YES completion:^{
        
    }];
}


- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    [self reloadData];
    
    if ([_delegate respondsToSelector:@selector(photosDidUpdated:)])
    {
        [_delegate photosDidUpdated:self];
    }
    
    [imagePickerController dismissViewControllerAnimated:YES completion:^{
    }];
}


- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [imagePickerController dismissViewControllerAnimated:YES completion:NULL];
}


- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self addPhoto:image];
    
    [cropViewController dismissViewControllerAnimated:NO completion:nil];
}


- (void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    [controller cancelLastSelectedItemIndexPath];
    
    [cropViewController dismissViewControllerAnimated:NO completion:^{
        ;
    }];
}

- (void)reloadData
{
    [self.photosCollection reloadData];
    
    for (JMAttachments *attachments in self.photos)
    {
        if (attachments.thumbnail == nil)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [attachments createThumbnail];
                
                dispatch_async (dispatch_get_main_queue (), ^{
                    [self reloadData];
                });
            });
            
            return;
        }
    }
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.editing)
    {
        return self.photos.count+1;
    }
    
    return self.photos.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kCellIdentifier = @"CellIdentifier";
    
    JMPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    
    if ((indexPath.row == 0) && self.editing)
    {
        cell.attachments       = nil;
        cell.photo.image       = [UIImage imageNamed:@"icon_Add_photo"];
        cell.photo.contentMode = UIViewContentModeCenter;
        [cell setStyle:UITableViewCellEditingStyleNone animated:NO];
        
        return cell;
    }
    
    JMAttachments *attachments = nil;
    
    if (self.editing)
    {
        attachments = [self.photos objectAtIndex:indexPath.row-1];
        [cell setStyle:UITableViewCellEditingStyleDelete animated:NO];
    }
    else {
        attachments = [self.photos objectAtIndex:indexPath.row];
    }
    
    cell.attachments = attachments;
    cell.photo.image = attachments.thumbnail;
    
    cell.photo.contentMode = UIViewContentModeScaleAspectFill;
    
    return cell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.row == 0) && self.editing)
    {
        [self changeAvatarTap];
    }
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = floorf(collectionView.frame.size.width/3.5);
    
    return CGSizeMake(width, floorf(width/6*4));
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat insets = floorf((collectionView.frame.size.width/3 - collectionView.frame.size.width/3.8)/2);
    
    return UIEdgeInsetsMake(0, insets, 0, insets);
}


@end