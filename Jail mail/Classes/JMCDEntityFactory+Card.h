//
//  JMCDEntityFactory+Card.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 24/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMCard.h"

@interface JMCDEntityFactory (Card)

- (JMCard *)newCard;

@end
