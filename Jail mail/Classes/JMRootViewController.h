//
//  JMRootViewController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 19/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@interface JMRootViewController : UISplitViewController <StoryboardProtocol, UISplitViewControllerDelegate>

@end
