//
//  UIViewController+Storyboard.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 02/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StoryboardProtocol <NSObject>

@required
+ (NSString *)storyboardID;

@end

@protocol XibProtocol<NSObject>

@required
+ (NSString *)xibID;

@end

@interface UIViewController (CustomInit)

+ (id)controller;

@end
