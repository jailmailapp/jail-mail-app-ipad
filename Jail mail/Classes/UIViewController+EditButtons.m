//
//  UIViewController+EditButtons.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UIViewController+EditButtons.h"

@implementation UIViewController (EditButtons)


- (void)updateEditButtonsWithEditing:(BOOL)isEditin
{
    if (isEditin) {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_done"]
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(editTap:)];
        self.navigationItem.rightBarButtonItem = editButton;
    } else {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_edit"]
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(editTap:)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
}


- (void)editTap:(id)sender
{
    self.editing = !self.editing;
    [self updateEditButtonsWithEditing:self.editing];
}


@end
