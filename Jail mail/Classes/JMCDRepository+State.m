//
//  JMCDRepository+State.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 20/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+State.h"

@implementation JMCDRepository (State)

- (NSArray *)allState
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMState entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}

@end
