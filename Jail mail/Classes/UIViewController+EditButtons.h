//
//  UIViewController+EditButtons.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (EditButtons)

- (void)updateEditButtonsWithEditing:(BOOL)isEditin;

@end
