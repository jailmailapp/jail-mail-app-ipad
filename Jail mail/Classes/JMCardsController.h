//
//  JMCardsViewController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 12/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMGalleryController.h"

@class JMLetter;

@interface JMCardsController : JMGalleryController

- (void)setLetter:(JMLetter *)letter;

@end
