//
//  JMSenderCreateController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMSenderCreateController.h"

#import "JMCDEntityFactory+User.h"
#import "JMCDRepository+State.h"
#import "JMFacebookLogin.h"
#import "JMFacebookProfile.h"
#import "JMGoogleSignIn.h"
#import "JMSenderEditController.h"

@interface JMSenderCreateController ()

@property (nonatomic, strong) IBOutlet UIButton *createTap;
@property (nonatomic, strong) IBOutlet UIButton *signGoogleTap;
@property (nonatomic, strong) IBOutlet UIButton *signFacebookTap;

@end

@implementation JMSenderCreateController

+ (NSString *)storyboardID
{
    return @"idSenderCreateController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Sign In", @"Sign In");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.navigationController isBeingPresented])
    {
        UIBarButtonItem *flexibleButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_close"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(dismiss:)];
        self.navigationItem.leftBarButtonItem = flexibleButton;
    }
}

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)createTap:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:^{
        JMSenderEditController *createSenderController = [JMSenderEditController controller];
        [[self.parent navigationController] pushViewController:createSenderController animated:YES];
    }];
}


- (void)createSenderWithUser:(JMUser *)user
{
    [self dismissViewControllerAnimated:NO completion:^{
        JMSenderEditController *createSenderController = [JMSenderEditController controller];
        [createSenderController setSender:user];
        [[self.parent navigationController] pushViewController:createSenderController animated:YES];
    }];
}


- (IBAction)signGoogleTap:(id)sender
{
    [[JMGoogleSignIn sharedInstance] logInPresentViewController:self completionBlock:^(GIDGoogleUser *aUser, NSError *error) {
        if (error != nil)
        {
            NSString *errorMessage = [NSString stringWithFormat:@"Error: %@", error.localizedDescription];
            
            [[[UIAlertView alloc] initWithTitle:@"Failure Log In"
                                        message:errorMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        } else {
            GIDProfileData *person = (GIDProfileData *)aUser.profile;
            
            JMCDRepository *repository = [JMCDRepository newRepository];
            JMUser *user = [[JMCDEntityFactory newFactory] newUser];
            [repository addEntity:user];
            
            if (person.name)
            {
                user.firstName = person.name;
            }
            
            if (person.email)
            {
                user.email = person.email;
            }
            
            if ([person hasImage])
            {
                NSData *data = [[NSData alloc] initWithContentsOfURL:[person imageURLWithDimension:1]];
                user.avatar = [UIImage imageWithData:data];
            }
            
            [self createSenderWithUser:user];
        }
    }];
}


- (IBAction)signFacebookTap:(id)sender
{
    [[JMFacebookLogin sharedInstance] logInWithCompletionBlock:^(NSError *error) {
        if (error != nil)
        {
            NSString *errorMessage = [NSString stringWithFormat:@"Failed to log you in.\nError: %@", error.localizedDescription];
            
            [[[UIAlertView alloc] initWithTitle:@"Failure"
                                        message:errorMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
            
            return;
        }
        
        [self downloadProfileFacebook];
        
    } cancelBlock:^{
        NSLog (@"Cancelled FB login");
    }];
}

- (void)downloadProfileFacebook
{
    [JMFacebookProfile downloadProfileInformationWithCompletion:^(NSError *error, id result) {
        NSLog (@"JMFacebookProfile %@", result);
        
        NSDictionary *userData = (NSDictionary *)result;
        
        if (result)
        {
            JMCDRepository *repository = [JMCDRepository newRepository];
            JMUser *user = [[JMCDEntityFactory newFactory] newUser];
            [repository addEntity:user];
            
            if ([result objectForKey:@"email"])
            {
                user.email = [result objectForKey:@"email"];
            }
            
            if ([result objectForKey:@"first_name"])
            {
                user.firstName = [result objectForKey:@"first_name"];
            }
            
            if ([result objectForKey:@"last_name"])
            {
                user.lastName = [result objectForKey:@"last_name"];
            }
            
            if ([result objectForKey:@"location"])
            {
                user.city = [[result objectForKey:@"location"] objectForKey:@"name"];
            }
            
            if ([result objectForKey:@"id"])
            {
                NSString *facebookID = userData[@"id"];
                NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
                
                if (pictureURL)
                {
                    NSData *data = [[NSData alloc] initWithContentsOfURL:pictureURL];
                    user.avatar = [UIImage imageWithData:data];
                }
            }
            
            [self createSenderWithUser:user];
        } else {
            NSString *errorMessage = [NSString stringWithFormat:@"Failed download profile info.\nError: %@", error.localizedDescription];
            
            [[[UIAlertView alloc] initWithTitle:@"Failure"
                                        message:errorMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
    }];
}

@end