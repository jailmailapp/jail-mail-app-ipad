//
//  JMSenderEditController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMSenderEditController.h"

#import "JMCDEntityFactory+User.h"
#import "JMCDRepository+State.h"
#import "JMCDRepository.h"
#import "JMState.h"
#import "JMTextFieldCell.h"
#import "JMUser.h"
#import "UIImageView+Avatar.h"
#import "UIViewController+ImageBackButton.h"
#import "PSTAlertController.h"

static NSString *const kFirstName     = @"First Name";
static NSString *const kLastName      = @"Last Name";
static NSString *const kEmail         = @"Email";
static NSString *const kStreetAddress = @"Street Address";
static NSString *const kCity          = @"City";
static NSString *const kCustom        = @"Custom(non-US)";
static NSString *const kZipCode       = @"Zip Code";
static NSString *const kState         = @"State";

@interface JMSenderEditController ()
<UIPickerViewDelegate,
UIPickerViewDataSource>{
    NSArray *_states;
    UIView *statesView;
}

@property (nonatomic, retain) JMUser *user;

@end

@implementation JMSenderEditController

+ (NSString *)storyboardID
{
    return @"idSenderEditController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"New Sender", @"New Sender");
    
    self.headers = [NSArray arrayWithObjects:
                    kFirstName,
                    kLastName,
                    kEmail,
                    kZipCode,
                    kStreetAddress,
                    kCity,
                    kCustom,
                    kState, nil];
    
    [self setCustomBackButton];
    
    [self setAvatarImage:_user.avatar];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_user)
    {
        self.navigationItem.title = NSLocalizedString(@"Edit Sender", @"Edit Sender");
    }
}


- (void)setSender:(id)sender
{
    if (sender && [sender isKindOfClass:JMUser.class])
    {
        _user = sender;
        
        [self setAvatarImage:_user.avatar];
    }
}


- (JMUser *)user
{
    if (_user == nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        _user = [[JMCDEntityFactory newFactory] newUser];
        [repository addEntity:_user];
    }
    
    return _user;
}


- (IBAction)backButtonTap:(id)sender
{
    [self.tableView reloadData];
    
    if ([_user hasChanges])
    {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"Save changes", @"Save changes")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                if ([self save])
                                                                {
                                                                    [self.navigationController popViewControllerAnimated:YES];
                                                                }
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Rollback changes", @"Rollback changes")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                if ([self rollback])
                                                                {
                                                                    [self.navigationController popViewControllerAnimated:YES];
                                                                }
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
        [alertController showWithSender:sender controller:self animated:YES completion:NULL];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)doneTap:(id)sender
{
    [super doneTap:sender];
    
    [self.tableView reloadData];
    
    if ([self save])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (BOOL)save
{
    [self.tableView reloadData];
    
    if ([self validateAll] == NO)
    {
        return NO;
    }
    
    JMCDRepository *repository = [JMCDRepository newRepository];
    
    if (![repository save])
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)rollback
{
    if (_user != nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        [repository rollback];
        
        _user = nil;
    }
    
    return YES;
}


#pragma mark - Validate

- (NSArray *)wrongField
{
    NSMutableArray *wrongFields = [NSMutableArray new];
    
    if (_user == nil)
    {
        return wrongFields;
    }
    
    if (_user.firstName.length == 0)
    {
        [wrongFields addObject:kFirstName];
    }
    
    if (_user.lastName.length == 0)
    {
        [wrongFields addObject:kLastName];
    }
    
    if (![self validateEmail:_user.email])
    {
        [wrongFields addObject:kEmail];
    }
    
    if (_user.street.length == 0)
    {
        [wrongFields addObject:kStreetAddress];
    }
    
    if (_user.city.length == 0)
    {
        [wrongFields addObject:kCity];
    }
    
    if (_user.zipCode.length == 0)
    {
        [wrongFields addObject:kZipCode];
    }
    
    if (_user.state == nil)
    {
        [wrongFields addObject:kState];
    }
    
    return wrongFields;
}


- (BOOL)validateAll
{
    NSArray *wrongFields = [self wrongField];
    
    if ([wrongFields containsObject:kEmail])
    {
        [self errorAlertWithText:NSLocalizedString(@"Enter a valid email address.", @"Enter a valid email address.")];
    }
    else if ([wrongFields containsObject:kState]) {
        [self errorAlertWithText:NSLocalizedString(@"Select state.", @"Select state.")];
    }
    else if (wrongFields.count > 0) {
        [self errorAlertWithText:NSLocalizedString(@"Сorrect the fields marked in red.", @"Сorrect the fields marked in red.")];
    }
    
    [self.tableView reloadData];
    
    return wrongFields.count == 0;
}


- (BOOL)validateRow:(NSInteger)row
{
    NSArray *wrongFields = [self wrongField];
    
    return ![wrongFields containsObject:[self.headers objectAtIndex:row]];
}


- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex   = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}


#pragma mark -

- (void)setAvatarImage:(UIImage *)avatarImage
{
    [super setAvatarImage:avatarImage];
    
    self.user.avatar = avatarImage;
}


- (void)header:(NSString *)header didEndEditing:(NSString *)string
{
    NSString *text = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([header isEqualToString:kFirstName] && (text.length > 0))
    {
        self.user.firstName = text;
    }
    else if ([header isEqualToString:kLastName] && (text.length > 0)) {
        self.user.lastName = text;
    }
    else if ([header isEqualToString:kEmail] && (text.length > 0)) {
        self.user.email = text;
    }
    else if ([header isEqualToString:kStreetAddress] && (text.length > 0)) {
        self.user.street = text;
    }
    else if ([header isEqualToString:kCity] && (text.length > 0)) {
        self.user.city = text;
    }
    else if ([header isEqualToString:kCustom] && (text.length > 0)) {
        self.user.custom = text;
    }
    else if ([header isEqualToString:kZipCode] && (text.length > 0)) {
        self.user.zipCode = text;
    }
    else if ([header isEqualToString:kState] && (text.length > 0)) {
        self.user.state.name = text;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [super textFieldShouldReturn:textField];
    
    JMTextField *field     = (JMTextField *)textField;
    NSIndexPath *indexPath = field.attachments;
    
    NSString *header = (self.headers.count > indexPath.section)?[self.headers objectAtIndex:indexPath.section] : @"";
    
    if ([header isEqualToString:kState])
    {
        [self pickerState];
    }
    
    return YES;
}


#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JMTextFieldCell *cell = (JMTextFieldCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    NSString *header = [self.headers objectAtIndex:indexPath.section];
    
    cell.field.keyboardType = UIKeyboardTypeDefault;
    
    if ([header isEqualToString:kFirstName])
    {
        cell.field.text = _user.firstName;
    }
    else if ([header isEqualToString:kLastName]) {
        cell.field.text = _user.lastName;
    }
    else if ([header isEqualToString:kEmail]) {
        cell.field.keyboardType = UIKeyboardTypeEmailAddress;
        cell.field.text         = _user.email;
    }
    else if ([header isEqualToString:kStreetAddress]) {
        cell.field.text = _user.street;
    }
    else if ([header isEqualToString:kCity]) {
        cell.field.text = _user.city;
    }
    else if ([header isEqualToString:kCustom]) {
        cell.field.text = _user.custom;
    }
    else if ([header isEqualToString:kZipCode]) {
        cell.field.text         = _user.zipCode;
        cell.field.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    
    if ([header isEqualToString:kState])
    {
        cell.field.text                   = _user.state.name;
        cell.field.userInteractionEnabled = NO;
    }
    else {
        cell.field.userInteractionEnabled = YES;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self pickerState];
}


#pragma mark - UIPickerView and UIPickerViewDataSource

- (UIToolbar *)accessoryView
{
    UIToolbar *accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(doneStateSelection)];
    [accessoryView setItems:[NSArray arrayWithObject:doneButton]];
    
    return accessoryView;
}


- (void)doneStateSelection
{
    [self.tableView reloadData];
    [self closeStateSelection];
}


- (void)closeStateSelection
{
    [statesView removeFromSuperview];
    statesView = nil;
}


- (void)pickerState
{
    if (_states == nil)
    {
        NSArray *items = [[JMCDRepository newRepository] allState];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        _states = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    }
    
    if (statesView == nil)
    {
        float screenWidth  = self.view.bounds.size.width;
        float screenHeight = self.view.bounds.size.height;
        
        statesView                 = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight/2, screenWidth, screenHeight/2)];
        statesView.backgroundColor = [UIColor whiteColor];
        [statesView addSubview:self.accessoryView];
        [self.view addSubview:statesView];
        
        UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, screenWidth, 280.0f)];
        pickerView.backgroundColor = [UIColor whiteColor];
        [pickerView setDataSource:self];
        [pickerView setDelegate:self];
        pickerView.showsSelectionIndicator = YES;
        
        [statesView addSubview:pickerView];
        
        id obj = _user.state;
        
        if (obj && [obj isKindOfClass:[JMState class]])
        {
            NSInteger index = [[_states valueForKey:@"name"] indexOfObject:[obj name]];
            
            [pickerView selectRow:index inComponent:0 animated:YES];
        }
        else {
            JMState *state = [_states objectAtIndex:0];
            self.user.state = state;
        }
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_states count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    JMState *state = [_states objectAtIndex:row];
    return state.name;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    JMState *state = [_states objectAtIndex:row];
    self.user.state = state;
}


@end