//
//  NSError+Request.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/07/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Request)

+ (NSError *)errorWithDescription:(NSString *)aDescription;

+ (NSError *)errorWithCode:(NSInteger)code description:(NSString *)aDescription;

@end
