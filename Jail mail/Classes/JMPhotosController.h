//
//  JMPhotosController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 29/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JMPhotosController;

@protocol PhotosControllerDelegate<NSObject>

@optional
- (void)photosDidUpdated:(JMPhotosController *)controller;

@end

@interface JMPhotosController : UIViewController

@property (nonatomic, retain) id <PhotosControllerDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *photos;

- (void)setFrame:(CGRect)rect;

- (void)reloadData;

@end
