//
//  JMCDEntityFactory+Institution.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory+Institution.h"

@implementation JMCDEntityFactory (Institution)

- (JMInstitution *)newInstitution
{
    JMInstitution *newInmate = (JMInstitution *)[self newEntityForClass:[JMInstitution class]];
    
    return newInmate;
}

@end
