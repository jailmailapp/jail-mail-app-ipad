//
//  PageContentViewController.m
//  Tibetan Singing Bowls
//
//  Created by Alexander Shvetsov on 24/Sep/2015.
//  Copyright © 2015 Yanpix. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

    self.imageContentView.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
}


@end