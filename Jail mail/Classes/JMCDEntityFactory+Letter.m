//
//  JMCDEntityFactory+Letter.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 17/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory+Letter.h"

@implementation JMCDEntityFactory (Letter)

- (JMLetter *)newLetter
{
    JMLetter *newLetter = (JMLetter *)[self newEntityForClass:[JMLetter class]];
    
    return newLetter;
}


@end
