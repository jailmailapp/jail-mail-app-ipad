//
//  JMCDRepository+Attachments.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 03/07/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"
#import "JMAttachments.h"

@interface JMCDRepository (Attachments)

- (NSArray *)attachmentsForStatus:(JMAttachmentsState)status;

@end
