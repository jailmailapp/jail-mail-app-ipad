//
//  JMCardEditController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 13/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@class JMUser, JMCard;

@interface JMCardEditController : UIViewController <StoryboardProtocol>

- (void)creationWithUser:(JMUser *)user;
- (void)editCard:(JMCard *)card;

@end
