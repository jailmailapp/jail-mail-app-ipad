//
//  JMCDEntityFactory+Attachments.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 30/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory+Attachments.h"

@implementation JMCDEntityFactory (Attachments)

- (JMAttachments *)newAttachments
{
    JMAttachments *newCard = (JMAttachments *)[self newEntityForClass:[JMAttachments class]];
    
    return newCard;
}

@end
