//
//  JMAttachmentsRequest.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 29/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMAttachmentsRequest.h"

#import "AFNetworking.h"
#import "JMAttachments.h"
#import "JMConst.h"
#import "JMPackage.h"
#import "JMUser.h"
#import "NSError+Request.h"

@implementation JMAttachmentsRequest

+ (void)sendAttachments:(JMAttachments *)attachments
               fileName:(NSString *)fileName
                    URL:(NSString *)URLString
                success:(void (^)(id response))success
                failure:(void (^)(NSError *error))failure
{
    NSLog(@"Start send %@", attachments.path);
    
    NSData *fileData = UIImageJPEGRepresentation(attachments.image, 1);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil constructingBodyWithBlock:^(id < AFMultipartFormData > formData) {
        [formData appendPartWithFileData:fileData
                                    name:@"-1"
                                fileName:fileName
                                mimeType:@"jpeg/pdf"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog (@"Success: %@ %@", responseObject, [operation responseString]);
        
        if (success)
        {
            success (@"Done send photo");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog (@"Error: %@ %@", error, [operation responseString]);
        
        if (failure)
        {
            failure ([NSError errorWithDescription:NSLocalizedString (@"Problem sending photo.", @"Problem sending photo.")]);
        }
    }];
}


+ (void)sendAttachments:(NSArray *)attachments
              forLetter:(JMLetter *)letter
                success:(void (^)(id response))success
                failure:(void (^)(NSError *error))failure
               progress:(void (^)(id progressValue))progress
{
    for (NSUInteger i = 0; i < attachments.count; i++)
    {
        JMAttachments *attachment = [attachments objectAtIndex:i];
        
        if (attachment.state == JMAttachmentsStateNew)
        {
            NSString *fileName = [NSString stringWithFormat:@"%@_%@_%lu.jpeg", letter.userID, letter.letterID, (unsigned long)i];
            
            NSString *URLString = [NSString stringWithFormat:@"%@/inmate/multiupload.html?email=%@&userId=%@&messageId=%@&transactionId=%@",
                                   HOST_URL,
                                   letter.sender.email,
                                   letter.userID,
                                   letter.letterID,
                                   @"-1"];
            
            [JMAttachmentsRequest sendAttachments:attachment
                                         fileName:fileName
                                              URL:URLString
                                          success:^(id response) {
                                              attachment.state = JMAttachmentsStateSubmitted;
                                              
                                              if (progress)
                                              {
                                                  progress(attachment);
                                              }
                                              
                                              [JMAttachmentsRequest sendAttachments:attachments
                                                                          forLetter:letter
                                                                            success:success
                                                                            failure:failure
                                                                           progress:progress];
                                          }
                                          failure:^(NSError *error) {
                                              attachment.state = JMAttachmentsStateError;
                                              
                                              if (progress)
                                              {
                                                  progress (attachment);
                                              }
                                              
                                              if (failure)
                                              {
                                                  failure (error);
                                              }
                                              
                                              [JMAttachmentsRequest sendAttachments:attachments
                                                                          forLetter:letter
                                                                            success:success
                                                                            failure:failure
                                                                           progress:progress];
                                          }];
            
            return;
        }
    }
    
    if (success)
    {
        success (attachments);
    }
}


+ (void)sendPhotosForPackage:(JMPackage *)package
                     success:(void (^)(id response))success
                     failure:(void (^)(NSError *error))failure
                    progress:(void (^)(id progress))progress
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nState == %d OR nState == %d", JMAttachmentsStateNew, JMAttachmentsStateError];
    NSSet *newAttachments = [package.letter.attachments filteredSetUsingPredicate:predicate];
    
    [JMAttachmentsRequest sendAttachments:[newAttachments allObjects]
                                forLetter:package.letter
                                  success:^(id response) {
                                      NSLog (@"Done send");
                                      
                                      if (success)
                                      {
                                          NSArray *attachments = response;
                                          
                                          for (JMAttachments *attachment in attachments)
                                          {
                                              if (attachment.state == JMAttachmentsStateError)
                                              {
                                                  dispatch_async (dispatch_get_main_queue (), ^{
                                                      success ([NSNumber numberWithBool:NO]);
                                                  });
                                                  
                                                  return;
                                              }
                                          }
                                          
                                          dispatch_async (dispatch_get_main_queue (), ^{
                                              success ([NSNumber numberWithBool:YES]);
                                          });
                                      }
                                  }
                                  failure:^(NSError *error) {
                                      if (failure)
                                      {
                                          dispatch_async (dispatch_get_main_queue (), ^{
                                              failure (error);
                                          });
                                      }
                                  }
                                 progress:^(id progressValue) {
                                     if (progress)
                                     {
                                         dispatch_async (dispatch_get_main_queue (), ^{
                                             progress (progressValue);
                                         });
                                     }
                                 }];
}

@end