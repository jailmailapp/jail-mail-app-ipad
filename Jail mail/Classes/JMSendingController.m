//
//  JMSendingController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 28/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMSendingController.h"

#import "JMAttachments.h"
#import "JMAttachmentsRequest.h"
#import "JMCDRepository.h"
#import "JMLetterRequest.h"
#import "JMPackage.h"

#import "JMAuthorizeNet.h"
#import "JMCardsController.h"
#import "JMFinancesCodeRequest.h"
#import "JMPayPal.h"
#import "JMPaymentRequest.h"
#import "JMThankYouController.h"
#import "RXMLElement.h"
#import "PSTAlertController.h"

@interface JMSendingController ()<UIAlertViewDelegate> {
    JMPackage *_package;
    UIAlertView *_alertError;
    BOOL _uploading;
}

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (strong, nonatomic) IBOutlet UIImageView *letterView;
@property (strong, nonatomic) IBOutlet UIImageView *phoneLetterView;
@property (strong, nonatomic) IBOutlet UIImageView *doneLetterView;
@property (strong, nonatomic) IBOutlet UIImageView *cluodLetterView;

@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) IBOutlet UIImageView *phonePhotoView;
@property (strong, nonatomic) IBOutlet UIImageView *donePhotoView;
@property (strong, nonatomic) IBOutlet UIImageView *cluodPhotoView;
@property (strong, nonatomic) IBOutlet UILabel *photoCountLabel;

@end

@implementation JMSendingController

+ (NSString *)storyboardID
{
    return @"idSendingController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Sending", @"Sending");
    
    _uploading = NO;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (_uploading == NO)
    {
        // This start point sending letter
        [self prepareAndUploadLetter];
    }
}


#pragma mark -

- (void)newLetterPackage:(JMPackage *)aLetter
{
    _package = aLetter;
    
    // Update state photos before start
    for (JMAttachments *attachments in _package.letter.attachments)
    {
        [attachments setState:JMAttachmentsStateNew];
    }
    
    _package.letter.letterID = nil;
    _package.letter.userID   = nil;
    _package.letter.sentAt   = nil;
}


- (void)continueLetterPackage:(JMPackage *)aLetter
{
    _package = aLetter;
}


#pragma mark -

- (void)goToThankYouController
{
    JMThankYouController *paymentController = [JMThankYouController controller];
    [self.navigationController pushViewController:paymentController animated:YES];
}


- (void)goToCardController
{
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:JMCardsController.class])
        {
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - Animating

- (void)stopAnimating
{
    self.letterView.alpha = 0;
    self.photoView.alpha  = 0;
    
    [self.letterView.layer removeAllAnimations];
    [self.photoView.layer removeAllAnimations];
}


- (void)startAnimatingLetter
{
    [self.activityView startAnimating];
    
    self.letterView.alpha = 0.5;
    
    [UIView animateWithDuration:1
                          delay:0.2
                        options:UIViewAnimationOptionRepeat
                     animations:^{
                         self.letterView.alpha = 1;
                         
                         self.letterView.frame = CGRectMake (CGRectGetMinX (self.cluodLetterView.frame),
                                                             self.letterView.frame.origin.y,
                                                             self.letterView.frame.size.width,
                                                             self.letterView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         self.letterView.frame = CGRectMake (CGRectGetMinX (self.phoneLetterView.frame)+CGRectGetWidth (self.phoneLetterView.frame)/2,
                                                             self.letterView.frame.origin.y,
                                                             self.letterView.frame.size.width,
                                                             self.letterView.frame.size.height);
                     }];
}


- (void)startAnimatingPhoto
{
    [self.activityView startAnimating];
    
    self.photoView.alpha = 0.5;
    
    [UIView animateWithDuration:1
                          delay:0.2
                        options:UIViewAnimationOptionRepeat
                     animations:^{
                         self.photoView.alpha = 1;
                         
                         self.photoView.frame = CGRectMake (CGRectGetMinX (self.cluodPhotoView.frame),
                                                            self.photoView.frame.origin.y,
                                                            self.photoView.frame.size.width,
                                                            self.photoView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         self.photoView.frame = CGRectMake (CGRectGetMinX (self.phonePhotoView.frame)+CGRectGetWidth (self.phonePhotoView.frame)/2,
                                                            self.photoView.frame.origin.y,
                                                            self.photoView.frame.size.width,
                                                            self.photoView.frame.size.height);
                     }];
}


- (void)doneLetterAnimating
{
    [UIView animateWithDuration:1.0f
                     animations:^{
                         self.doneLetterView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}


- (void)donePhotoAnimating
{
    [UIView animateWithDuration:1.0f
                     animations:^{
                         self.donePhotoView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}


#pragma mark - Payment type

- (void)paymentCardPackage
{
    NSLog(@"\n------------------------------------------------------------ STEP  5:\n");
    
    [JMPaymentRequest creditCardPaymentPackage:_package
                                      deviceID:[NSString stringWithFormat:@"%@", [UIDevice currentDevice].identifierForVendor.UUIDString]
                                       success:^(id response) {
                                           if (([response rangeOfString:@"Declined"].location != NSNotFound) || ([response rangeOfString:@"Error"].location != NSNotFound))
                                           {
                                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString (@"Credit Card Declined", @"Credit Card Declined")
                                                                                               message:NSLocalizedString (@"Please check info and re-submit request.", @"Please check info and re-submit request.")
                                                                                              delegate:nil
                                                                                     cancelButtonTitle:NSLocalizedString (@"OK", @"OK")
                                                                                     otherButtonTitles:nil];
                                               [alert show];
                                               
                                               [self goToCardController];
                                           }
                                           else {
                                               [self processDone];
                                           }
                                       } failure:^(NSError *error) {
                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString (@"Sending Failed", @"Sending Failed")
                                                                                           message:error.localizedDescription
                                                                                          delegate:nil
                                                                                 cancelButtonTitle:NSLocalizedString (@"OK", @"OK")
                                                                                 otherButtonTitles:nil];
                                           [alert show];
                                           
                                           [self goToCardController];
                                       }];
}


#pragma mark PayPal

- (void)paymentPayPalPackage
{
    NSLog(@"\n------------------------------------------------------------ STEP  5:\n");
    
    [[JMPayPal sharedInstance] payForTheLetter:_package success:^(PayPalPayment *completedPayment) {
        NSString *PayPalID = [[completedPayment.confirmation valueForKey:@"response"] valueForKey:@"id"];
        
        NSLog (@"Payment PayPal package: %@", PayPalID);
        
        [self verifyPayPalTransaction:PayPalID];
    } cancel:^(){
        NSLog (@"Cancel");
        
        [self goToCardController];
    } error:^(NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString (@"Sending Failed", @"Sending Failed")
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString (@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        
        [self goToCardController];
    } inPaymentViewControllerPresentedIn:self];
}


- (void)verifyPayPalTransaction:(NSString *)PayPalID
{
    [JMPaymentRequest verifyPayPalTransaction:PayPalID package:_package success:^(id response) {
        RXMLElement *rootXML = [RXMLElement elementFromXMLString:response encoding:NSUTF8StringEncoding];
        
        [rootXML iterateWithRootXPath:@"//root" usingBlock: ^(RXMLElement *player) {
            NSString *result = [player child:@"success"].text;
            
            NSLog (@"%@", result);
            
            if (result.boolValue)
            {
                [self processDone];
            }
            else {
                NSString *error   = [player child:@"error"].text;
                NSString *message = [player child:@"message"].text;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error
                                                                message:message
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString (@"OK", @"OK")
                                                      otherButtonTitles:nil];
                [alert show];
                
                [self goToCardController];
            }
        }];
    } failure:^(NSError *error) {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:NSLocalizedString (@"Sending Failed", @"Sending Failed")
                                                                                 message:[NSString stringWithFormat:NSLocalizedString (@"IMPORTANTLY don't close fast this alert. \n Failed sending transaction info to the server, you can re-submit info or take a screenshot and contact by email. \nError %@ \nYour transaction ID:%@ \n", @"IMPORTANTLY don't close this window. \n Failed sending transaction info to the server, you can re-submit info or take a screenshot and contact by email. \nError: %@ \nYour transaction ID: %@ \n"), error.localizedDescription, PayPalID]
                                                                          preferredStyle:PSTAlertControllerStyleActionSheet];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Re-submit", @"Re-submit")
                                                            style:PSTAlertActionStyleDefault
                                                          handler:^(PSTAlertAction *action) {
                                                              [self verifyPayPalTransaction:PayPalID];
                                                          }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel")
                                                            style:PSTAlertActionStyleDefault
                                                          handler:^(PSTAlertAction *action) {
                                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                                          }]];
        
        [alertController showWithSender:nil controller:self animated:YES completion:NULL];
    }];
}


#pragma mark AuthorizeNet

- (void)paymentAuthorizeNetPackage
{
    NSLog(@"\n------------------------------------------------------------ STEP  5:\n");
    
    [[JMAuthorizeNet  sharedInstance] payForTheLetter:_package success:^(id response) {
        NSString *AuthorizeNetID = response;
        
        NSLog (@"Payment ApplePay package: %@", AuthorizeNetID);
        
        [self verifyAuthorizeNetTransaction:AuthorizeNetID];
    } cancel:^(id response){
        NSLog (@"Cancel");
        
        [self goToCardController];
    } error:^(NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString (@"Sending Failed", @"Sending Failed")
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString (@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        
        [self goToCardController];
    } inPaymentViewControllerPresentedIn:self];
}


- (void)verifyAuthorizeNetTransaction:(NSString *)AuthorizeNetID
{
    [JMPaymentRequest verifyAuthorizeNetTransaction:AuthorizeNetID package:_package success:^(id response) {
        RXMLElement *rootXML = [RXMLElement elementFromXMLString:response encoding:NSUTF8StringEncoding];
        
        [rootXML iterateWithRootXPath:@"//root" usingBlock: ^(RXMLElement *player) {
            NSString *result = [player child:@"success"].text;
            
            NSLog (@"%@", result);
            
            if (result.boolValue)
            {
                [self processDone];
            }
            else {
                NSString *error = [player child:@"error"].text;
                NSString *message = [player child:@"message"].text;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString (@"OK", @"OK")
                                                      otherButtonTitles:nil];
                [alert show];
                
                [self goToCardController];
            }
        }];
    } failure:^(NSError *error) {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:NSLocalizedString (@"Sending Failed", @"Sending Failed")
                                                                                 message:[NSString stringWithFormat:NSLocalizedString (@"IMPORTANTLY don't close fast this alert. \n Failed sending transaction info to the server, you can re-submit info or take a screenshot and contact by email. \nError %@ \nYour transaction ID:%@ \n", @"IMPORTANTLY don't close this window. \n Failed sending transaction info to the server, you can re-submit info or take a screenshot and contact by email. \nError: %@ \nYour transaction ID: %@ \n"), error.localizedDescription, AuthorizeNetID]
                                                                          preferredStyle:PSTAlertControllerStyleActionSheet];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Re-submit", @"Re-submit")
                                                            style:PSTAlertActionStyleDefault
                                                          handler:^(PSTAlertAction *action) {
                                                              [self verifyAuthorizeNetTransaction:AuthorizeNetID];
                                                          }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel")
                                                            style:PSTAlertActionStyleDefault
                                                          handler:^(PSTAlertAction *action) {
                                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                                          }]];
        
        [alertController showWithSender:nil controller:self animated:YES completion:NULL];
    }];
}


#pragma mark -

- (void)saveLetter
{
    // Update letter
    JMCDRepository *repository = [JMCDRepository newRepository];
    
    [repository save];
}


- (void)prepareAndUploadLetter
{
    if ((_package == nil) || (_uploading == YES))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sending Failed", @"Sending Failed")
                                                        message:NSLocalizedString(@"Something went wrong. Try again.", @"Something went wrong. Try again.")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        return;
    }
    
    NSLog(@"\n------------------------------------------------------------ STEP  1:\n");
    
    _uploading = YES;
    [self stopAnimating];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nState == %d OR nState == %d", JMAttachmentsStateNew, JMAttachmentsStateError];
    NSSet *newAttachments  = [_package.letter.attachments filteredSetUsingPredicate:predicate];
    
    // Update count label
    [self.photoCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)newAttachments.count]];
    
    if ((_package.letter.letterID == nil) && (_package.letter.userID == nil))
    {
        [self startUploadLetter];
    }
    else {
        [self doneLetterAnimating];
        
        if (_package.letterPhotos > 0 && _package.letter.type != JMLetterTypePostcard)
        {
            [self startUploadPhoto];
        }
        else {
            [self donePhotoAnimating];
            [self successfulUploadOverride:NO];
        }
    }
}


- (void)startUploadLetter
{
    [self startAnimatingLetter];
    
    NSLog(@"\n------------------------------------------------------------ STEP  2:\n");
    
    [JMLetterRequest sendPackage:_package success:^(id response) {
        [self stopAnimating];
        [self startAnimatingPhoto];
        
        [UIView animateWithDuration:1.0f
                         animations:^{
                             self.doneLetterView.alpha = 1;
                         }
                         completion:^(BOOL finished) {
                         }];
        
        _package.letter.state = JMLetterStateInProcess;
        
        [self saveLetter];
        
        [self  performSelector:@selector(startUploadPhoto) withObject:nil afterDelay:0.3f];
    } failure:^(NSError *error) {
        [self stopAnimating];
        
        _package.letter.state = JMLetterStateError;
        
        [self saveLetter];
        
        [self alertUploadFailed];
    }];
}


- (void)startUploadPhoto
{
    [self startAnimatingPhoto];
    
    NSLog(@"\n------------------------------------------------------------ STEP  3:\n");
    
    [JMAttachmentsRequest sendPhotosForPackage:_package
                                       success:^(id response) {
                                           [self stopAnimating];
                                           
                                           [UIView animateWithDuration:1.0f
                                                            animations:^{
                                                                self.donePhotoView.alpha = 1;
                                                            }
                                                            completion:^(BOOL finished) {
                                                            }];
                                           
                                           [self saveLetter];
                                           
                                           NSNumber *num = response;
                                           
                                           if (num.boolValue)
                                           {
                                               [self successfulUploadOverride:NO];
                                           }
                                           else {
                                               [self alertUploadFailed];
                                           }
                                       }
                                       failure:^(NSError *error) {
                                           [self alertWithError:error.localizedDescription];
                                           
                                           [self saveLetter];
                                       }
                                      progress:^(id progress) {
                                          NSInteger count = [self.photoCountLabel.text integerValue]-1;
                                          [self.photoCountLabel setText:[NSString stringWithFormat:@"%ld", (long)count]];
                                          
                                          [self saveLetter];
                                      }];
}


- (void)processDone
{
    [self stopAnimating];
    
    _package.letter.state  = JMLetterStateSubmitted;
    _package.letter.sentAt = [NSDate date];
    
    [self saveLetter];
    
    if (_package.typePayment == JMTypePaymentCard && _package.priceTotal != 0)
    {
        [self  performSelector:@selector(goToThankYouController) withObject:nil afterDelay:1.0f];
    }
    else {
        //For JMTypePaymentCard this method work automatically
        
        NSLog(@"\n------------------------------------------------------------ STEP  6':\n");
        
        [JMFinancesCodeRequest updateRewardWithDeviceID:[NSString stringWithFormat:@"%@", [UIDevice currentDevice].identifierForVendor.UUIDString]
                                                package:_package success:^(id response)
         {
             [self  performSelector:@selector(goToThankYouController) withObject:nil afterDelay:1.0f];
         } failure:^(NSError *error) {
             PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:NSLocalizedString (@"Failed upgrade discounts", @"Failed upgrade discounts")
                                                                                      message:NSLocalizedString (@"The letter was sent successfully. But the error upgrade discounts.", @"The letter was sent successfully. But the error upgrade discounts.")
                                                                               preferredStyle:PSTAlertControllerStyleActionSheet];
             
             [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Re-upgrade discounts", @"Re-upgrade discounts")
                                                                 style:PSTAlertActionStyleDefault
                                                               handler:^(PSTAlertAction *action) {
                                                                   [self processDone];
                                                               }]];
             
             [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel")
                                                                 style:PSTAlertActionStyleDefault
                                                               handler:^(PSTAlertAction *action) {
                                                                   [self.navigationController popToRootViewControllerAnimated:YES];
                                                               }]];
             
             [alertController showWithSender:nil controller:self animated:YES completion:NULL];
         }];
    }
}


- (void)successfulUploadOverride:(BOOL)override
{
    NSLog(@"\n------------------------------------------------------------ STEP  4:\n");
    
    [JMLetterRequest updateCompletedPackage:_package overrideCompletedStatusToUploaded:override success:^(id response) {
        if (_package.priceTotal == 0)
        {
            [self processDone];
        }
        else if ((_package.typePayment == JMTypePaymentCard))
        {
            [self paymentCardPackage];
        }
        else if (_package.typePayment == JMTypePaymentApplePay) {
            [self paymentAuthorizeNetPackage];
        }
        else if (_package.typePayment == JMTypePaymentPayPal) {
            [self paymentPayPalPackage];
        }
    } failure:^(NSError *error) {
        [self alertWithError:error.localizedDescription];
    }];
}


- (void)alertWithError:(NSString *)error
{
    if (_alertError.visible)
    {
        error = [error stringByAppendingFormat:@"%@\n%@", _alertError.message, error];
        [_alertError dismissWithClickedButtonIndex:0 animated:NO];
    }
    
    _alertError = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sending Failed", @"Sending Failed")
                                             message:error
                                            delegate:self
                                   cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                   otherButtonTitles:nil];
    [_alertError show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_alertError == alertView)
    {
        [self alertUploadFailed];
    }
}


- (void)alertUploadFailed
{
    BOOL canProceedUploadedItems = [self canProceedUploadedItems];
    
    NSString *message = [NSString stringWithFormat:NSLocalizedString (@"Failed to upload some files to the server%@ re-submit request or cancel your order and continue later.",
                                                                      @"Failed to upload some files to the server%@ re-submit request or cancel your order and continue later."), (canProceedUploadedItems)?@", you can proceed to payment with uploaded items," : @""];
    
    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:NSLocalizedString (@"Upload Failed", @"Upload Failed")
                                                                               message:message
                                                                        preferredStyle:PSTAlertControllerStyleActionSheet];
    
    if (canProceedUploadedItems)
    {
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Payment with uploaded items", @"Payment with uploaded items")
                                                             style:PSTAlertActionStyleDefault
                                                           handler:^(PSTAlertAction *action) {
                                                               [self successfulUploadOverride:YES];
                                                           }]];
    }
    
    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Re-submit", @"Re-submit")
                                                         style:PSTAlertActionStyleDefault
                                                       handler:^(PSTAlertAction *action) {
                                                           [self prepareAndUploadLetter];
                                                       }]];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel")
                                                         style:PSTAlertActionStyleDefault
                                                       handler:^(PSTAlertAction *action) {
                                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                                       }]];
    
    [alertController showWithSender:nil controller:self animated:YES completion:NULL];
}


#pragma mark -

- (BOOL)canProceedUploadedItems
{
    if ((_package.letter.letterID != nil) && (_package.letter.userID != nil) && _package.typePayment == JMTypePaymentCard)
    {
        return YES;
    }
    
    return NO;
}


@end