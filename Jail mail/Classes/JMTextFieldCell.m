//
//  JMTextFieldCell.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMTextFieldCell.h"

@implementation JMTextField

@end


@implementation JMTextFieldCell

- (void)awakeFromNib
{
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.field.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height-1);
}

- (UITextField *)field
{
    if (_field == nil) {
        _field = [[JMTextField alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height-1)];
        _field.backgroundColor = [UIColor colorWithRed:0.918 green:0.941 blue:0.961 alpha:1.000];
        _field.autocorrectionType = UITextAutocorrectionTypeNo;
        _field.spellCheckingType = UITextSpellCheckingTypeNo;
        [self.contentView addSubview:_field];
    }
    
    return _field;
}

@end
