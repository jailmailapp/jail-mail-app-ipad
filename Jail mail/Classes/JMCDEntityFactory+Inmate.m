//
//  JMCDEntityFactory+Inmate.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory+Inmate.h"

@implementation JMCDEntityFactory (Inmate)

- (JMInmate *)newInmate
{
    JMInmate *newInmate = (JMInmate *)[self newEntityForClass:[JMInmate class]];
    
    return newInmate;
}

@end
