//
//  JMProfileCell.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 09/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "EditableCollectionCell.h"

#import "JMProfileView.h"

@interface JMProfileCell : EditableCollectionCell

@property (strong, nonatomic) JMProfileView *profileView;

@end
