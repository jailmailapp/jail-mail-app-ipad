//
//  JMState.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEntity.h"

@class JMInstitution, JMUser;

@interface JMState : JMEntity

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSSet *institutions;
@property (nonatomic, retain) NSSet *users;
@end

@interface JMState (CoreDataGeneratedAccessors)

- (void)addInstitutionsObject:(JMInstitution *)value;
- (void)removeInstitutionsObject:(JMInstitution *)value;
- (void)addInstitutions:(NSSet *)values;
- (void)removeInstitutions:(NSSet *)values;

- (void)addUsersObject:(JMUser *)value;
- (void)removeUsersObject:(JMUser *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end