//
//  JMCard.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEntity.h"

@class JMCardType, JMUser;

@interface JMCard : JMEntity

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *cvcode;
@property (nonatomic, retain) NSString *number;
@property (nonatomic, retain) NSDate *createdAt;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) JMUser *user;

- (NSString *)numberName;

@end