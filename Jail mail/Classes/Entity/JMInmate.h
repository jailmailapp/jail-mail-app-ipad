//
//  JMInmate.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMEntity.h"

@class JMInstitution, JMLetter;

@interface JMInmate : JMEntity

@property (nonatomic, retain) NSString *bed;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *avatarPath;
@property (nonatomic, retain) JMInstitution *institution;
@property (nonatomic, retain) NSSet *letters;

@property (nonatomic, retain) UIImage *avatar;

- (NSString *)fullName;

@end

@interface JMInmate (CoreDataGeneratedAccessors)

- (void)addLettersObject:(JMLetter *)value;
- (void)removeLettersObject:(JMLetter *)value;
- (void)addLetters:(NSSet *)values;
- (void)removeLetters:(NSSet *)values;

@end