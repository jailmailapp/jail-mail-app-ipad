//
//  JMEntity.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

@interface JMEntity : NSManagedObject

+ (NSString *)entityName;

@end
