//
//  JMCard.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCard.h"

#import "JMUser.h"

#import "CardIOCreditCardInfo.h"


@implementation JMCard

@dynamic name;
@dynamic cvcode;
@dynamic number;
@dynamic date;
@dynamic createdAt;
@dynamic user;

+ (NSString *)entityName
{
    return @"Card";
}


- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSDate *now = [NSDate date];
    self.createdAt = now;
}


- (NSString *)numberName
{
    NSString *number = [NSString stringWithFormat:@"****%@", [self.number substringWithRange:NSMakeRange(self.number.length-4, 4)]];
    
    return number;
}

@end