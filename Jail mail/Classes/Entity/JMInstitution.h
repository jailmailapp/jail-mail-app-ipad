//
//  JMInstitution.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEntity.h"

@class JMInmate, JMState;

@interface JMInstitution : JMEntity

@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *zipCode;
@property (nonatomic, retain) NSSet *inmates;
@property (nonatomic, retain) JMState *state;

- (NSString *)note;

@end

@interface JMInstitution (CoreDataGeneratedAccessors)

- (void)addInmatesObject:(JMInmate *)value;
- (void)removeInmatesObject:(JMInmate *)value;
- (void)addInmates:(NSSet *)values;
- (void)removeInmates:(NSSet *)values;

@end