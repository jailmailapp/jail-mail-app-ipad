//
//  JMLetter.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEntity.h"

typedef NS_ENUM(NSUInteger, JMLetterType)
{
    JMLetterTypeMail     = 0,
    JMLetterTypePostcard = 1,
    JMLetterTypePhoto    = 2
};

typedef NS_ENUM(NSUInteger, JMLetterState)
{
    JMLetterStateDraft     = 0,
    JMLetterStateSubmitted = 1,
    JMLetterStateInProcess = 2,
    JMLetterStateError     = 3
};

@class JMInmate, JMAttachments, JMUser;

@interface JMLetter : JMEntity

@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSDate *createdAt;
@property (nonatomic, retain) NSDate *sentAt;
@property (nonatomic, retain) NSSet *attachments;
@property (nonatomic, retain) JMUser *sender;
@property (nonatomic, retain) JMInmate *recipient;
@property (nonatomic, retain) NSString *userID;
@property (nonatomic, retain) NSString *letterID;
@property (nonatomic, retain) NSNumber *nState;
@property (nonatomic, retain) NSNumber *nType;

@property (nonatomic) JMLetterType type;
@property (nonatomic) JMLetterState state;

- (NSSet *)submittedAttachments;

@end

@interface JMLetter (CoreDataGeneratedAccessors)

- (void)addAttachmentsObject:(JMAttachments *)value;
- (void)removeAttachmentsObject:(JMAttachments *)value;
- (void)addAttachments:(NSSet *)values;
- (void)removeAttachments:(NSSet *)values;

@end