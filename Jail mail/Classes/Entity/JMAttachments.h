//
//  JMUploadItem.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMEntity.h"

typedef NS_ENUM(NSUInteger, JMAttachmentsState)
{
    JMAttachmentsStateNew = 0,
    JMAttachmentsStateSubmitted = 1,
    JMAttachmentsStateError = 2
};


@class JMLetter;

@interface JMAttachments : JMEntity

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSNumber *nState;
@property (nonatomic, retain) JMLetter *letter;
@property (nonatomic, retain) NSDate * createdAt;

@property (nonatomic, retain) UIImage *image;

- (UIImage *)thumbnail;
- (void)createThumbnail;

- (JMAttachmentsState)state;
- (void)setState:(JMAttachmentsState)state;

- (void)clearData;

@end