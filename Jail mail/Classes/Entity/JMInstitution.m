//
//  JMInstitution.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMState.h"

@implementation JMInstitution

@dynamic address;
@dynamic city;
@dynamic name;
@dynamic zipCode;
@dynamic inmates;
@dynamic state;

+ (NSString *)entityName
{
    return @"Institution";
}

- (NSString *)note
{
    return [NSString stringWithFormat:@"%@\n%@, %@ %@", self.address, self.city, self.state.name, self.zipCode];
}

@end