//
//  JMUser.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCard.h"
#import "JMLetter.h"
#import "JMState.h"
#import "JMUser.h"

#import "UIImage+Save.h"

@implementation JMUser

@dynamic street;
@dynamic city;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic zipCode;
@dynamic avatarPath;
@dynamic cards;
@dynamic letters;
@dynamic state;
@dynamic custom;

+ (NSString *)entityName
{
    return @"User";
}


- (NSString *)fullName
{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}


- (UIImage *)avatar
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.avatarPath];
    
    UIImage *avatar = [UIImage imageWithContentsOfFile:path];
    
    return avatar;
}


- (void)setAvatar:(UIImage *)image
{
    if (image != nil)
    {
        self.avatarPath = [image saveWithFileName:[NSString stringWithFormat:@"avatar-%@.png", [NSDate date]]];
    }
}

@end