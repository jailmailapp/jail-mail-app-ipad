//
//  JMState.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInstitution.h"
#import "JMState.h"
#import "JMUser.h"

@implementation JMState

@dynamic name;
@dynamic institutions;
@dynamic users;

+ (NSString *)entityName
{
    return @"State";
}


@end