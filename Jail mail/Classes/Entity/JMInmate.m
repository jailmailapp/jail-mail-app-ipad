//
//  JMInmate.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMLetter.h"

#import "UIImage+Save.h"

@implementation JMInmate

@dynamic bed;
@dynamic firstName;
@dynamic identifier;
@dynamic lastName;
@dynamic avatarPath;
@dynamic institution;
@dynamic letters;

+ (NSString *)entityName
{
    return @"Inmate";
}

- (NSString *)fullName
{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}


- (UIImage *)avatar
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.avatarPath];
    
    UIImage *avatar = [UIImage imageWithContentsOfFile:path];
    
    return avatar;
}


- (void)setAvatar:(UIImage *)image
{
    if (image != nil)
    {
        self.avatarPath = [image saveWithFileName:[NSString stringWithFormat:@"avatar-%@.png", [NSDate date]]];
    }
}

@end
