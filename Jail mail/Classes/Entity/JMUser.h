//
//  JMUser.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMEntity.h"

@class JMCard, JMLetter, JMState;

@interface JMUser : JMEntity

@property (nonatomic, retain) NSString *street;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *zipCode;
@property (nonatomic, retain) NSString *custom;
@property (nonatomic, retain) NSString *avatarPath;
@property (nonatomic, retain) NSSet *cards;
@property (nonatomic, retain) NSSet *letters;
@property (nonatomic, retain) JMState *state;

@property (nonatomic, retain) UIImage *avatar;

- (NSString *)fullName;

@end

@interface JMUser (CoreDataGeneratedAccessors)

- (void)addCardsObject:(JMCard *)value;
- (void)removeCardsObject:(JMCard *)value;
- (void)addCards:(NSSet *)values;
- (void)removeCards:(NSSet *)values;

- (void)addLettersObject:(JMLetter *)value;
- (void)removeLettersObject:(JMLetter *)value;
- (void)addLetters:(NSSet *)values;
- (void)removeLetters:(NSSet *)values;

@end