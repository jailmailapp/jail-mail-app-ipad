//
//  JMLetter.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMAttachments.h"
#import "JMInmate.h"
#import "JMLetter.h"
#import "JMUser.h"

#import "PDFHelper.h"

@interface JMLetter ()

@end

@implementation JMLetter

@dynamic message;
@dynamic createdAt;
@dynamic sentAt;
@dynamic attachments;
@dynamic sender;
@dynamic recipient;
@dynamic nType;
@dynamic userID;
@dynamic letterID;
@dynamic nState;

+ (NSString *)entityName
{
    return @"Letter";
}


- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSDate *now = [NSDate date];
    self.createdAt = now;
}

- (JMLetterType)type
{
    return [self.nType integerValue];
}

- (void)setType:(JMLetterType)type
{
    self.nType = [NSNumber numberWithInteger:type];
}

- (JMLetterState)state
{
    return [self.nState integerValue];
}

- (void)setState:(JMLetterState)state
{
    self.nState = [NSNumber numberWithInteger:state];
}

- (NSSet *)submittedAttachments
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nState == %d", JMAttachmentsStateSubmitted];
    NSSet *newAttachments = [self.attachments filteredSetUsingPredicate:predicate];
    
    return newAttachments;
}

@end