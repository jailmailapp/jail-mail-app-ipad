//
//  JMUploadItem.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMAttachments.h"

#import "JMLetter.h"
#import "UIImage+Save.h"

#define THUMBNAIL_WIDTH 300
#define THUMBNAIL_HEIGHT 200

@interface JMAttachments () {
    UIImage *_thumbnail;
}

@end

@implementation JMAttachments

@dynamic path;
@dynamic nState;
@dynamic letter;
@dynamic createdAt;
@synthesize image = _image;

+ (NSString *)entityName
{
    return @"Attachments";
}


- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSDate *now = [NSDate date];
    self.createdAt = now;
}


- (UIImage *)image
{
    UIImage *image = nil;
    
    if (self.path != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.path];
        NSData* imageFileData = [[NSData alloc] initWithContentsOfFile:path];
        image = [[UIImage alloc] initWithData:imageFileData];
    }
    
    return image;
}


- (UIImage *)thumbnail
{
    return _thumbnail;
}

- (void)createThumbnail
{
    UIImage *originalImage = [self image];
    
    CGSize destinationSize = CGSizeMake(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
    
    if (originalImage.size.width < originalImage.size.height) {
        destinationSize = CGSizeMake(destinationSize.height, destinationSize.width);
    }
    
    UIGraphicsBeginImageContext(destinationSize);
    [originalImage drawInRect:CGRectMake(0, 0, destinationSize.width, destinationSize.height)];
    _thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}


- (void)setImage:(UIImage *)image
{
    if (image != nil)
    {
        self.path = [image saveWithFileName:[NSString stringWithFormat:@"/image-%@(%d).png", [NSDate date], rand()%100000+1]];
    }
}


- (void)prepareForDeletion
{
    [super prepareForDeletion];
    
    [self deleteSavedImage];
}


- (BOOL)deleteSavedImage
{
    _image = nil;
    
    return [self.image deleteWithFileName:self.path];
}


- (JMAttachmentsState)state;
{
    return [self.nState integerValue];
}


- (void)setState:(JMAttachmentsState)state;
{
    self.nState = [NSNumber numberWithInteger:state];
}


- (void)dealloc
{
    _image = nil;
}

@end