//
//  JMComposeController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@class JMLetter;

@interface JMComposeController : UIViewController <StoryboardProtocol>

- (void)editLetter:(JMLetter *)letter;
- (void)setPhotoMode:(BOOL)isPhotoMode;

@end
