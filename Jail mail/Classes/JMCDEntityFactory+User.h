//
//  EntityFactory+User.h
//  Jail mail
//
//  Created by Alexander on 29.03.15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMUser.h"

@interface JMCDEntityFactory (User)

- (JMUser *)newUser;

- (JMUser *)newUserWithFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email;

- (JMUser *)newUserFromFacebookInfo:(NSDictionary *)info;

- (JMUser *)newUserFromGoogleInfo:(NSDictionary *)info;

@end