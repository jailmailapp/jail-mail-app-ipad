//
//  JMRecipientsController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMGalleryController.h"

#import "JMRecipientsDelegate.h"

@interface JMRecipientsController : JMGalleryController

@property (nonatomic, weak) id <JMRecipientsDelegate> delegate;

@end
