//
//  Repository.h
//  Jail mail
//
//  Created by Alexander on 28.03.15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JMRepositoryDelegate<NSObject>

@optional
- (void)repository:(id)repository failure:(NSError *)error;

@end

@interface JMAbstractRepository : NSObject

@property (weak) id<JMRepositoryDelegate> delegate;

+ (id)newRepository;

- (void)addEntity:(id)entity;

- (void)deleteEntity:(id)entity;

- (BOOL)save;

- (void)rollback;

@end