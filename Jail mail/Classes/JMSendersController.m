//
//  JMSendersController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMSendersController.h"

#import "JMCDRepository+User.h"
#import "JMNavigationController.h"
#import "JMProfileCell.h"
#import "JMProfileView.h"
#import "JMUser.h"

#import "JMSenderCreateController.h"
#import "JMSenderEditController.h"
#import "PSTAlertController.h"

@interface JMSendersController ()<EditableCellDelegate> {
}

@end

@implementation JMSendersController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title       = NSLocalizedString(@"Senders", @"Senders");
    self.prompt.text = NSLocalizedString(@"Choose saved sender", @"Choose saved sender");
    [self.createButton setTitle:NSLocalizedString(@"Create new sender", @"Create new sender") forState:UIControlStateNormal];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadItems];
    
    if ([self.navigationController isBeingPresented])
    {
        UIBarButtonItem *flexibleButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_close"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(dismiss:)];
        self.navigationItem.leftBarButtonItem = flexibleButton;
    }
}


- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)reloadItems
{
    NSArray *items = [[JMCDRepository newRepository] allUsers];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    self.items = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    [self.galleryCollection reloadData];
    
    [super reloadItems];
}


- (IBAction)createTap:(id)sender
{
    JMSenderCreateController *createSenderController = [JMSenderCreateController controller];
    createSenderController.parent = self;
    JMNavigationController *nav = [[JMNavigationController alloc] initWithRootViewController:createSenderController];
    
    
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:nav];
    [popover presentPopoverFromRect:[sender frame] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (void)deleteCellAttachments:(id)attachments
{
    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        JMUser *inmate = (JMUser *)attachments;
        
        if (inmate && [inmate isKindOfClass:JMUser.class])
        {
            NSLog (@"Delete");
            
            JMCDRepository *repository = [JMCDRepository newRepository];
            [repository deleteEntity:attachments];
            [repository save];
            
            [self reloadItems];
        }
    }]];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
    [alertController showWithSender:nil controller:self animated:YES completion:NULL];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMProfileCell *photoCell = (JMProfileCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JMUser *user = [self.items objectAtIndex:indexPath.row];
    [photoCell.profileView setAvatar:user.avatar];
    [photoCell.profileView setName:user.fullName];
    [photoCell setAttachments:user];
    [photoCell setDelegate:self];
    
    return photoCell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMUser *user = [self.items objectAtIndex:indexPath.row];
    
    if (self.editing)
    {
        JMSenderEditController *senderEditController = [JMSenderEditController controller];
        [senderEditController setSender:user];
        [self.navigationController pushViewController:senderEditController animated:YES];
    }
    else {
        if (_delegate && [_delegate respondsToSelector:@selector(selectedSender:)])
        {
            [_delegate selectedSender:user];
            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:NO completion:nil];
        }
    }
}


@end