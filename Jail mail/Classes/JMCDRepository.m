//
//  CDRepository.m
//  Jail mail
//
//  Created by Alexander on 29.03.15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"

#import "JMCoreDataManager.h"

@interface JMCDRepository ()

@property (nonatomic, retain) JMCoreDataManager *manager;

@end

@implementation JMCDRepository

+ (id)newRepository
{
    JMCDRepository *repository = [[JMCDRepository alloc] init];
    
    return repository;
}


- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.manager = [JMCoreDataManager sharedManager];
    }
    
    return self;
}


#pragma mark -

- (void)addEntity:(id)entity
{
    [self.manager.context insertObject:entity];
}


- (void)deleteEntity:(id)entity
{
    [self.manager.context deleteObject:entity];
}


- (BOOL)save
{
    NSError *error = [self.manager saveContext];
    
    if (error)
    {
        NSLog(@"Core Data error:%@", error);
        
        if ([self.delegate respondsToSelector:@selector(repository:failure:)])
        {
            [self.delegate repository:self failure:error];
        }
        
        return NO;
    }
    
    return YES;
}


- (void)rollback
{
    [self.manager.context rollback];
}


@end