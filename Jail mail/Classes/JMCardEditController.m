//
//  JMCardEditController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 13/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCardEditController.h"

#import "CardIOCreditCardInfo.h"
#import "JMCDEntityFactory+Card.h"
#import "JMCDRepository+Card.h"
#import "JMUser.h"
#import "SRMonthPicker.h"
#import "UIViewController+ImageBackButton.h"
#import "PSTAlertController.h"

static NSString *const kName   = @"Name";
static NSString *const kNumber = @"Number";
static NSString *const kDate   = @"Date";
static NSString *const kCVCode = @"CVCode";

@interface JMCardEditController ()<SRMonthPickerDelegate>
{
    UIView *pickerContener;
}

@property (strong, nonatomic) IBOutlet UITextField *cvCodeField;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *numberField;
@property (strong, nonatomic) IBOutlet UIButton *monthButton;
@property (strong, nonatomic) IBOutlet UIButton *yearButton;
@property (strong, nonatomic) IBOutlet UIButton *methodButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) JMUser *user;
@property (nonatomic, retain) JMCard *card;

@end

@implementation JMCardEditController

+ (NSString *)storyboardID
{
    return @"idCardEditController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"New Card", @"New Card");
    
    self.monthButton.layer.borderColor  = [UIColor darkGrayColor].CGColor;
    self.monthButton.layer.borderWidth  = 1;
    self.yearButton.layer.borderColor   = [UIColor darkGrayColor].CGColor;
    self.yearButton.layer.borderWidth   = 1;
    self.methodButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.methodButton.layer.borderWidth = 1;
    
    [self setCustomBackButton];
    
    [self observeKeyboard];
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_done"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(doneTap:)];
    self.navigationItem.rightBarButtonItem = editButton;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateUI];
}


- (JMCard *)card
{
    if (_card == nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        _card = [[JMCDEntityFactory newFactory] newCard];
        [repository addEntity:_card];
    }
    
    return _card;
}


- (void)editCard:(JMCard *)card
{
    _card = card;
}


- (void)updateUI
{
    self.nameField.text   = _card.name;
    self.numberField.text = _card.number;
    self.cvCodeField.text = _card.cvcode;
    
    [self donePickerSelection];
}


- (void)creationWithUser:(JMUser *)user
{
    if ((user != nil) && [user isKindOfClass:JMUser.class])
    {
        _user = user;
    }
    else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (BOOL)save
{
    if ([self validateAll] == NO)
    {
        return NO;
    }
    
    if (_card.user == nil)
    {
        _card.user = _user;
    }
    
    JMCDRepository *repository = [JMCDRepository newRepository];
    
    if (![repository save])
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)rollback
{
    if (_card != nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        [repository rollback];
        
        _card = nil;
    }
    
    return YES;
}


- (void)errorAlertWithText:(NSString *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                    message:error
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    [alert show];
}


#pragma mark - IBAction

- (IBAction)monthTap:(id)sender
{
    [self showMonthPicker];
}


- (IBAction)yearTap:(id)sender
{
    [self showMonthPicker];
}


- (IBAction)methodTap:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", @"Information")
                                                    message:NSLocalizedString(@"This feature is in development.", @"This feature is in development.")
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    [alert show];
}


- (IBAction)backButtonTap:(id)sender
{
    if ([_card hasChanges])
    {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"Save changes", @"Save changes")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                if ([self save])
                                                                {
                                                                    [self.navigationController popViewControllerAnimated:YES];
                                                                }
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Rollback changes", @"Rollback changes")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                if ([self rollback])
                                                                {
                                                                    [self.navigationController popViewControllerAnimated:YES];
                                                                }
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
        [alertController showWithSender:sender controller:self animated:YES completion:NULL];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)doneTap:(id)sender
{
    if ([self save])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Validate

- (NSArray *)wrongField
{
    NSMutableArray *wrongFields = [NSMutableArray new];
    
    if (_card == nil)
    {
        return wrongFields;
    }
    
    if ([self validateCVCode:_card.cvcode] == NO)
    {
        [wrongFields addObject:kCVCode];
    }
    
    if ([self validateCardNumber:_card.number] == NO)
    {
        [wrongFields addObject:kNumber];
    }
    
    if (_card.date == nil)
    {
        [wrongFields addObject:kDate];
    }
    
    return wrongFields;
}


- (BOOL)validateAll
{
    NSArray *wrongFields = [self wrongField];
    
    if ([wrongFields containsObject:kNumber])
    {
        [self errorAlertWithText:NSLocalizedString(@"Enter a valid card number.", @"Enter a valid card number.")];
    }
    else if ([wrongFields containsObject:kCVCode]) {
        [self errorAlertWithText:NSLocalizedString(@"Enter CV Code.", @"Enter CV Code.")];
    }
    else if ([wrongFields containsObject:kDate]) {
        [self errorAlertWithText:NSLocalizedString(@"Enter date.", @"Enter date.")];
    }
    
    return wrongFields.count == 0;
}


- (JMCardType *)updateTypeCard:(JMCard *)card
{
    CardIOCreditCardInfo *cardInfo = [[CardIOCreditCardInfo alloc] init];
    cardInfo.cardNumber = card.number;
    
    return nil;
}


- (BOOL)validateCVCode:(NSString *)cvcode
{
    NSString *number = [cvcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *toTest = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    if (([toTest length] < 1) || ([toTest length] > 4))
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)validateCardNumber:(NSString *)cardNumber
{
    NSString *number = [cardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *toTest = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSInteger n, i, alternate, sum;
    
    n = [toTest length];
    
    if ((n < 13) || (n > 19))
    {
        return 0;
    }
    
    for (alternate = 0, sum = 0, i = n - 1; i > -1; --i)
    {
        if (!isdigit([toTest characterAtIndex:i]))
        {
            return 0;
        }
        
        n = [toTest characterAtIndex:i] - '0';
        
        if (alternate)
        {
            n *= 2;
            
            if (n > 9)
            {
                n = (n % 10) + 1;
            }
        }
        
        alternate = !alternate;
        
        sum += n;
    }
    
    return (sum % 10 == 0);
}


#pragma mark -

- (void)donePickerSelection
{
    if (_card.date)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"MM"];
        [self.monthButton setTitle:[formatter stringFromDate:_card.date] forState:UIControlStateNormal];
        
        [formatter setDateFormat:@"yyyy"];
        [self.yearButton setTitle:[formatter stringFromDate:_card.date] forState:UIControlStateNormal];
    }
    
    [self closePickerSelection];
}


- (void)closePickerSelection
{
    [pickerContener removeFromSuperview];
    pickerContener = nil;
}


- (void)showMonthPicker
{
    [self.cvCodeField resignFirstResponder];
    [self.nameField resignFirstResponder];
    [self.numberField resignFirstResponder];
    
    if (pickerContener != nil)
    {
        [pickerContener removeFromSuperview];
    }
    
    NSInteger accessoryToolbarHeight = 44;
    CGFloat screenWidth              = self.view.bounds.size.width;
    CGFloat screenHeight             = self.view.bounds.size.height;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(donePickerSelection)];
    UIToolbar *accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, accessoryToolbarHeight)];
    [accessoryView setItems:[NSArray arrayWithObject:doneButton]];
    
    pickerContener                 = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight/2, screenWidth, screenHeight/2)];
    pickerContener.backgroundColor = [UIColor whiteColor];
    [pickerContener addSubview:accessoryView];
    [self.view addSubview:pickerContener];
    
    SRMonthPicker *picker = [[SRMonthPicker alloc] init];
    picker.backgroundColor     = [UIColor whiteColor];
    picker.frame               = CGRectMake(0, accessoryToolbarHeight, screenWidth, 216.0f);
    picker.monthPickerDelegate = self;
    [pickerContener addSubview:picker];
}


- (void)monthPickerDidChangeDate:(SRMonthPicker *)monthPicker
{
    self.card.date = monthPicker.date;
}


#pragma mark -

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)aString
{
    NSString *string = [textField.text stringByReplacingCharactersInRange:range withString:aString];
    
    if (textField == self.nameField)
    {
        self.card.name = string;
    }
    else if (textField == self.numberField) {
        if (string.length > 16)
        {
            return NO;
        }
        else {
            self.card.number = string;
        }
    }
    else if (textField == self.cvCodeField) {
        if (string.length > 4)
        {
            return NO;
        }
        else {
            self.card.cvcode = string;
        }
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == self.nameField)
    {
        self.card.name = textField.text;
    }
    else if (textField == self.numberField) {
        self.card.number = textField.text;
        
        [self updateTypeCard:self.card];
    }
    else if (textField == self.cvCodeField) {
        self.card.cvcode = textField.text;
    }
    
    return YES;
}


- (void)observeKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        id _obj = [note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect _keyboardFrame = CGRectNull;
        
        if ([_obj respondsToSelector:@selector(getValue:)])
        {
            [_obj getValue:&_keyboardFrame];
        }
        
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_scrollView setContentInset:UIEdgeInsetsMake (0.f, 0.f, _keyboardFrame.size.height+10, 0.f)];
        } completion:nil];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_scrollView setContentInset:UIEdgeInsetsZero];
        } completion:nil];
    }];
}

@end