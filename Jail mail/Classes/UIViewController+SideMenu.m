//
//  UIViewController+SideMenu.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 03/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UIViewController+SideMenu.h"

#import "AppDelegate.h"
#import "JMInboxesController.h"
#import "JMMenuController.h"
#import "JMNavigationController.h"

@implementation UIViewController (SideMenu)

- (void)addMenuBarButton
{
    UIBarButtonItem *flexibleButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"]
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(showMenu:)];
    self.navigationItem.leftBarButtonItem = flexibleButton;
}


- (void)showMenu:(id)sender
{
    id firstObject = (JMRootViewController *)[JMAppDelegate rootController].viewControllers.firstObject;
    
    if ([firstObject isKindOfClass:JMNavigationController.class])
    {
        JMNavigationController *navigation = firstObject;
        
        UIViewController *controller = navigation.visibleViewController;
        
        if ([controller isKindOfClass:JMInboxesController.class])
        {
            JMMenuController *menuController = [JMMenuController controller];
            [controller.navigationController pushViewController:menuController animated:NO];
        }
        
        if ([controller isKindOfClass:JMMenuController.class])
        {
            [controller.navigationController popToRootViewControllerAnimated:NO];
        }
    }
}


@end