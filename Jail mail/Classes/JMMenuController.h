//
//  JMMenuController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

typedef enum {
    InboxesMenuItem,
    InfoMenuItem,
    SettingsMenuItem,
    FeedbackMenuItem,
    LoyaltyMenuItem,
    CountMenuItem
} MainMenuItem;

@interface JMMenuController : UIViewController  <StoryboardProtocol>

- (void)selectItemWithIdentifier:(MainMenuItem)menuItem;

@end
