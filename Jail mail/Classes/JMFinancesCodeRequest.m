//
//  JMPromotionalCodeRequest.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 29/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMFinancesCodeRequest.h"


#import "JMConst.h"
#import "JMLetter.h"
#import "JMPackage.h"
#import "MRProgress.h"
#import "RXMLElement.h"
#import <MRProgress/MRActivityIndicatorView+AFNetworking.h>
#import <MRProgress/MRProgressOverlayView+AFNetworking.h>

@implementation JMFinancesCodeRequest

+ (void)verificationCode:(NSString *)code
                 package:(JMPackage *)package
                 success:(void (^)(id response))success
                 failure:(void (^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    manager.requestSerializer = requestSerializer;
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[@"letterPages"] = [NSNumber numberWithInteger:package.letterPages];
    dictionary[@"photosCount"] = [NSNumber numberWithInteger:package.letterPhotos];
    dictionary[@"moneyOrder"]  = [NSNumber numberWithInteger:[package priceTotal]];
    dictionary[@"couponId"]    = code;
    
    NSLog(@"%@", dictionary);
    
    [manager POST:[NSString stringWithFormat:@"%@/promo/rest/json/promo/requestcarrier", HOST_URL]
       parameters:dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog (@"Success: %@", responseObject);
              
              if (success)
              {
	                 id errorString = [responseObject valueForKey:@"couponError"];
                  
	                 if (errorString == [NSNull null] || [errorString length] == 0)
                     {
                         CGFloat totalWithoutDicount = [[responseObject valueForKey:@"totalWithDiscount"] floatValue];
                         
                         CGFloat discount = [package priceTotal] - totalWithoutDicount;
                         
                         dispatch_async (dispatch_get_main_queue (), ^{
                             success ([NSNumber numberWithFloat:discount]);
                         });
                     }
                     else {
                         NSMutableDictionary *details = [NSMutableDictionary dictionary];
                         [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                         
                         NSError *error = [NSError errorWithDomain:@"JMRequestError" code:2001 userInfo:details];
                         
                         dispatch_async (dispatch_get_main_queue (), ^{
                             failure (error);
                         });
                     }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              dispatch_async (dispatch_get_main_queue (), ^{
                  failure (error);
              });
          }];
}

+ (void)updateRewardWithDeviceID:(NSString *)deviceID
                         package:(JMPackage *)package
                         success:(void (^)(id response))success
                         failure:(void (^)(NSError *error))failure
{
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<root>"];
    [xmlString appendFormat:@"<deviceId>%@</deviceId>", deviceID];
    [xmlString appendFormat:@"<letterPages>%ld</letterPages>", (long)package.letterPages];
    [xmlString appendFormat:@"<photCount>%ld</photCount>", (long)package.letterSubmittedPhotos];
    [xmlString appendFormat:@"</root>"];
    
    NSLog(@"Rewards xml: %@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/updateRewardDetails.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    AFURLConnectionOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                               NSLog (@"Request (%@) Success: %@", operation.request.URL.relativeString, [operation responseString]);
                                                                               
                                                                               if (success)
                                                                               {
                                                                                   //success / failure
                                                                                   
                                                                                   if ([[operation responseString] rangeOfString:@"success"].location != NSNotFound)
                                                                                   {
                                                                                       
                                                                                       success([NSNumber numberWithBool:YES]);
                                                                                   }
                                                                                   else {
                                                                                       success([NSNumber numberWithBool:NO]);
                                                                                   }
                                                                               }
                                                                           }
                                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                               NSLog (@"Request (%@) Error: %@", operation.request.URL.relativeString, error);
                                                                               
                                                                               if (failure)
                                                                               {
                                                                                   failure (error);
                                                                               }
                                                                           }];
    
    [manager.operationQueue addOperation:operation];
    
    MRProgressOverlayView *overlayView = [MRProgressOverlayView showOverlayAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    overlayView.mode = MRProgressOverlayViewModeIndeterminate;
    [overlayView setModeAndProgressWithStateOfOperation:operation];
    [overlayView setStopBlockForOperation:operation];
}


+ (void)rewardsWithDeviceID:(NSString *)deviceID
                    success:(void (^)(id response))success
                    failure:(void (^)(NSError *error))failure
{
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<root>"];
    [xmlString appendFormat:@"<deviceId>%@</deviceId>", deviceID];
    [xmlString appendFormat:@"</root>"];
    
    NSLog(@"Rewards xml: %@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/rewards.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    AFURLConnectionOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                               NSLog (@"Request (%@) Success: %@", operation.request.URL.relativeString, [operation responseString]);
                                                                               
                                                                               if (success)
                                                                               {
                                                                                   /*
                                                                                    <root>
                                                                                    <lettersTo>7</lettersTo>
                                                                                    <lettersCount>10</lettersCount>
                                                                                    <photosTo>6</photosTo>
                                                                                    <photosCount>10</photosCount>
                                                                                    </root>
                                                                                    */
                                                                                   
                                                                                   if ([operation responseString].length > 0)
                                                                                   {
                                                                                       RXMLElement *rootXML = [RXMLElement elementFromXMLString:[operation responseString] encoding:NSUTF8StringEncoding];
                                                                                       
                                                                                       [rootXML iterateWithRootXPath:@"//root" usingBlock: ^(RXMLElement *player) {
                                                                                           NSDictionary *rewards = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                                    [player child:@"lettersTo"].text, @"lettersTo",
                                                                                                                    [player child:@"lettersCount"].text, @"lettersCount",
                                                                                                                    [player child:@"photosTo"].text, @"photosTo",
                                                                                                                    [player child:@"photosCount"].text, @"photosCount", nil];
                                                                                           
                                                                                           success (rewards);
                                                                                       }];
                                                                                   }
                                                                                   else {
                                                                                       if (failure)
                                                                                       {
                                                                                           failure (nil);
                                                                                       }
                                                                                   }
                                                                               }
                                                                           }
                                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                               NSLog (@"Request (%@) Error: %@", operation.request.URL.relativeString, error);
                                                                               
                                                                               if (failure)
                                                                               {
                                                                                   failure (error);
                                                                               }
                                                                           }];
    
    [manager.operationQueue addOperation:operation];
    
    MRProgressOverlayView *overlayView = [MRProgressOverlayView showOverlayAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    overlayView.mode = MRProgressOverlayViewModeIndeterminate;
    [overlayView setModeAndProgressWithStateOfOperation:operation];
    [overlayView setStopBlockForOperation:operation];
}

+ (AFURLConnectionOperation *)totalPriceWithDeviceID:(NSString *)deviceID
                                             package:(JMPackage *)package
                                             success:(void (^)(id response))success
                                             failure:(void (^)(NSError *error))failure
{
    NSMutableString *xmlString = [NSMutableString string];
    [xmlString appendFormat:@"<root>"];
    [xmlString appendFormat:@"<deviceId>%@</deviceId>", deviceID];
    
    if (package.letter.type == JMLetterTypePostcard)
    {
        [xmlString appendFormat:@"<letterPages>0</letterPages>"];
        [xmlString appendFormat:@"<photCount>0</photCount>"];
        [xmlString appendFormat:@"<type>postcard</type>"];
    }
    else {
        [xmlString appendFormat:@"<letterPages>%ld</letterPages>", (long)package.letterPages];
        [xmlString appendFormat:@"<photCount>%lu</photCount>", (long)package.letterPhotos];
        
        if (package.letter.type == JMLetterTypePhoto) {
            [xmlString appendFormat:@"<type>photos</type>"];
        }
        
        if (package.letter.type == JMLetterTypeMail) {
            [xmlString appendFormat:@"<type>mail</type>"];
        }
    }
    
    [xmlString appendFormat:@"<couponId>%@</couponId>", package.discountID];
    [xmlString appendFormat:@"</root>"];
    
    NSLog(@"Rewards xml: %@", xmlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/totalPriceCalculate.html", HOST_URL]]];
    [request setHTTPBody:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    AFURLConnectionOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                               NSLog (@"Request (%@) Success: %@", operation.request.URL.relativeString, [operation responseString]);
                                                                               if (success)
                                                                               {
                                                                                   /*
                                                                                    <root>
                                                                                    <priceTotal>0.99</priceTotal>
                                                                                    <priceRewards>0.0</priceRewards>
                                                                                    <pricePhotos>0.99</pricePhotos>
                                                                                    <priceMessages>0.0</priceMessages>
                                                                                    <priceDiscount>0.0</priceDiscount>
                                                                                    </root>
                                                                                    */
                                                                                   
                                                                                   if ([operation responseString].length > 0)
                                                                                   {
                                                                                       if ([[operation responseString] isEqualToString:@"failure"]) {
                                                                                           [JMFinancesCodeRequest rewardsWithDeviceID:deviceID
                                                                                                                              success:^(NSDictionary *response) {
                                                                                                                                  [self totalPriceWithDeviceID:deviceID
                                                                                                                                                       package:package
                                                                                                                                                       success:success
                                                                                                                                                       failure:failure];
                                                                                                                              } failure:^(NSError *error) {
                                                                                                                                  if (failure)
                                                                                                                                  {
                                                                                                                                      failure (error);
                                                                                                                                  }
                                                                                                                              }];
                                                                                           return;
                                                                                       }
                                                                                       
                                                                                       RXMLElement *rootXML = [RXMLElement elementFromXMLString:[operation responseString] encoding:NSUTF8StringEncoding];
                                                                                       
                                                                                       [rootXML iterateWithRootXPath:@"//root" usingBlock: ^(RXMLElement *player) {
                                                                                           NSDictionary *rewards = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                                    [player child:@"priceTotal"].text, @"priceTotal",
                                                                                                                    [player child:@"priceRewards"].text, @"priceRewards",
                                                                                                                    [player child:@"pricePhotos"].text, @"pricePhotos",
                                                                                                                    [player child:@"priceMessages"].text, @"priceMessages",
                                                                                                                    [player child:@"priceDiscount"].text, @"priceDiscount",nil];
                                                                                           
                                                                                           success (rewards);
                                                                                       }];
                                                                                   }
                                                                               }
                                                                               
                                                                               
                                                                           }
                                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                               NSLog (@"Request (%@) Error: %@", operation.request.URL.relativeString, error);
                                                                               
                                                                               if (failure)
                                                                               {
                                                                                   failure (error);
                                                                               }
                                                                           }];
    
    [manager.operationQueue addOperation:operation];
    
    return operation;
    
    //    MRProgressOverlayView *overlayView = [MRProgressOverlayView showOverlayAddedTo:[[UIApplication sharedApplication] keyWindow]
    //                                                                             title:NSLocalizedString(@"Update price...", @"Update price...")
    //                                                                              mode:MRProgressOverlayViewModeIndeterminateSmall
    //                                                                          animated:YES];
    //    [overlayView setModeAndProgressWithStateOfOperation:operation];
    //    [overlayView setStopBlockForOperation:operation];
}


@end