//
//  JMEditProfileController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMEditProfileController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, retain) NSArray *headers;

@property (strong, nonatomic) IBOutlet UIImageView *avatarView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)changeAvatarTap:(id)sender;

- (IBAction)doneTap:(id)sender;

- (void)setAvatarImage:(UIImage *)avatarImage;

- (void)header:(NSString *)header changeString:(NSString *)string;

- (void)header:(NSString *)header didEndEditing:(NSString *)string;

- (void)errorAlertWithText:(NSString *)error;

- (BOOL)validateRow:(NSInteger)row;

@end
