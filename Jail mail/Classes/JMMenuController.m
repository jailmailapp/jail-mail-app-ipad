//
//  JMMenuController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMMenuController.h"

#import "JMCDRepository+Letter.h"
#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMLetter.h"
#import "MFSideMenu.h"
#import "UIImageView+Avatar.h"
#import "UIViewController+SideMenu.h"
#import "AppDelegate.h"

#import "JMFeedbackController.h"
#import "JMInboxesController.h"
#import "JMInformationController.h"
#import "JMRewardsController.h"
#import "JMSettingsController.h"
#import "JMDashboardController.h"

@interface JMMenuItem : NSObject

@property (strong) NSString *title;
@property (strong) id controller;

@end

@implementation JMMenuItem

@end

@interface JMMenuController ()<UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *_items;
}

@property (strong, nonatomic) IBOutlet UITableView *tableMenu;
@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIView *lastProfileView;

@end

@implementation JMMenuController

+ (NSString *)storyboardID
{
    return @"idMenuController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 44)];
    [logoView setImage:[UIImage imageNamed:@"logo_Jail_Mail"]];
    [logoView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = logoView;
    
    [self addMenuBarButton];
    
    [self.avatar circle];
    [self.avatar whiteBorder];
    
    _items = [NSMutableArray array];
    
    for (int i = 0; i < CountMenuItem; i++)
    {
        JMMenuItem *item = [[JMMenuItem alloc] init];
        
        switch (i) {
            case InboxesMenuItem:
                item.title = NSLocalizedString(@"Outboxes", @"Outboxes");
                break;
            case InfoMenuItem:
                item.title = NSLocalizedString(@"Information & Help", @"Information & Help");
                break;
            case SettingsMenuItem:
                item.title = NSLocalizedString(@"Settings", @"Settings");
                break;
            case FeedbackMenuItem:
                item.title = NSLocalizedString(@"Feedback/Share", @"Feedback/Share");
                break;
            case LoyaltyMenuItem:
                item.title = NSLocalizedString(@"Rewards", @"Rewards");
                break;
            default:
                break;
        }
        
        [_items addObject:item];
    }
    
    //[self selectItemWithIdentifier:InboxesMenuItem];
    
    [self updateLastRecipient];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateEventOccurred:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];
}

- (void)showMenu:(id)sender
{
    [self selectItemWithIdentifier:InboxesMenuItem];
}


- (void)menuStateEventOccurred:(NSNotification *)notification
{
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    
    if (event == MFSideMenuStateEventMenuWillOpen)
    {
        [self.tableMenu reloadData];
        [self updateLastRecipient];
    }
}


- (void)updateLastRecipient
{
    NSArray *letters = [[JMCDRepository newRepository] allLetters];
    
    if (letters.count)
    {
        self.lastProfileView.hidden = NO;
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:NO];
        letters = [letters sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        JMLetter *letter = [letters firstObject];
        
        [self.avatar setAvatarImage:letter.recipient.avatar];
        [self.nameLabel setText:letter.recipient.fullName];
        
        NSMutableAttributedString *description = [[NSMutableAttributedString alloc] initWithString:@""];
        
        if (letter.recipient.identifier.length > 0)
        {
            NSMutableAttributedString *string = [self attributedStringFromTitle:@"Inmate Id: " note:letter.recipient.identifier];
            [description appendAttributedString:string];
        }
        
        if (letter.recipient.bed.length > 0)
        {
            NSMutableAttributedString *string = [self attributedStringFromTitle:@"Bed: " note:letter.recipient.bed];
            [description appendAttributedString:string];
        }
        
        if ([letter.recipient.institution note].length > 0)
        {
            NSMutableAttributedString *string = [self attributedStringFromTitle:@"Address:\n" note:[letter.recipient.institution note]];
            [description appendAttributedString:string];
        }
        
        self.descriptionLabel.attributedText = description;
    }
    else {
        self.lastProfileView.hidden = YES;
    }
}


- (NSMutableAttributedString *)attributedStringFromTitle:(NSString *)title note:(NSString *)note
{
    NSString *string = [NSString stringWithFormat:@"%@%@\n", title, note];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.651 green:0.675 blue:0.706 alpha:1.000] range:NSMakeRange(0, title.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, title.length)];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(title.length, note.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:NSMakeRange(title.length, note.length)];
    
    return attributedString;
}


- (void)selectItemWithIdentifier:(MainMenuItem)identifier
{
    if (_items.count <= identifier)
    {
        return;
    }
    
    JMMenuItem *item = [_items objectAtIndex:identifier];
    
    if (item.controller != nil)
    {
        [self pushViewController:item.controller];
    }
    else {
        switch (identifier) {
            case InboxesMenuItem: {
                [self.navigationController popToRootViewControllerAnimated:NO];
                
                UINavigationController *navigationController = (UINavigationController *)[JMAppDelegate detailNavigation];
                [navigationController popToRootViewControllerAnimated:NO];
            }
                break;
            case InfoMenuItem: {
                JMInformationController *informationController = [JMInformationController controller];
                [self pushViewController:informationController];
                item.controller = informationController;
            }
                break;
            case SettingsMenuItem: {
                JMSettingsController *settingsController = [JMSettingsController controller];
                [self pushViewController:settingsController];
                item.controller = settingsController;
            }
                break;
            case FeedbackMenuItem: {
                JMFeedbackController *feedbackController = [JMFeedbackController controller];
                [self pushViewController:feedbackController];
                item.controller = feedbackController;
            }
                break;
            case LoyaltyMenuItem: {
                JMRewardsController *loyaltyController = [JMRewardsController controller];
                [self pushViewController:loyaltyController];
                item.controller = loyaltyController;
            }
                break;
            default:
                break;
        }
    }
}


- (void)pushViewController:(UIViewController *)viewController
{
    if (viewController)
    {
        UINavigationController *navigationController = (UINavigationController *)[JMAppDelegate detailNavigation];
        [navigationController popToRootViewControllerAnimated:NO];
        [navigationController pushViewController:viewController animated:NO];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellIdentifier = @"CellIdentifier";
    UITableViewCell *cell            = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if (!cell)
    {
        [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    }
    
    JMMenuItem *item = [_items objectAtIndex:indexPath.row];
    
    cell.textLabel.text = item.title;
    
    cell.accessoryView = nil;
    
    if (indexPath.row == InboxesMenuItem)
    {
        NSArray *letters = [[JMCDRepository newRepository] allLetters];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 20, 20)];
        label.layer.cornerRadius = label.frame.size.height/2;
        label.clipsToBounds      = YES;
        label.backgroundColor    = [UIColor colorWithRed:0.314 green:0.357 blue:0.435 alpha:1.000];
        label.textColor          = [UIColor colorWithRed:0.922 green:0.945 blue:0.965 alpha:1.000];
        label.text               = [NSString stringWithFormat:@"%lu", (unsigned long)letters.count];
        label.textAlignment      = NSTextAlignmentCenter;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
        [view addSubview:label];
        cell.accessoryView = view;
        
        cell.imageView.image = [UIImage imageNamed:@"icon_menu_inboxes"];
    }
    else if (indexPath.row == InfoMenuItem) {
        cell.imageView.image = [UIImage imageNamed:@"icon_menu_info"];
    }
    else if (indexPath.row == SettingsMenuItem) {
        cell.imageView.image = [UIImage imageNamed:@"icon_menu_option"];
    }
    else if (indexPath.row == FeedbackMenuItem) {
        cell.imageView.image = [UIImage imageNamed:@"icon_menu_feedback"];
    }
    else if (indexPath.row == LoyaltyMenuItem) {
        cell.imageView.image = [UIImage imageNamed:@"icon_menu_loyalty"];
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self selectItemWithIdentifier:(MainMenuItem)indexPath.row];
}


@end