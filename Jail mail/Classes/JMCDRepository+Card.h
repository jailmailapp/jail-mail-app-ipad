//
//  JMCDRepository+Card.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"

#import "JMCard.h"

@interface JMCDRepository (Card)

- (NSArray *)allCard;

- (NSArray *)allCardForUser:(JMUser *)user;

@end
