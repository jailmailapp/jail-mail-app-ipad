//
//  RMNavigationController.h
//  RedmineMobile
//
//  Created by Alexander Shvetsov on 31.01.14.
//  Copyright (c) 2014 Yanpix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMNavigationController : UINavigationController

- (void)setColorBlue;
- (void)setColorGray;

@end
