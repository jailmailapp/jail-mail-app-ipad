//
//  JMInformationController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 10/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@interface JMInformationController : UIViewController <StoryboardProtocol>

@end
