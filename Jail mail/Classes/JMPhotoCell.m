//
//  JMPhotoCell.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 04/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPhotoCell.h"

@implementation JMPhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        _photo = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, self.frame.size.width-10, self.frame.size.height-10)];
        _photo.clipsToBounds = YES;
        _photo.layer.borderWidth = 1;
        _photo.layer.borderColor = [UIColor grayColor].CGColor;
        [self.contentView addSubview:_photo];
        
        _photo.backgroundColor = [UIColor whiteColor];
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowOffset = CGSizeMake(-3, 3);
        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.5;
    }
    
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.photo.image = nil;
}


@end