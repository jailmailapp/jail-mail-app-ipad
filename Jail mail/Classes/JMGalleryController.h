//
//  JMGalleryController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@interface JMGalleryController : UIViewController <XibProtocol, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UICollectionView *galleryCollection;
@property (strong, nonatomic) IBOutlet UIButton *createButton;
@property (strong, nonatomic) IBOutlet UILabel *prompt;
@property (nonatomic, retain) NSArray *items;

- (void)reloadItems;
- (IBAction)createTap:(id)sender;

@end
