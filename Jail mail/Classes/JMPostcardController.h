//
//  JMPostCardController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 13/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@class JMLetter;

@interface JMPostcardController : UIViewController <StoryboardProtocol>

- (void)setLetter:(JMLetter *)letter;

@end
