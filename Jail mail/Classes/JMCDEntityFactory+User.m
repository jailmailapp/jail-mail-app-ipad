//
//  EntityFactory+User.m
//  Jail mail
//
//  Created by Alexander on 29.03.15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory+User.h"

@implementation JMCDEntityFactory (User)

- (JMUser *)newUser
{
    JMUser *newUser = (JMUser *)[self newEntityForClass:[JMUser class]];
    
    return newUser;
}


- (JMUser *)newUserWithFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email
{
    JMUser *user = [self newUser];
    user.firstName = firstName;
    user.lastName  = lastName;
    user.email     = email;
    
    return user;
}


- (JMUser *)newUserFromFacebookInfo:(NSDictionary *)info
{
    return nil;
}


- (JMUser *)newUserFromGoogleInfo:(NSDictionary *)info
{
    return nil;
}


@end