//
//  EntityFactory.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Entity;

@protocol JMEntityDelegate<NSObject>

@optional
- (void)entity:(id)entity failure:(NSError *)error;

@end

@interface JMAbstractEntityFactory : NSObject

@property (weak) id<JMEntityDelegate> delegate;

+ (id)newFactory;

- (Entity *)newEntityForClass:(Class)classEntity;

@end