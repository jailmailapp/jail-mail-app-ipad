//
//  JMCDEntityFactory+Attachments.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 30/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMAttachments.h"

@interface JMCDEntityFactory (Attachments)

- (JMAttachments *)newAttachments;

@end
