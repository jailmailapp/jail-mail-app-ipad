//
//  JMCDEntityFactory+Card.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 24/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory+Card.h"

@implementation JMCDEntityFactory (Card)

- (JMCard *)newCard
{
    JMCard *newCard = (JMCard *)[self newEntityForClass:[JMCard class]];
    
    return newCard;
}

@end
