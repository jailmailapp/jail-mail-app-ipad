//
//  JMInboxesCell.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 03/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInboxesCell.h"

#import "UIImageView+Avatar.h"

@implementation JMInboxesCell

- (void)awakeFromNib
{
    [self.avatar circle];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    self.avatar.layer.cornerRadius  = self.avatar.frame.size.height/2;
    self.avatar.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


@end
