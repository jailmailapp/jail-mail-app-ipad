//
//  UIViewController+SideMenu.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 03/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SideMenu)

- (void)addMenuBarButton;

@end
