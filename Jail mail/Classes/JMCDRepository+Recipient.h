//
//  JMCDRepository+Recipient.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"

#import "JMInmate.h"

@interface JMCDRepository (Recipient)

- (NSArray *)allRecipient;

@end
