//
//  JMProfileView.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMProfileView : UIView

@property (nonatomic) BOOL circle;

- (void)setAvatar:(UIImage *)avatar;
- (void)setName:(NSString *)name;

@end
