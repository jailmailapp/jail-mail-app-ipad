//
//  JMAuthorizeNet.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 25/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JMPackage.h"

typedef void (^authorizeCompletionBlock) (id response);
typedef void (^authorizeCancelBlock) (id response);
typedef void (^authorizeErrorBlock) (NSError *error);

@interface JMAuthorizeNet : NSObject

+ (id)sharedInstance;

- (void)payForTheLetter:(JMPackage *)packege
                success:(authorizeCompletionBlock)successBlock
                 cancel:(authorizeCancelBlock)cancelBlock
                  error:(authorizeErrorBlock)errorBlock
inPaymentViewControllerPresentedIn:(UIViewController *)viewController;

@end