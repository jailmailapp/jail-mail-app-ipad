//
//  UIImage+Save.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UIImage+Save.h"

@implementation UIImage (Save)

- (NSString *)applicationDocumentsDirectory
{
    NSArray *paths               = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}


- (NSString *)saveWithFileName:(NSString *)fileName
{
    if (fileName && fileName.length)
    {
        NSString *directoryPath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"/Images"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:directoryPath])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        
        NSString *path = [directoryPath stringByAppendingPathComponent:fileName];
        
        if ([self saveToFile:path])
        {
            return [NSString stringWithFormat:@"/Images/%@", fileName];
        }
    }
    
    return nil;
}


- (BOOL)deleteWithFileName:(NSString *)fileName
{
    if (fileName && fileName.length)
    {
        NSString *imageFile = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:fileName];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imageFile])
        {
            NSError *error = nil;
            
            [[NSFileManager defaultManager] removeItemAtPath:imageFile error:&error];
            
            if (error == nil)
            {
                NSLog(@"%@", error.localizedDescription);
                return NO;
            }
        }
        
        return YES;
    }
    
    return YES;
}


- (BOOL)saveToFile:(NSString *)path
{
    NSData *data = UIImagePNGRepresentation(self);
    return [data writeToFile:path atomically:YES];
}


@end