//
//  JMAttachmentsRequest.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 29/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JMPackage, JMAttachments;

@interface JMAttachmentsRequest : NSObject

+ (void)sendPhotosForPackage:(JMPackage *)package
                     success:(void (^)(id response))success
                     failure:(void (^)(NSError *error))failure
                    progress:(void (^)(id progress))progress;

@end
