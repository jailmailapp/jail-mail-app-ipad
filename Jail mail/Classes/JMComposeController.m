//
//  JMComposeController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMComposeController.h"

#import "JMCDEntityFactory+Letter.h"
#import "JMCDRepository.h"
#import "JMConst.h"
#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMPhotosController.h"
#import "JMRecipientsDelegate.h"
#import "JMSendersDelegate.h"
#import "JMUser.h"
#import "UIImageView+Avatar.h"
#import "UITextView+Placeholder.h"
#import "UITextView+Toolbar.h"
#import "UIViewController+ImageBackButton.h"

#import "JMAttachments.h"
#import "JMCardsController.h"
#import "JMRecipientsController.h"
#import "JMSendersController.h"
#import "PSTAlertController.h"

static NSString *const kMessage          = @"Message";
static NSString *const kSender           = @"Sender";
static NSString *const kRecipient        = @"Recipient";
static NSString *const kAttachments      = @"Attachments";
static NSString *const kAttachmentsCount = @"kAttachmentsCount";

@interface JMComposeController ()<
    JMSendersDelegate,
    JMRecipientsDelegate,
    PhotosControllerDelegate,
    UITextViewDelegate,
    UIActionSheetDelegate> {
    JMPhotosController *_photosController;
    BOOL _isPhotoMode;
}

@property (strong, nonatomic) IBOutlet UIImageView *letterStateView;
@property (strong, nonatomic) IBOutlet UITextView *textLetter;
@property (strong, nonatomic) IBOutlet UIImageView *senderAvatar;
@property (strong, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *senderInfoLabel;
@property (strong, nonatomic) IBOutlet UIButton *senderButton;
@property (strong, nonatomic) IBOutlet UIButton *recipientButton;
@property (strong, nonatomic) IBOutlet UIImageView *recipientAvatar;
@property (strong, nonatomic) IBOutlet UILabel *recipientNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *recipientInfoLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *attachmentHeight;
@property (strong, nonatomic) IBOutlet UIButton *attachmentButton;
@property (strong, nonatomic) IBOutlet UIImageView *attachmentArrow;
@property (strong, nonatomic) IBOutlet UIView *attachmentView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) JMLetter *letter;

@end

@implementation JMComposeController

+ (NSString *)storyboardID
{
    return @"idComposeController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Compose", @"Compose");

    [self setCustomBackButton];

    [self.textLetter keyboardToolbarSetup];
    self.textLetter.placeholder        = NSLocalizedString(@"Compose a letter...", @"Compose a letter...");
    self.textLetter.textContainerInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    self.textLetter.textAlignment      = NSTextAlignmentJustified;

    [self observeKeyboard];

    [self setPhotoMode:_isPhotoMode];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setPhotoMode:_isPhotoMode];

    [self updateUI];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self showAttachmentTap:nil];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [self saveLetter];

    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}


- (void)addNavigationButtons
{
    UIBarButtonItem *attachmentButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_attachment"]
                                                                         style:UIBarButtonItemStyleDone
                                                                        target:self
                                                                        action:@selector(showAttachmentTap:)];
    UIImage *image = nil;

    if (_letter.state == JMLetterStateDraft)
    {
        image = [UIImage imageNamed:@"icon_send"];
    }
    else if ((_letter.state == JMLetterStateInProcess) || (_letter.state == JMLetterStateError)) {
        image = [UIImage imageNamed:@"icon_send_continue"];
    }
    else if (_letter.state == JMLetterStateSubmitted) {
        image = [UIImage imageNamed:@"icon_locked"];
    }

    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(sendLetterTap:)];

    if (_isPhotoMode)
    {
        self.navigationItem.rightBarButtonItem = sendButton;
    }
    else {
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:sendButton, attachmentButton, nil];
    }
}


- (void)setPhotoMode:(BOOL)isPhotoMode
{
    _isPhotoMode = isPhotoMode;

    if (_isPhotoMode)
    {
        self.attachmentHeight.constant = CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.headerView.frame);

        if (_photosController)
        {
            [_photosController.view removeFromSuperview];
            _photosController = nil;
        }

        _photosController                      = [[JMPhotosController alloc] init];
        _photosController.delegate             = self;
        _photosController.view.backgroundColor = [UIColor clearColor];
        [_photosController setFrame:CGRectMake(0,
                                               0,
                                               self.view.bounds.size.width,
                                               self.attachmentHeight.constant)];
        [self.attachmentView addSubview:_photosController.view];
        self.attachmentView.backgroundColor = [UIColor clearColor];

        [_photosController setPhotos:[[_letter.attachments allObjects] mutableCopy]];

        self.attachmentButton.hidden = YES;
        self.attachmentArrow.hidden  = YES;
    }

    [self addNavigationButtons];
}


- (JMLetter *)letter
{
    if (_letter == nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        _letter = [[JMCDEntityFactory newFactory] newLetter];

        if (_isPhotoMode)
        {
            _letter.type    = JMLetterTypePhoto;
            _letter.message = NSLocalizedString(@"Jail Mail Photos Order", @"Jail Mail Photos Order");
        }
        else {
            _letter.type = JMLetterTypeMail;
        }

        [repository addEntity:_letter];
    }

    return _letter;
}


- (void)editLetter:(JMLetter *)letter
{
    _letter = letter;
    
    [self updateUI];
    
    if (_photosController)
    {
        [_photosController setPhotos:[[_letter.attachments allObjects] mutableCopy]];
        //[_photosController reloadData];
    }
}


- (void)blockUI:(BOOL)block
{
    self.senderButton.enabled    = !block;
    self.recipientButton.enabled = !block;
    self.textLetter.editable     = !block;
    _photosController.editing    = !block;
    self.editing                 = !block;
}


- (NSArray *)wrongField
{
    NSMutableArray *wrongFields = [[NSMutableArray alloc] init];

    if ((_letter.attachments == nil) || (_letter.attachments.count == 0))
    {
        [wrongFields addObject:kAttachments];
    }
    else if ((_letter.attachments > 0) && ((_letter.attachments.count < MIN_PHOTO_COUNT) && _isPhotoMode)) {
        [wrongFields addObject:kAttachmentsCount];
    }

    if ((_letter.message == nil) || (_letter.message.length == 0))
    {
        [wrongFields addObject:kMessage];
    }

    if (_letter.sender == nil)
    {
        [wrongFields addObject:kSender];
    }

    if (_letter.recipient == nil)
    {
        [wrongFields addObject:kRecipient];
    }

    return wrongFields;
}


- (BOOL)validateLetter
{
    NSArray *wrongFields = [self wrongField];

    if ([wrongFields containsObject:kMessage] && [wrongFields containsObject:kAttachments])
    {
        [self errorAlertWithText:NSLocalizedString(@"Enter a message or attachments.", @"Enter a message or attachments.")];
    }
    else if ([wrongFields containsObject:kSender]) {
        [self errorAlertWithText:NSLocalizedString(@"Select sender.", @"Select sender.")];
    }
    else if ([wrongFields containsObject:kRecipient]) {
        [self errorAlertWithText:NSLocalizedString(@"Select contact.", @"Select contact.")];
    }
    else if ([wrongFields containsObject:kAttachmentsCount] && _isPhotoMode) {
        [self errorAlertWithText:[NSString stringWithFormat:NSLocalizedString(@"If only sending photos, you must send at least %d", @"If only sending photos, you must send at least %d"), MIN_PHOTO_COUNT]];
    }

    return wrongFields.count == 0 || (wrongFields.count == 1 && ([wrongFields containsObject:kMessage] || [wrongFields containsObject:kAttachments]));
}


- (void)errorAlertWithText:(NSString *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                    message:error
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    [alert show];
}


- (void)updateUI
{
    JMUser *user = _letter.sender;

    if (user != nil)
    {
        [self.senderAvatar setAvatarImage:user.avatar];
        [self.senderNameLabel setText:user.fullName];
        [self.senderInfoLabel setText:user.email];
        [self.senderButton setTitle:@"" forState:UIControlStateNormal];
        [self.senderAvatar blueBorder];
    }
    else {
        [self.senderAvatar setImage:[UIImage imageNamed:@"icon_Add_receiver"]];
        [self.senderNameLabel setText:@""];
        [self.senderInfoLabel setText:@""];
        [self.senderButton setTitle:@"Add sender" forState:UIControlStateNormal];
        [self.senderAvatar whiteBorder];
    }

    JMInmate *recipient = _letter.recipient;

    if (recipient != nil)
    {
        [self.recipientAvatar setAvatarImage:recipient.avatar];
        [self.recipientNameLabel setText:recipient.fullName];
        [self.recipientInfoLabel setText:recipient.institution.name];
        [self.recipientButton setTitle:@"" forState:UIControlStateNormal];
        [self.recipientAvatar blueBorder];
    }
    else {
        [self.recipientAvatar setImage:[UIImage imageNamed:@"icon_Add_receiver"]];
        [self.recipientNameLabel setText:@""];
        [self.recipientInfoLabel setText:@""];
        [self.recipientButton setTitle:@"Add contact" forState:UIControlStateNormal];
        [self.recipientAvatar whiteBorder];
    }

    [self.textLetter setText:_letter.message];
    [self.senderAvatar circle];
    [self.recipientAvatar circle];

    [self updateAttachmentsLabel];

    if (_letter.state == JMLetterStateDraft)
    {
        self.letterStateView.image = nil;
        [self blockUI:NO];
    }
    else {
        [self blockUI:YES];

        if ((_letter.state == JMLetterStateInProcess) || (_letter.state == JMLetterStateError))
        {
            self.letterStateView.image = [UIImage imageNamed:@"process_failed"];
        }
        else if (_letter.state == JMLetterStateSubmitted) {
            self.letterStateView.image = [UIImage imageNamed:@"proces_sended"];
        }
    }

    [self addNavigationButtons];
}


- (BOOL)saveLetter
{
    JMCDRepository *repository = [JMCDRepository newRepository];

    if (![repository save])
    {
        return NO;
    }

    return YES;
}


- (BOOL)deleteLetter
{
    if (_letter != nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        [repository deleteEntity:_letter];

        if (![repository save])
        {
            return NO;
        }

        _letter = nil;
    }

    return YES;
}


#pragma mark - IBAction

- (IBAction)selectSenderTap:(id)sender
{
    JMSendersController *senderController = [JMSendersController controller];
    senderController.delegate = self;
    [self.navigationController pushViewController:senderController animated:YES];
}


- (IBAction)selectRecipientTap:(id)sender
{
    JMRecipientsController *recipientController = [JMRecipientsController controller];
    recipientController.delegate = self;
    [self.navigationController pushViewController:recipientController animated:YES];
}


- (IBAction)sendLetterTap:(id)sender
{
    [self.textLetter resignFirstResponder];

    if (_letter.state == JMLetterStateDraft)
    {
        if ([self validateLetter])
        {
            [self saveLetter];

            JMCardsController *controller = [JMCardsController controller];
            [controller setLetter:_letter];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
    else if ((_letter.state == JMLetterStateInProcess) || (_letter.state == JMLetterStateError)) {
        JMCardsController *controller = [JMCardsController controller];
        [controller setLetter:_letter];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (_letter.state == JMLetterStateSubmitted) {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:@"Upadete to Draft" preferredStyle:PSTAlertControllerStyleActionSheet];

        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"YES", @"YES")
                                                             style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            _letter.state = JMLetterStateDraft;
            _letter.letterID = nil;
            _letter.userID = nil;
            _letter.sentAt = nil;

            for (JMAttachments *attachments in _letter.attachments)
            {
                [attachments setState:JMAttachmentsStateNew];
            }

            [self saveLetter];

            [self updateUI];
        }]];

        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];

        [alertController showWithSender:sender controller:self animated:YES completion:NULL];
    }
}


- (IBAction)showAttachmentTap:(id)sender
{
    if (_isPhotoMode)
    {
        return;
    };

    [self.textLetter resignFirstResponder];

    if (self.attachmentHeight.constant == self.attachmentButton.frame.size.height)
    {
        self.attachmentHeight.constant = 300;
        self.attachmentArrow.image     = [UIImage imageNamed:@"icon_down"];
    }
    else {
        self.attachmentHeight.constant = self.attachmentButton.frame.size.height;
        self.attachmentArrow.image     = [UIImage imageNamed:@"icon_up"];
    }

    if (_photosController)
    {
        [_photosController.view removeFromSuperview];
        _photosController = nil;
    }

    _photosController          = [[JMPhotosController alloc] init];
    _photosController.editing  = self.editing;
    _photosController.delegate = self;
    [_photosController setFrame:CGRectMake(0,
                                           self.attachmentButton.frame.size.height,
                                           self.attachmentView.frame.size.width,
                                           self.attachmentHeight.constant-self.attachmentButton.frame.size.height)];
    [self.attachmentView addSubview:_photosController.view];

    [_photosController setPhotos:[[_letter.attachments allObjects] mutableCopy]];

    [self updateAttachmentsLabel];
}


- (void)updateAttachmentsLabel
{
    NSInteger count = 0;
    count = [_letter.attachments allObjects].count;

    NSString *title = [NSString stringWithFormat:@"%lu attachments", (long)count];
    [self.attachmentButton setTitle:title forState:UIControlStateNormal];
}


- (IBAction)backButtonTap:(id)sender
{
    if ([_letter hasChanges])
    {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];

        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"Save changes", @"Save changes")
                                                             style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            if ([self saveLetter])
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }]];

        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Rollback changes", @"Rollback changes")
                                                             style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            if ([self deleteLetter])
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }]];

        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
        [alertController showWithSender:sender controller:self animated:YES completion:NULL];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - PhotosControllerDelegate

- (void)photosDidUpdated:(JMPhotosController *)controller
{
    JMCDRepository *repository = [JMCDRepository newRepository];

    for (JMAttachments *attachments in controller.photos)
    {
        if (![self.letter.attachments containsObject:attachments])
        {
            [repository addEntity:attachments];
            [self.letter addAttachmentsObject:attachments];
        }
    }

    [self updateAttachmentsLabel];
}


- (void)checkZipCode
{
    if ([_letter.sender.zipCode isEqualToString:_letter.recipient.institution.zipCode])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"Warning")
                                                        message:NSLocalizedString(@"Are you sure you are entering the correct address info?", @"Are you sure you are entering the correct address info?")
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
        [alert show];
    }
}


#pragma mark - JMSenderControllerDelegate

- (void)selectedSender:(id)sender
{
    if ([sender isKindOfClass:JMUser.class])
    {
        self.letter.sender = sender;

        [self checkZipCode];
    }

    [self updateUI];
}


#pragma mark - JMRecipientControllerDelegate

- (void)selectedRecipient:(id)sender
{
    if ([sender isKindOfClass:JMInmate.class])
    {
        self.letter.recipient = sender;

        [self checkZipCode];
    }

    [self updateUI];
}


#pragma mark -

- (void)textViewDidChange:(UITextView *)textView
{
    NSString *text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.letter.message = text;
}


- (void)observeKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        id _obj = [note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect _keyboardFrame = CGRectNull;

        if ([_obj respondsToSelector:@selector(getValue:)])
        {
            [_obj getValue:&_keyboardFrame];
        }

        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.attachmentHeight.constant = _keyboardFrame.size.height;
        } completion:nil];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.attachmentHeight.constant = self.attachmentButton.frame.size.height;
        } completion:nil];
    }];
}


- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    [self showAttachmentTap:nil];
    [self showAttachmentTap:nil];
}


@end