//
//  InboxesController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+CustomInit.h"

@interface JMInboxesController : UIViewController <StoryboardProtocol>

@property (nonatomic) UIViewController *detail;

@end