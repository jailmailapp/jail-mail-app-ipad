//
//  JMFeedbackController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 08/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMFeedbackController.h"

#import "UIViewController+SideMenu.h"
#import <MRProgress/MRProgress.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

#define ITUNES_URL @"https://itunes.apple.com/us/app/jail-mail/id518643782?mt=8"

@interface JMFeedbackController ()<MFMailComposeViewControllerDelegate, UIDocumentInteractionControllerDelegate>
{
    UIDocumentInteractionController *documentController;
}

@end

@implementation JMFeedbackController

+ (NSString *)storyboardID
{
    return @"idFeedbackController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Feedback", @"Feedback");
    
    [self addMenuBarButton];
}


#pragma mark Rate

- (IBAction)rateTap:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ITUNES_URL]];
}


#pragma mark Facebook

- (IBAction)facebookTap:(id)sender
{
    [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
    
    SLComposeViewController *facebookSheet = [[SLComposeViewController alloc] init];
    facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [facebookSheet addImage:[UIImage imageNamed:@"facebook_banner"]];
    [facebookSheet addURL:[[NSURL alloc] initWithString:ITUNES_URL]];
    [facebookSheet setCompletionHandler:^(SLComposeViewControllerResult result)
     {
         [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
         
         if (result == SLComposeViewControllerResultDone)
         {
             NSLog (@"The user posted to Facebook");
             
             [[[UIAlertView alloc] initWithTitle:@"Successful"
                                         message:@"The message was published in Fasebook."
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
         }
     }];
    
    [self presentViewController:facebookSheet animated:YES completion:nil];
}

#pragma mark Twitter

- (IBAction)twitterTap:(id)sender
{
    [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
    
    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetSheet setInitialText:[self shareText]];
    [tweetSheet addImage:[UIImage imageNamed:@"instagram_banner"]];
    [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result)
     {
         [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
         
         if (result == SLComposeViewControllerResultDone)
         {
             [[[UIAlertView alloc] initWithTitle:@"Successful"
                                         message:@"The message was published in Twitter."
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
         }
     }];
    
    [self presentViewController:tweetSheet animated:YES completion:nil];
}

#pragma mark Instagram

- (IBAction)instagramTap:(id)sender
{
    [self shareImageOnInstagram:[UIImage imageNamed:@"instagram_banner"]];
}


#pragma mark -

- (NSString *)shareText
{
    NSString *string = [NSString stringWithFormat:@"Send letters, photos and postcards to your incarcerated loved ones easily using Jail Mail app https://itunes.apple.com/us/app/jail-mail/id518643782?mt=8"];
    
    return string;
}


- (IBAction)showEmail:(id)sender
{
    NSString *emailTitle  = @"Contact Jail Mail Team";
    NSString *messageBody = @"Got questions about any of our products or services? Send us an email with as many tech baffling questions as you like! Our friendly sales team will answer all your questions!";
    NSArray *toRecipents  = [NSArray arrayWithObject:@"support@jailmailapp.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
    
    /*
     - Реализацыя Instagram отдельной кнопкой требует довольно много работы. Так как у них нет своего SDK, а только свой API. Нам прийдется все делать вручную. У нас приложение не орентировано на шаринг фото и это скорее всего одноразовая функцыя. Поэтому если это не крейне небходимо нам стоит оставить так как есть сейчас.
     */
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)shareImageOnInstagram:(UIImage *)shareImage
{
    NSString *documentDirectory = [NSHomeDirectory () stringByAppendingPathComponent:@"Documents"];
    NSString *saveImagePath     = [documentDirectory stringByAppendingPathComponent:@"Image.ig"];
    NSData *imageData           = UIImagePNGRepresentation(shareImage);
    [imageData writeToFile:saveImagePath atomically:YES];
    
    documentController            = [[UIDocumentInteractionController alloc] init];
    documentController.delegate   = self;
    documentController.UTI        = @"com.instagram.photo";
    documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[self shareText], @"InstagramCaption", nil];
    [documentController setURL:[NSURL fileURLWithPath:saveImagePath]];
    [documentController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
}


@end