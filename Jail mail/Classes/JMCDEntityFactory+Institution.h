//
//  JMCDEntityFactory+Institution.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMInstitution.h"

@interface JMCDEntityFactory (Institution)

- (JMInstitution *)newInstitution;

@end
