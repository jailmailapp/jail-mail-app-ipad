//
//  JMAuthorizeNet.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 25/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMAuthorizeNet.h"

@implementation JMAuthorizeNet

+ (id)sharedInstance
{
    return nil;
}

- (void)payForTheLetter:(JMPackage *)packege
                success:(authorizeCompletionBlock)successBlock
                 cancel:(authorizeCancelBlock)cancelBlock
                  error:(authorizeErrorBlock)errorBlock
inPaymentViewControllerPresentedIn:(UIViewController *)viewController
{
    cancelBlock(nil);
}

@end
/*
 #import <PassKit/PassKit.h>
 #import <AddressBook/AddressBook.h>
 #import <AddressBook/ABRecord.h>
 #import "AuthNet.h"
 #import "Address.h"
 #import "NSString+Base64.h"
 #import "NSString+HMAC_MD5.h"
 #import "JMPaymentRequest.h"
 //#import "STPTestPaymentAuthorizationViewController.h"
 
 #define PAYMENT_SUCCESSFUL @"Your transaction of $0.01 has been processed successfully."
 
 @interface JMAuthorizeNet ()<AuthNetDelegate, PKPaymentAuthorizationViewControllerDelegate, NSXMLParserDelegate> {
 UIViewController *_controller;
 AuthNet *authNet;
 
 NSTimeInterval fingerprintTimestamp;
 NSString *fingerprintHashValue;
 NSString *fingerprintInvoice;
 NSString *paymentEncryptedData;
 BOOL authorizationDone;
 
 authorizeCompletionBlock _successBlock;
 authorizeCancelBlock _cancelBlock;
 authorizeErrorBlock _errorBlock;
 JMPackage *_packege;
 }
 
 @property(nonatomic, strong) NSString *elementName;
 @property(nonatomic, strong) NSMutableString *outstring;
 
 @property (nonatomic, retain) Address *billingAddress;
 @property (nonatomic, retain) Address *shippingAddress;
 
 @end
 
 @implementation JMAuthorizeNet
 
 + (id)sharedInstance
 {
 static JMAuthorizeNet *_instance = nil;
 static dispatch_once_t onceToken;
 
 dispatch_once(&onceToken, ^{
 if (_instance == nil)
 {
 _instance = [[super allocWithZone:NULL] init];
 }
 });
 
 return _instance;
 }
 
 
 + (id)allocWithZone:(NSZone *)zone
 {
 return [self sharedInstance];
 }
 
 
 - (id)copyWithZone:(NSZone *)zone
 {
 return self;
 }
 
 - (NSString *)amount
 {
 NSLog(@"AuthorizeNet amount:%@",[NSString stringWithFormat:@"%.2f", [_packege priceTotal]]);
 
 return [NSString stringWithFormat:@"%.2f", [_packege priceTotal]];
 }
 
 #pragma mark -
 
 - (void)payForTheLetter:(JMPackage *)packege
 success:(authorizeCompletionBlock)successBlock
 cancel:(authorizeCancelBlock)cancelBlock
 error:(authorizeErrorBlock)errorBlock
 inPaymentViewControllerPresentedIn:(UIViewController *)viewController
 {
 _successBlock = successBlock;
 _cancelBlock = cancelBlock;
 _errorBlock = errorBlock;
 _controller = viewController;
 
 _packege = packege;
 
 [self presentPaymentController];
 }
 
 #pragma mark - AuthNetDelegate Methods
 
 
 - (void)paymentSucceeded:(CreateTransactionResponse*)response {
 
 NSLog(@"Payment Success ********************** ");
 
 //    NSString *title = @"Successfull Transaction";
 //    NSString *alertMsg = nil;
 //    UIAlertView *PaumentSuccess = nil;
 
 TransactionResponse *transResponse = response.transactionResponse;
 
 _successBlock(transResponse.transId);
 
 //    alertMsg = [response responseReasonText];
 NSLog(@"%@",response.responseReasonText);
 
 //    if ([transResponse.responseCode isEqualToString:@"4"])
 //    {
 //        PaumentSuccess = [[UIAlertView alloc] initWithTitle:title message:alertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"LOGOUT",nil];
 //    }
 //    else
 //    {
 //        PaumentSuccess = [[UIAlertView alloc] initWithTitle:title message:PAYMENT_SUCCESSFUL delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"LOGOUT",nil];
 //    }
 //
 //    [PaumentSuccess show];
 }
 
 
 - (void)paymentCanceled {
 
 NSLog(@"Payment Canceled ********************** ");
 
 _cancelBlock(nil);
 }
 
 
 -(void) requestFailed:(AuthNetResponse *)response {
 
 NSLog(@"Request Failed ********************** ");
 
 NSString *title = nil;
 NSString *alertErrorMsg = nil;
 UIAlertView *alert = nil;
 
 if ( [response errorType] == SERVER_ERROR)
 {
 title = NSLocalizedString(@"Server Error", @"");
 alertErrorMsg = [response responseReasonText];
 }
 else if([response errorType] == TRANSACTION_ERROR)
 {
 title = NSLocalizedString(@"Transaction Error", @"");
 alertErrorMsg = [response responseReasonText];
 }
 else if([response errorType] == CONNECTION_ERROR)
 {
 title = NSLocalizedString(@"Connection Error", @"");
 alertErrorMsg = [response responseReasonText];
 }
 
 Messages *ma = response.anetApiResponse.messages;
 
 AuthNetMessage *m = [ma.messageArray objectAtIndex:0];
 
 NSLog(@"Response Msg Array Count: %lu", (unsigned long)[ma.messageArray count]);
 
 NSLog(@"Response Msg Code %@ ", m.code);
 
 NSString *errorCode = [NSString stringWithFormat:@"%@",m.code];
 NSString *errorText = [NSString stringWithFormat:@"%@",m.text];
 
 NSString *errorMsg = [NSString stringWithFormat:@"%@ : %@", errorCode, errorText];
 
 if (alertErrorMsg == nil) {
 alertErrorMsg = errorText;
 }
 
 NSLog(@"Error Code and Msg %@", errorMsg);
 
 
 if ( ([m.code isEqualToString:@"E00027"]) || ([m.code isEqualToString:@"E00007"]) || ([m.code isEqualToString:@"E00096"]))
 {
 
 alert = [[UIAlertView alloc] initWithTitle:title
 message:alertErrorMsg
 delegate:self
 cancelButtonTitle:NSLocalizedString(@"OK", @"")
 otherButtonTitles:nil];
 }
 else if ([m.code isEqualToString:@"E00008"]) // Finger Print Value is not valid.
 {
 alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error"
 message:errorText
 delegate:self
 cancelButtonTitle:NSLocalizedString(@"OK", @"")
 otherButtonTitles:nil];
 }
 else
 {
 alert = [[UIAlertView alloc] initWithTitle:title
 message:alertErrorMsg
 delegate:self
 cancelButtonTitle:NSLocalizedString(@"OK", @"")
 otherButtonTitles:nil];
 }
 [alert show];
 
 _errorBlock(nil);
 
 return;
 }
 
 
 - (void) connectionFailed:(AuthNetResponse *)response {
 NSLog(@"%@", response.responseReasonText);
 NSLog(@"Connection Failed");
 
 NSString *title = nil;
 NSString *message = nil;
 
 if ([response errorType] == NO_CONNECTION_ERROR)
 {
 title = NSLocalizedString(@"No Signal", @"");
 message = NSLocalizedString(@"Unable to complete your request. No Internet connection.", @"");
 }
 else
 {
 title = NSLocalizedString(@"Connection Error", @"");
 message = NSLocalizedString(@"A connection error occurred.  Please try again.", @"");
 }
 
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
 message:message
 delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
 alert.delegate = self;
 [alert show];
 
 }
 
 
 -(void)logoutSucceeded:(LogoutResponse *)response
 {
 NSLog(@"Logout Success ********************** ");
 }
 
 
 #pragma mark - From Test PDF
 
 -(CreateTransactionRequest *)createTransactionReqObjectWithApiLoginID:(NSString *) apiLoginID
 fingerPrintHashData:(NSString *) fpHashData
 sequenceNumber:(NSInteger) sequenceNumber
 transactionType:(AUTHNET_ACTION) transactionType
 opaqueDataValue:(NSString*) opaqueDataValue
 dataDescriptor:(NSString *) dataDescriptor
 invoiceNumber:(NSString *) invoiceNumber
 totalAmount:(NSDecimalNumber*) totalAmount
 fpTimeStamp:(NSTimeInterval) fpTimeStamp
 {
 CreateTransactionRequest *transactionRequestObj = [CreateTransactionRequest createTransactionRequest];
 TransactionRequestType *transactionRequestType = [TransactionRequestType transactionRequest];
 
 transactionRequestObj.transactionRequest = transactionRequestType;
 transactionRequestObj.transactionType = transactionType;
 
 // Set the fingerprint.
 // Note: Finger print generation requires transaction key.
 // Finger print generation must happen on the server.
 
 FingerPrintObjectType *fpObject = [FingerPrintObjectType fingerPrintObjectType];
 fpObject.hashValue = fpHashData;
 fpObject.sequenceNumber = (int)sequenceNumber;
 fpObject.timeStamp = fpTimeStamp;
 
 transactionRequestObj.anetApiRequest.merchantAuthentication.fingerPrint = fpObject;
 transactionRequestObj.anetApiRequest.merchantAuthentication.name = apiLoginID;
 
 // Set the Opaque data
 OpaqueDataType *opaqueData = [OpaqueDataType opaqueDataType];
 opaqueData.dataValue= opaqueDataValue;
 opaqueData.dataDescriptor = dataDescriptor;
 
 PaymentType *paymentType = [PaymentType paymentType];
 paymentType.creditCard= nil;
 paymentType.bankAccount= nil;
 paymentType.trackData= nil;
 paymentType.swiperData= nil;
 paymentType.opData = opaqueData;
 
 transactionRequestObj.transactionRequest.billTo.firstName = self.billingAddress.firstName;
 transactionRequestObj.transactionRequest.billTo.lastName = self.billingAddress.lastName;
 transactionRequestObj.transactionRequest.billTo.address = self.billingAddress.street1;
 transactionRequestObj.transactionRequest.billTo.city = self.billingAddress.city;
 transactionRequestObj.transactionRequest.billTo.zip = self.billingAddress.zip;
 transactionRequestObj.transactionRequest.billTo.state = self.billingAddress.state;
 transactionRequestObj.transactionRequest.billTo.country = self.billingAddress.country;
 transactionRequestObj.transactionRequest.billTo.phoneNumber = self.billingAddress.phone;
 
 transactionRequestObj.transactionRequest.shipTo.firstName = self.shippingAddress.firstName;
 transactionRequestObj.transactionRequest.shipTo.lastName = self.shippingAddress.lastName;
 transactionRequestObj.transactionRequest.shipTo.address = self.shippingAddress.street1;
 transactionRequestObj.transactionRequest.shipTo.city = self.shippingAddress.city;
 transactionRequestObj.transactionRequest.shipTo.zip = self.shippingAddress.zip;
 transactionRequestObj.transactionRequest.shipTo.state = self.shippingAddress.state;
 transactionRequestObj.transactionRequest.shipTo.country = self.shippingAddress.country;
 
 transactionRequestObj.transactionRequest.customer.email = self.shippingAddress.email;
 
 transactionRequestType.amount = [NSString stringWithFormat:@"%@",totalAmount];
 transactionRequestType.payment = paymentType;
 transactionRequestType.retail.marketType = @"0"; //0
 transactionRequestType.retail.deviceType = @"7";
 
 OrderType *orderType = [OrderType order];
 orderType.invoiceNumber = invoiceNumber;
 
 NSLog(@"Invoice Number Before Sending the Request %@", orderType.invoiceNumber);
 
 return transactionRequestObj;
 }
 
 
 - (void) performTransactionWithEncryptedPaymentData:(NSString *)encryptedPaymentData
 {
 paymentEncryptedData = encryptedPaymentData;
 
 [JMPaymentRequest fingerprintWithAmount:[self amount]
 success:^(id response) {
 ;
 NSXMLParser *XMLParser = (NSXMLParser *)response;
 [XMLParser setShouldProcessNamespaces:YES];
 XMLParser.delegate = self;
 [XMLParser parse];
 }
 failure:^(NSError *error) {
 ;
 }];
 }
 
 
 -(void) updateAddressObject:(Address *)addressObj fromPKPaymentABRecord:(ABRecordRef *)PKPaymentABRecord
 {
 if (PKPaymentABRecord != nil && addressObj != nil)
 {
 
 addressObj.firstName = (__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord,kABPersonFirstNameProperty);
 addressObj.lastName = (__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord, kABPersonLastNameProperty);
 
 ABMultiValueRef emails = CFBridgingRetain((__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord, kABPersonEmailProperty));
 
 if(ABMultiValueGetCount(emails) > 0 )
 {
 for(CFIndex i = 0; i< ABMultiValueGetCount(emails); i++ )
 {
 CFStringRef label = ABMultiValueCopyLabelAtIndex(emails, i);
 CFStringRef value = ABMultiValueCopyValueAtIndex(emails, i);
 addressObj.email = (__bridge NSString*)value;
 if (label) CFRelease(label);
 if (value) CFRelease(value);
 }
 }
 
 
 ABMultiValueRef phoneNumbers = CFBridgingRetain((__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord, kABPersonPhoneProperty));
 
 if(ABMultiValueGetCount(phoneNumbers) > 0 )
 {
 for(CFIndex i = 0; i< ABMultiValueGetCount(phoneNumbers); i++ )
 {
 CFStringRef label = ABMultiValueCopyLabelAtIndex(phoneNumbers, i);
 CFStringRef value = ABMultiValueCopyValueAtIndex(phoneNumbers, i);
 addressObj.phone = (__bridge NSString*)value;
 if (label) CFRelease(label);
 if (value) CFRelease(value);
 }
 }
 
 ABMultiValueRef streetAddress = ABRecordCopyValue(PKPaymentABRecord, kABPersonAddressProperty);
 if (ABMultiValueGetCount(streetAddress) > 0)
 {
 NSDictionary *theDict = (__bridge NSDictionary*)ABMultiValueCopyValueAtIndex(streetAddress, 0);
 
 addressObj.street1 = [theDict objectForKey:(NSString *)kABPersonAddressStreetKey];
 addressObj.city = [theDict objectForKey:(NSString *)kABPersonAddressCityKey];
 addressObj.state = [theDict objectForKey:(NSString *)kABPersonAddressStateKey];
 addressObj.zip = [theDict objectForKey:(NSString *)kABPersonAddressZIPKey];
 addressObj.country = [theDict objectForKey:(NSString *)kABPersonAddressCountryKey];
 }
 CFRelease(streetAddress);
 }
 else
 {
 NSLog(@"Invalid Params");
 }
 }
 
 
 #pragma mark - Authorization delegate methods
 
 
 - (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
 didAuthorizePayment:(PKPayment *)payment
 completion:(void (^)(PKPaymentAuthorizationStatus))completion {
 
 NSLog(@"payment %@", payment);
 
 authorizationDone = YES;
 
 if (payment)
 {
 // Go off and auth the card
 self.billingAddress = [[Address alloc] init];
 self.shippingAddress = [[Address alloc] init];
 
 if (payment.billingAddress != nil && self.billingAddress != nil)
 {
 [self updateAddressObject:self.billingAddress fromPKPaymentABRecord:(ABRecordRef*)payment.billingAddress];
 
 }
 
 if (payment.shippingAddress != nil && self.shippingAddress != nil)
 {
 [self updateAddressObject:self.shippingAddress fromPKPaymentABRecord:(ABRecordRef*)payment.shippingAddress];
 
 }
 
 // Since the Phone and Email is enabled only if we enable the Shipping, Just to use for the billing we do this.
 
 if (self.billingAddress.phone == nil)
 {
 self.billingAddress.phone = self.shippingAddress.phone;
 }
 
 if (self.billingAddress.email == nil)
 {
 self.billingAddress.email = self.shippingAddress.email;
 }
 
 NSString *base64string = [NSString base64forData:payment.token.paymentData];
 
 [self performTransactionWithEncryptedPaymentData:base64string];
 
 completion(PKPaymentAuthorizationStatusSuccess);
 }
 else
 {
 completion(PKPaymentAuthorizationStatusFailure);
 }
 }
 
 
 - (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller
 {
 [controller dismissViewControllerAnimated:YES completion:^{
 
 if (authorizationDone == NO) {
 _cancelBlock(nil);
 }
 }];
 }
 
 
 // ApplePay with Passkit
 -(void )presentPaymentController
 {
 PKPaymentRequest *request = [[PKPaymentRequest alloc] init];
 
 request.currencyCode = @"USD";
 request.countryCode = @"US";
 // This is a test merchant id to demo the capability, this would work with Visa cards only.
 request.merchantIdentifier = @"merchant.com.notforgottenllc.applepay";  // replace with YOUR_APPLE_MERCHANT_ID == merchant.anet.sampleapp.dev.001
 request.applicationData = [@"" dataUsingEncoding:NSUTF8StringEncoding];
 request.merchantCapabilities = PKMerchantCapability3DS;
 request.supportedNetworks = @[PKPaymentNetworkMasterCard, PKPaymentNetworkVisa, PKPaymentNetworkAmex];
 //    request.requiredBillingAddressFields = PKAddressFieldPostalAddress|PKAddressFieldPhone|PKAddressFieldEmail;
 //    request.requiredShippingAddressFields = PKAddressFieldPostalAddress|PKAddressFieldPhone|PKAddressFieldEmail;
 
 // Set amount here
 NSString *amountText =  [self amount]; // Get the payment amount
 NSDecimalNumber *amountValue = [NSDecimalNumber decimalNumberWithString:amountText];
 
 PKPaymentSummaryItem *item = [[PKPaymentSummaryItem alloc] init];
 item.amount = amountValue;
 item.label = @"Test Payment Total";
 
 request.paymentSummaryItems = @[item];
 
 authorizationDone = NO;
 
 PKPaymentAuthorizationViewController *сontroller = (PKPaymentAuthorizationViewController *)[[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
 
 if (сontroller != nil)
 {
 сontroller.delegate = self;
 [_controller presentViewController:сontroller animated:YES completion:CompletionBlock];
 } else {
 //The device cannot make payments. Please make sure Passbook has valid Credit Card added.
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"PassKit Payment Error"
 message:NSLocalizedString(@"The device cannot make payment at this time. Please check Passbook has Valid Credit Card and Payment Request has Valid Currency & Apple MerchantID.", @"")
 delegate:nil
 cancelButtonTitle:NSLocalizedString(@"OK", @"")
 otherButtonTitles:nil];
 [alert show];
 }
 }
 
 
 void (^CompletionBlock)(void) = ^
 {
 NSLog(@"This is Completion block");
 
 };
 
 
 #pragma mark - NSXMLParser
 
 - (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
 {
 self.elementName = qName;
 self.outstring = [NSMutableString string];
 }
 
 
 - (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
 {
 // 1
 if ([elementName isEqualToString:@"hashValue"]) {
 fingerprintHashValue = self.outstring;
 }
 // 2
 else if ([elementName isEqualToString:@"sequence"]) {
 fingerprintInvoice = self.outstring;
 }
 // 3
 else if ([elementName isEqualToString:@"timestamp"]) {
 fingerprintTimestamp = [self.outstring integerValue];
 }
 }
 
 - (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
 {
 if (!self.elementName)
 return;
 
 [self.outstring appendFormat:@"%@", string];
 }
 
 
 - (void)parserDidEndDocument:(NSXMLParser *)parser
 {
 if ([parser parserError]) {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error parse fingerprint"
 message:[[parser parserError] localizedDescription]
 delegate:nil
 cancelButtonTitle:NSLocalizedString(@"OK", @"")
 otherButtonTitles:nil];
 [alert show];
 }
 
 
 
 NSString *dataDescriptor = @"FID=COMMON.APPLE.INAPP.PAYMENT";
 
 NSDecimalNumber* amount = [NSDecimalNumber decimalNumberWithString:[self amount]];
 
 NSString *apiLogInID = @"66njfPa9X7";
 
 CreateTransactionRequest * transactionRequestObj = [self createTransactionReqObjectWithApiLoginID:apiLogInID
 fingerPrintHashData:fingerprintHashValue
 sequenceNumber:fingerprintInvoice.intValue
 transactionType:AUTH_CAPTURE
 opaqueDataValue:paymentEncryptedData
 dataDescriptor:dataDescriptor
 invoiceNumber:fingerprintInvoice
 totalAmount:amount
 fpTimeStamp:fingerprintTimestamp];
 
 //    NSDecimalNumber* invoiceNumber = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d", arc4random() % 10000]];
 //    NSTimeInterval _fingerprintTimestamp = [[NSDate date] timeIntervalSince1970];
 //
 //    NSString *apiLogInID                     = @"29XNx9w36";      // replace with YOUR_APILOGIN_ID
 //    NSString *transactionSecretKey           = @"9fF3AVtw6jG8m27R"; // replace with YOUR_TRANSACTION_SECRET_KEY
 //
 ////    NSString *apiLogInID = @"66njfPa9X7";
 ////    NSString *transactionSecretKey = @"74gyd4t5P9E9xDW5";
 //
 //
 //    NSString *_fingerprintHashValue = [self prepareFPHashValueWithApiLoginID:apiLogInID
 //                                                       transactionSecretKey:transactionSecretKey
 //                                                             sequenceNumber:invoiceNumber.longValue
 //                                                                totalAmount:amount
 //                                                                  timeStamp:_fingerprintTimestamp];
 //
 //    CreateTransactionRequest * transactionRequestObj = [self createTransactionReqObjectWithApiLoginID:apiLogInID
 //                                                                                  fingerPrintHashData:_fingerprintHashValue
 //                                                                                       sequenceNumber:invoiceNumber.intValue
 //                                                                                      transactionType:AUTH_CAPTURE
 //                                                                                      opaqueDataValue:[self getData2]//paymentEncryptedData
 //                                                                                       dataDescriptor:dataDescriptor
 //                                                                                        invoiceNumber:invoiceNumber.stringValue
 //                                                                                          totalAmount:amount
 //                                                                                          fpTimeStamp:_fingerprintTimestamp];
 
 if (transactionRequestObj != nil)
 {
 authNet = [AuthNet authNetWithEnvironment:ENV_TEST];
 
 [authNet setDelegate:self];
 
 authNet.environment = ENV_TEST;
 
 // Submit the transaction for AUTH_CAPTURE.
 [authNet purchaseWithRequest:transactionRequestObj];
 
 // Submit the transaction for AUTH_ONLY.
 //[authNet authorizeWithRequest:transactionRequestObj];
 }
 }
 
 
 #pragma mark -
 
 -(NSString* )getData2
 {
 return @"eyJkYXRhIjoiVXIxZ2NESnczZDlEc0lkcDBuNlwvUmVSMENTQ3A0KzZNTUkxUlhwOVBjZkgzZDZVTDJtUnRJMGJCS09MMmNrVkluNXhPQkIwZmxUQkdEUmFGZGp2OTRPUklRVHdsM0VaWEI4cWxCZFl6UGQ1WFhNTU81MGVSR1NQTlwvUEdqeDA2WVRmMlhUMnI4ZzZZamZlSDdob3ZsNmR4Nk9JV1llM0NDSGhYSlFzSkpZWUpUb2RZZlp0b1BRKzd3VFBiR05Bbll0NElESk4xYkJDMXJSQnRrTHBvUE9ZUk1mWXRqSHVkYWVwOG9SbzZaWFRRVVBmdkVXdVY2S01EVzlKTzhsbjlUTE1Nak9PYVwvVXRQZ3JTeTA1WU42RE4xT1wvS2JyeisrY01CeHRlSjBuODg3U2JLZ3cyVUczSDI1UG96bkpocUNOMjlPNTVFZ2FoRlZ1NmszVHQ4aXNTK3dMT3pOa21xeFlrUytUWlEwT3RWWkU4XC84bWRUa3NLVTIwWXRBXC9yZ3p4cE94N2g1MkhTOXBiZ0ducXBSYnB0SWJBRUpITk4yZHRXQ25TUk9Zc0pjSW9YXC9zPSIsInZlcnNpb24iOiJFQ192MSIsImhlYWRlciI6eyJhcHBsaWNhdGlvbkRhdGEiOiI5NGVlMDU5MzM1ZTU4N2U1MDFjYzRiZjkwNjEzZTA4MTRmMDBhN2IwOGJjN2M2NDhmZDg2NWEyYWY2YTIyY2MyIiwidHJhbnNhY3Rpb25JZCI6ImMxY2FmNWFlNzJmMDAzOWE4MmJhZDkyYjgyODM2MzczNGY4NWJmMmY5Y2FkZjE5M2QxYmFkOWRkY2I2MGE3OTUiLCJlcGhlbWVyYWxQdWJsaWNLZXkiOiJNSUlCU3pDQ0FRTUdCeXFHU000OUFnRXdnZmNDQVFFd0xBWUhLb1pJemowQkFRSWhBUFwvXC9cL1wvOEFBQUFCQUFBQUFBQUFBQUFBQUFBQVwvXC9cL1wvXC9cL1wvXC9cL1wvXC9cL1wvXC9cL1wvTUZzRUlQXC9cL1wvXC84QUFBQUJBQUFBQUFBQUFBQUFBQUFBXC9cL1wvXC9cL1wvXC9cL1wvXC9cL1wvXC9cL1wvOEJDQmF4alhZcWpxVDU3UHJ2VlYybUlhOFpSMEdzTXhUc1BZN3pqdytKOUpnU3dNVkFNU2ROZ2lHNXdTVGFtWjQ0Uk9kSnJlQm4zNlFCRUVFYXhmUjh1RXNRa2Y0dk9ibFk2UkE4bmNEZllFdDZ6T2c5S0U1UmRpWXdwWlA0MExpXC9ocFwvbTQ3bjYwcDhENTRXSzg0elYyc3hYczdMdGtCb043OVI5UUloQVBcL1wvXC9cLzhBQUFBQVwvXC9cL1wvXC9cL1wvXC9cL1wvKzg1dnF0cHhlZWhQTzV5c0w4WXlWUkFnRUJBMElBQk1jWk5BSDVtY2ZDVXdRT3JycDM1dkpJazBJMU92RXQrUkJaVFlmdFhiSXZjYjdYQzJnK3pMMkFzYWpcL3o2TWxJa0p0OVRPTVl2VVU4ZGJneE01S3gxbz0iLCJwdWJsaWNLZXlIYXNoIjoidDZtRHdrc3dqSEU3YnV0TWFCTmpZMGVzTDVtbGV4aFB6dHVcL3ppQWJCMWc9In0sInNpZ25hdHVyZSI6Ik1JSURRZ1lKS29aSWh2Y05BUWNDb0lJRE16Q0NBeThDQVFFeEN6QUpCZ1VyRGdNQ0dnVUFNQXNHQ1NxR1NJYjNEUUVIQWFDQ0Fpc3dnZ0luTUlJQmxLQURBZ0VDQWhCY2wrUGYzK1U0cGsxM25WRDlud1FRTUFrR0JTc09Bd0lkQlFBd0p6RWxNQ01HQTFVRUF4NGNBR01BYUFCdEFHRUFhUUJBQUhZQWFRQnpBR0VBTGdCakFHOEFiVEFlRncweE5EQXhNREV3TmpBd01EQmFGdzB5TkRBeE1ERXdOakF3TURCYU1DY3hKVEFqQmdOVkJBTWVIQUJqQUdnQWJRQmhBR2tBUUFCMkFHa0Fjd0JoQUM0QVl3QnZBRzB3Z1o4d0RRWUpLb1pJaHZjTkFRRUJCUUFEZ1kwQU1JR0pBb0dCQU5DOCtrZ3RnbXZXRjFPempnRE5yalRFQlJ1b1wvNU1LdmxNMTQ2cEFmN0d4NDFibEU5dzRmSVhKQUQ3RmZPN1FLaklYWU50MzlyTHl5N3hEd2JcLzVJa1pNNjBUWjJpSTFwajU1VWM4ZmQ0ZnpPcGszZnRaYVFHWE5MWXB0RzFkOVY3SVM4Mk91cDlNTW8xQlBWclhUUEhOY3NNOTlFUFVuUHFkYmVHYzg3bTByQWdNQkFBR2pYREJhTUZnR0ExVWRBUVJSTUUrQUVIWldQcld0SmQ3WVo0MzFoQ2c3WUZTaEtUQW5NU1V3SXdZRFZRUURIaHdBWXdCb0FHMEFZUUJwQUVBQWRnQnBBSE1BWVFBdUFHTUFid0J0Z2hCY2wrUGYzK1U0cGsxM25WRDlud1FRTUFrR0JTc09Bd0lkQlFBRGdZRUFiVUtZQ2t1SUtTOVFRMm1GY01ZUkVJbTJsK1hnOFwvSlh2K0dCVlFKa09Lb3NjWTRpTkRGQVwvYlFsb2dmOUxMVTg0VEh3TlJuc3ZWM1BydjdSVFk4MWdxMGR0Qzh6WWNBYUFrQ0hJSTN5cU1uSjRBT3U2RU9XOWtKazIzMmdTRTdXbEN0SGJmTFNLZnVTZ1FYOEtYUVl1WkxrMlJyNjNOOEFwWHNYd0JMM2NKMHhnZUF3Z2QwQ0FRRXdPekFuTVNVd0l3WURWUVFESGh3QVl3Qm9BRzBBWVFCcEFFQUFkZ0JwQUhNQVlRQXVBR01BYndCdEFoQmNsK1BmMytVNHBrMTNuVkQ5bndRUU1Ba0dCU3NPQXdJYUJRQXdEUVlKS29aSWh2Y05BUUVCQlFBRWdZQ29lMm5iTVhoRG9lUlVGTkJJd1lhUlwvVzYyXC9PMitrem1iSVM2VmVOTzRWWStXQlZYN0ZWMEVqMGtZYWM3QUViSFpKUzhMc2VCeTZjTFFMYzNESkM4WElpTXo1a3NhUEtraFBBbElFbWVZU3plWXdMOVNNOGptQW9KQ0VVakVHU1N2anVPSkh6WlJ3TjZxRHAyS0RPUVY0N3RlUFJyT3NzN3hxMld5dnJxeXlRPT0ifQ==";
 }
 
 
 -(NSString*) prepareFPHashValueWithApiLoginID:(NSString*) apiLoginID
 transactionSecretKey:(NSString*) transactionSecretKey
 sequenceNumber:(NSInteger) sequenceNumber
 totalAmount:(NSDecimalNumber*) totalAmount
 timeStamp:(NSTimeInterval) timeStamp
 {
 NSString *fpHashValue = nil;
 
 fpHashValue =[NSString stringWithFormat:@"%@^%ld^%lld^%@^", apiLoginID, (long)sequenceNumber, (long long)timeStamp, totalAmount];
 
 NSLog(@"Finger Print Before HMAC_MD5: %@", fpHashValue);
 
 return [NSString HMAC_MD5_WithTransactionKey:transactionSecretKey fromValue:fpHashValue];
 }*/
