//
//  JMСompletedLetter.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPackage.h"

#import "JMConst.h"
#import "JMCoreDataManager.h"
#import "PDFHelper.h"

@interface JMPackage () {
    NSInteger _letterPages;
}

@end

@implementation JMPackage

- (instancetype)initWithLetter:(JMLetter *)aLetter
{
    self = [super init];

    if (self)
    {
        self.letter = aLetter;
    }

    return self;
}


- (NSInteger)letterPages
{
    if (_letterPages == 0)
    {
        PDFHelper *_pdfHelper = [[PDFHelper alloc] init];
        _letterPages = [_pdfHelper createLetterFromLetter:self withFileName:kDefLetterName];
    }

    return _letterPages;
}


- (NSInteger)letterPhotos
{
    if (_letter.type == JMLetterTypePostcard)
    {
        return 0;
    }

    return _letter.attachments.count;
}


- (NSInteger)letterSubmittedPhotos
{
    if (_letter.type == JMLetterTypePostcard)
    {
        return 0;
    }

    return _letter.submittedAttachments.count;
}


- (NSString *)discountID
{
    if (_discountID == nil)
    {
        _discountID = @"";
    }

    return _discountID;
}


@end