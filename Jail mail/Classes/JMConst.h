//
//  JMConst.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//


#define HOST_URL @"http://jailmail.notforgotten.biz:8090"
//@"http://107.21.221.86:9090"
//

#define PRICE_MESSAGE 0.99

#define PRICE_PHOTO 0.49

#define PRICE_POSTCARD 1.30

#define MIN_PHOTO_COUNT 3