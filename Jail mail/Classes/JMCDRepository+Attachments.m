//
//  JMCDRepository+Attachments.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 03/07/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+Attachments.h"

@implementation JMCDRepository (Attachments)

- (NSArray *)attachmentsForStatus:(JMAttachmentsState)status;
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nState == %d", JMAttachmentsStateSubmitted];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMAttachments entityName]];
    [request setPredicate:predicate];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}

@end
