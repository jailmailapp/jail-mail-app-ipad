//
//  JMGoogleSignIn.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/Nov/2015.
//  Copyright © 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMGoogleSignIn.h"

static NSString *const kClientId = @"933197727402-mfevv5udk73v2gtf0saar1j4bia0gfq0.apps.googleusercontent.com";

@interface JMGoogleSignIn ()<GIDSignInDelegate, GIDSignInUIDelegate>
{
    GoogleLogInCompletionBlock _logInCompletionBlock;
    UIViewController *_presentViewController;
}

@end

@implementation JMGoogleSignIn

+ (instancetype)sharedInstance
{
    static JMGoogleSignIn *sharedInstance = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] init];
    });

    return sharedInstance;
}


- (instancetype)init
{
    self = [super init];

    if (self != nil)
    {
        _logInCompletionBlock = nil;
    }

    return self;
}


- (void)logInPresentViewController:(UIViewController *)viewController completionBlock:(GoogleLogInCompletionBlock)logInCompletionBlock
{
    _logInCompletionBlock  = logInCompletionBlock;
    _presentViewController = viewController;

    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.clientID                = kClientId;
    signIn.scopes                  = @[@"profile", @"email"];
    signIn.delegate                = self;
    signIn.uiDelegate              = self;

    [signIn signIn];
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    NSLog(@"Signed in user");

    _logInCompletionBlock(user, error);
}


- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    _logInCompletionBlock(user, error);
}


// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    // [myActivityIndicator stopAnimating];
}


// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [_presentViewController presentViewController:viewController animated:YES completion:nil];
}


// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [_presentViewController dismissViewControllerAnimated:YES completion:nil];
}


@end