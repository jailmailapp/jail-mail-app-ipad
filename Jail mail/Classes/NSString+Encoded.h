//
//  NSString+Encoded.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 28/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encoded)

+ (NSMutableString *)convertSpecialCharacterIntoHTMLEncoded:(NSString *)data1 andURL:(BOOL)isURL;

@end
