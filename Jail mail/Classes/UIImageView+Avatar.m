//
//  UIImageView+Avatar.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 02/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "UIImageView+Avatar.h"

@implementation UIImageView (Avatar)

- (void)circle
{
    self.layer.cornerRadius  = self.frame.size.height/2;
    self.layer.masksToBounds = YES;
}


- (void)blueBorder
{
    self.layer.borderWidth = 2;
    self.layer.borderColor = [UIColor colorWithRed:0.243 green:0.467 blue:0.839 alpha:1.000].CGColor;
}

- (void)whiteBorder
{
    self.layer.borderWidth = 2;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)setAvatarImage:(UIImage *)avatarImage
{
    if (avatarImage != nil) {
        self.image = avatarImage;
    } else {
        self.image = [UIImage imageNamed:@"person_big"];
    }
}

@end