//
//  JMLoyalty.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMRewardsController.h"

#import "JMFinancesCodeRequest.h"
#import "UIViewController+SideMenu.h"

#define DEFAULT_CONT_AMOUNT 10

@interface JMRewardsController ()<UICollectionViewDataSource, UICollectionViewDelegate> {
    NSInteger _amountMessagesToDiscounts;
    NSInteger _amountPhotosToDiscounts;
    
    NSInteger _lettersTo;
    NSInteger _lettersCount;
    NSInteger _photosTo;
    NSInteger _photosCount;
}

@property (strong, nonatomic) IBOutlet UICollectionView *photoCollection;
@property (strong, nonatomic) IBOutlet UICollectionView *messageCollection;
@property (strong, nonatomic) IBOutlet UILabel *quotationLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UILabel *photoLabel;
@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) IBOutlet UIImageView *messageView;

@end

@implementation JMRewardsController

+ (NSString *)storyboardID
{
    return @"idLoyaltyController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Rewards", @"Rewards");
    
    _lettersTo = DEFAULT_CONT_AMOUNT;
    _lettersCount = DEFAULT_CONT_AMOUNT;
    _photosTo = DEFAULT_CONT_AMOUNT;
    _photosCount = DEFAULT_CONT_AMOUNT;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.navigationController isBeingPresented])
    {
        UIBarButtonItem *flexibleButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_close"] style:UIBarButtonItemStyleBordered target:self action:@selector(dismiss:)];
        self.navigationItem.leftBarButtonItem = flexibleButton;
    }
    else {
        [self addMenuBarButton];
    }
    
    [self reloadRewards];
    
    [self updateUI];
}


- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)reloadRewards
{
    [JMFinancesCodeRequest rewardsWithDeviceID:[NSString stringWithFormat:@"%@", [UIDevice currentDevice].identifierForVendor.UUIDString]
                                       success:^(NSDictionary *response) {
                                           
                                           _lettersTo    = [[response valueForKey:@"lettersTo"] integerValue];
                                           _lettersCount = [[response valueForKey:@"lettersCount"] integerValue];
                                           _photosTo     = [[response valueForKey:@"photosTo"] integerValue];
                                           _photosCount  = [[response valueForKey:@"photosCount"] integerValue];
                                           
                                           [self updateUI];
                                           
                                           NSLog (@"%ld %ld %ld %ld", (long)_lettersTo, (long)_lettersCount, (long)_photosTo, (long)_photosCount);
                                           
                                       } failure:^(NSError *error) {
                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                                                           message:NSLocalizedString(@"Error update rewards", @"Error update rewards")
                                                                                          delegate:nil
                                                                                 cancelButtonTitle:nil
                                                                                 otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
                                           [alert show];
                                       }];
}

- (void)updateUI
{
    self.quotationLabel.text = NSLocalizedString(@"You give loyalty, you'll get it back. You give love, you'll get it back.",
                                                 @"You give loyalty, you'll get it back. You give love, you'll get it back.");
    self.messageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Submit %ld more letters to get one for free!",
                                                                          @"Submit %ld more letters to get one for free!"), (long)_lettersTo];
    self.photoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Submit %ld more photos to get one for free!",
                                                                        @"Submit %ld more photos to get one for free!"), (long)_photosTo];
    
    if (_lettersTo == 0)
    {
        self.messageView.image = [UIImage imageNamed:@"letter_blue"];
    }
    
    if (_photosTo == 0)
    {
        self.photoView.image = [UIImage imageNamed:@"photo_blue"];
    }
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self.messageLabel.text];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.243 green:0.471 blue:0.835 alpha:1.000] range:NSMakeRange(7, 2)];
    self.messageLabel.attributedText = attString;
    
    attString = [[NSMutableAttributedString alloc] initWithString:self.photoLabel.text];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.243 green:0.471 blue:0.835 alpha:1.000] range:NSMakeRange(7, 2)];
    self.photoLabel.attributedText = attString;
    
    [self.photoCollection reloadData];
    [self.messageCollection reloadData];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.photoCollection) {
        return _photosCount;
    }
    
    if (collectionView == self.messageCollection) {
        return _lettersCount;
    }
    
    return DEFAULT_CONT_AMOUNT;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kCellIdentifier = @"CellIdentifier";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:999];
    
    if (collectionView == self.messageCollection)
    {
        if (indexPath.row < _lettersCount-_lettersTo)
        {
            imageView.image = [UIImage imageNamed:@"letter_blue"];
        }
        else {
            imageView.image = [UIImage imageNamed:@"letter_gray"];
        }
    }
    else {
        if (indexPath.row < _photosCount-_photosTo)
        {
            imageView.image = [UIImage imageNamed:@"photo_blue"];
        }
        else {
            imageView.image = [UIImage imageNamed:@"photo_gray"];
        }
    }
    
    return cell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
}


@end

@interface TopAlignedCollectionViewFlowLayout : UICollectionViewFlowLayout

@end

@implementation TopAlignedCollectionViewFlowLayout

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    
    UICollectionViewLayoutAttributes *att = [array lastObject];
    
    if (att)
    {
        CGFloat lastY = att.frame.origin.y + att.frame.size.height;
        CGFloat diff  = self.collectionView.frame.size.height - lastY;
        
        if (diff > 0)
        {
            UIEdgeInsets contentInsets = UIEdgeInsetsMake(diff/2, 0.0, 0.0, 0.0);
            self.collectionView.contentInset = contentInsets;
        }
    }
    
    return array;
}


@end