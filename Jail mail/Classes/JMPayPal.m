//
//  JMPayPal.m
//  Jail mail
//
//  Created by Oleksiy Kovtun on 4/2/15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPayPal.h"
#import <PayPalMobile.h>

typedef void (^EmptyBlock) ();

@interface JMPayPal () <PayPalPaymentDelegate> {
    PayPalPaymentSucceededBlock _payPalPaymentSucceededBlock;
    PayPalPaymentCancelledBlock _payPalPaymentCancelledBlock;
}

@end

@implementation JMPayPal

+ (instancetype)sharedInstance {
    static JMPayPal *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    
    if (self != nil) {
        _payPalPaymentSucceededBlock = nil;
        _payPalPaymentCancelledBlock = nil;
    }
    
    return self;
}

#pragma mark -

- (void)payForTheLetter:(JMPackage *)packege
                success:(PayPalPaymentSucceededBlock)successBlock
                 cancel:(PayPalPaymentCancelledBlock)cancelBlock
                  error:(PayPalPaymentErrorBlock)errorBlock
inPaymentViewControllerPresentedIn:(UIViewController *)viewController {
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", [packege priceTotal]]];
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Sending letter.";
    payment.items = nil;
    payment.paymentDetails = nil;
    
    if (!payment.processable) {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Payment isn't processable."};
        NSError *error = [NSError errorWithDomain:@"JMPayPal (pay for the letter)"
                                             code:0
                                         userInfo:userInfo];
        
        errorBlock(error);
        
        return;
    }
    
    _payPalPaymentSucceededBlock = successBlock;
    _payPalPaymentCancelledBlock = cancelBlock;
    
    NSString *сlientID = @"AUw3lNbmkMijqmHc_5CTlqQIRWYGSXHbvZqucP0_HnhKbwXpAzhY4JIiv2CT5FEqMVMe9TIc7SM9xPOm";
    NSDictionary *environments = @{PayPalEnvironmentProduction: сlientID};
    
    [PayPalMobile initializeWithClientIdsForEnvironments:environments];
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
    
    PayPalConfiguration *payPalConfiguration = [[PayPalConfiguration alloc] init];
    payPalConfiguration.acceptCreditCards = YES;
    payPalConfiguration.merchantName = @"Jail Mail";
    payPalConfiguration.languageOrLocale = [NSLocale preferredLanguages][0];
    payPalConfiguration.acceptCreditCards = YES;
    
    PayPalPaymentViewController *paymentVC;
    
    paymentVC= [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                      configuration:payPalConfiguration
                                                           delegate:self];
    
    [viewController presentViewController:paymentVC animated:YES completion:nil];
}

#pragma mark - <PayPalPaymentDelegate>

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
    
    if (_payPalPaymentCancelledBlock != nil) {
        _payPalPaymentCancelledBlock();
    }
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
    
    if (_payPalPaymentSucceededBlock != nil) {
        _payPalPaymentSucceededBlock(completedPayment);
    }
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                willCompletePayment:(PayPalPayment *)completedPayment
                    completionBlock:(PayPalPaymentDelegateCompletionBlock)completionBlock {
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
    
    if (_payPalPaymentSucceededBlock != nil) {
        _payPalPaymentSucceededBlock(completedPayment);
    }
}

@end
