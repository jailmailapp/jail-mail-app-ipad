//
//  EditableCollectionCell.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "EditableCollectionCell.h"

@implementation EditableCollectionCell

- (UIButton *)buttonEdit
{
    if (_buttonEdit == nil)
    {
        self.buttonEdit = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-15, 0, 20, 20)];
        [self.buttonEdit addTarget:self action:@selector(deleteTap)forControlEvents:UIControlEventTouchUpInside];
        [self.buttonEdit setImage:[UIImage imageNamed:@"icon_delete"] forState:UIControlStateNormal];
        [self addSubview:self.buttonEdit];
    }
    
    return _buttonEdit;
}


- (void)setStyle:(UITableViewCellEditingStyle)editingStyle animated:(BOOL)animated
{
    if (editingStyle == UITableViewCellEditingStyleNone)
    {
        [self.buttonEdit setHidden:YES];
        
        if (animated)
        {
            [self stopAnimation];
        }
    }
    else {
        [self.buttonEdit setHidden:NO];
        
        if (animated)
        {
            [self startAnimation];
        }
    }
}


- (void)deleteTap
{
    if ([_delegate respondsToSelector:@selector(deleteCellAttachments:)])
    {
        [_delegate deleteCellAttachments:self.attachments];
    }
}


- (void)startAnimation
{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    float startAngle       = (-1) * M_PI/180.0;
    float stopAngle        = -startAngle;
    [anim setFromValue:[NSNumber numberWithFloat:startAngle]];
    [anim setToValue:[NSNumber numberWithFloat:3 * stopAngle]];
    [anim setAutoreverses:YES];
    [anim setDuration:0.15];
    [anim setRepeatCount:NSUIntegerMax];
    float timeOffset = (float)(arc4random() % 100) / 100 - 0.50;
    [anim setTimeOffset:timeOffset];
    [self.layer addAnimation:anim forKey:@"iconShake"];
}


- (void)stopAnimation
{
    [self.layer removeAllAnimations];
}


@end