//
//  JMRecipientEditController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEditProfileController.h"

#import "UIViewController+CustomInit.h"
#import "JMRecipientsDelegate.h"

@interface JMRecipientEditController : JMEditProfileController <StoryboardProtocol>

@property (nonatomic, weak) id <JMRecipientsDelegate> delegate;

- (void)setRecipient:(id)recipient;

@end
