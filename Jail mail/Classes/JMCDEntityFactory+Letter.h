//
//  JMCDEntityFactory+Letter.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 17/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDEntityFactory.h"

#import "JMLetter.h"

@interface JMCDEntityFactory (Letter)

- (JMLetter *)newLetter;

@end
