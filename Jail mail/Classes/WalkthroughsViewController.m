//
//  TSWalkthroughsViewController.m
//  Tibetan Singing Bowls
//
//  Created by Alexander Shvetsov on 24/Sep/2015.
//  Copyright © 2015 Yanpix. All rights reserved.
//

#import "WalkthroughsViewController.h"
#import "PageContentViewController.h"

@interface WalkthroughsViewController ()

@end

@implementation WalkthroughsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pageTitles = @[@"Brand new design!", @"Click here to get started", @"Sending letters and photos is easier than ever", @"Now you can send postcards", @"Create profiles to add a presonal touch"];
    self.pageImages = @[@"1", @"2", @"3", @"4", @"5"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"idPageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0,
                                                    20,
                                                    self.view.frame.size.width,
                                                    self.view.frame.size.height-CGRectGetHeight(self.leftButton.frame));
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.view sendSubviewToBack:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    self.pageControl.numberOfPages = self.pageImages.count;
    self.pageControl.currentPage = 0;
}

- (NSInteger)pageCount
{
    return [self.pageTitles count]-1;
}

- (IBAction)leftTapped:(id)sender {
    
    NSUInteger pageIndex = ((PageContentViewController *) [_pageViewController.viewControllers objectAtIndex:0]).pageIndex;
    
    if (self.pageCount == pageIndex)
    {
        [self changePage:UIPageViewControllerNavigationDirectionReverse];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)rightTapped:(id)sender {
    NSUInteger pageIndex = ((PageContentViewController *) [_pageViewController.viewControllers objectAtIndex:0]).pageIndex;
    
    if (self.pageCount != pageIndex)
    {
        [self changePage:UIPageViewControllerNavigationDirectionForward];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)changePage:(UIPageViewControllerNavigationDirection)direction {
    NSUInteger pageIndex = ((PageContentViewController *) [_pageViewController.viewControllers objectAtIndex:0]).pageIndex;
    
    if (direction == UIPageViewControllerNavigationDirectionForward) {
        pageIndex++;
    }
    else {
        pageIndex--;
    }

    PageContentViewController *viewController = [self viewControllerAtIndex:pageIndex];
    
    if (viewController == nil) {
        return;
    }
    
    [self updateUIWithPage:pageIndex];
    
    [_pageViewController setViewControllers:@[viewController]
                                  direction:direction
                                   animated:YES
                                 completion:nil];
    
}

- (void)updateUIWithPage:(NSUInteger)index
{
    self.pageControl.currentPage = index;
    
    if (self.pageCount != index) {
        [self.leftButton setTitle:@"Skip" forState:UIControlStateNormal];
        [self.rightButton  setTitle:@"Next" forState:UIControlStateNormal];
    } else {
        [self.leftButton setTitle:@"Back" forState:UIControlStateNormal];
        [self.rightButton  setTitle:@"Get Started!" forState:UIControlStateNormal];
    }
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"idPageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    [self updateUIWithPage:index];
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    
    
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    [self updateUIWithPage:index];
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

@end
