//
//  JMPayPal.h
//  Jail mail
//
//  Created by Oleksiy Kovtun on 4/2/15.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPackage.h"
#import <Foundation/Foundation.h>
#import <PayPalPayment.h>
#import <UIKit/UIKit.h>

typedef void (^PayPalPaymentSucceededBlock)(PayPalPayment *completedPayment);
typedef void (^PayPalPaymentCancelledBlock)();
typedef void (^PayPalPaymentErrorBlock)(NSError *error);

/**
 * This class is used for creating `PaymentViewController` object which is used for making payments in PayPal
 */
@interface JMPayPal : NSObject

+ (instancetype)sharedInstance;

/**
 * Creates PayPal configuration and payment, then creates PaymentViewController and presents it from `viewController`
 * \param successBlock The block which will be executed on the completion of the payment
 * \param cancelBlock The block which will be executed when user cancels the payment
 * \param errorBlock The block which will be executed on the when the payment isn't processable
 * \param viewController The view controller which used for presenting PaymentViewController
 */
- (void)payForTheLetter:(JMPackage *)packege
                success:(PayPalPaymentSucceededBlock)successBlock
                 cancel:(PayPalPaymentCancelledBlock)cancelBlock
                  error:(PayPalPaymentErrorBlock)errorBlock
inPaymentViewControllerPresentedIn:(UIViewController *)viewController;

@end