//
//  JMLetterRequest.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMLetterRequest.h"

#import "AFNetworking.h"
#import "JMAttachments.h"
#import "JMCard.h"
#import "JMConst.h"
#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMPackage.h"
#import "JMState.h"
#import "JMUser.h"
#import "NSError+Request.h"
#import "NSString+Encoded.h"
#import "PDFHelper.h"

@implementation JMLetterRequest

#pragma mark -

+ (NSString *)buildXMLFromPackage:(JMPackage *)_package
{
    if ((_package.letterPages == 0) && (_package.letterPhotos == 0))
    {
        return @"";
    }
    
    NSMutableString *xmlString = [NSMutableString string];
    
    [xmlString appendFormat:@"<RequestCarrier>"];
    [xmlString appendFormat:@"<userId>%@</userId>", @"-1"];
    [xmlString appendFormat:@"<messageId>-1</messageId>"];
    [xmlString appendFormat:@"<transactionId>%@</transactionId>", @"-1"];
    [xmlString appendFormat:@"<command>INMATE-1-1-1</command>"];
    [xmlString appendFormat:@"<userEmail>%@</userEmail>", _package.letter.sender.email];
    
    [xmlString appendFormat:@"<modelListWarapper>"];
    [xmlString appendFormat:@"<list>"];
    [xmlString appendFormat:@"<com.inmate.model.Message>"];
    [xmlString appendFormat:@"<messageId>-1</messageId>"];
    [xmlString appendFormat:@"<userId>%@</userId>", @"-1"];
    [xmlString appendFormat:@"<photoCount>%lu</photoCount>", (long)_package.letterPhotos];
    [xmlString appendFormat:@"<transactionId>%@</transactionId>", @"-1"];
    [xmlString appendFormat:@"<letterPageCount>%lu</letterPageCount>", (long)_package.letterPages];
    [xmlString appendFormat:@"<moneyOrderAmount>%.2f</moneyOrderAmount>", [_package priceTotal]];
    
    if (_package.letter.type == JMLetterTypePostcard) {
        [xmlString appendFormat:@"<postcard>true</postcard>"];
    }
    
    if (_package.letter.message)
    {
        [xmlString appendFormat:@"<dear>%@</dear>", @"Dear"];
        [xmlString appendFormat:@"<sign>%@</sign>", @"Bob"];
        [xmlString appendFormat:@"<font>%@</font>", @"Helvetica"];
        [xmlString appendFormat:@"<messageSubject>%@</messageSubject>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.fullName andURL:NO]];
        [xmlString appendFormat:@"<letterName>%@</letterName>", [NSString convertSpecialCharacterIntoHTMLEncoded:@"Letter" andURL:NO]];
        [xmlString appendFormat:@"<salutation>%@</salutation>", [NSString convertSpecialCharacterIntoHTMLEncoded:@"Sincerly" andURL:NO]];
    }
    
    [xmlString appendFormat:@"<inmateFirstName>%@</inmateFirstName>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.firstName andURL:NO]];
    [xmlString appendFormat:@"<inmateLastName>%@</inmateLastName>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.lastName andURL:NO]];
    [xmlString appendFormat:@"<inmateId>%@</inmateId>", _package.letter.recipient.identifier];
    [xmlString appendFormat:@"<inmateBedAssignment>%@</inmateBedAssignment>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.bed andURL:NO]];
    [xmlString appendFormat:@"<inmateInsName>%@</inmateInsName>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.institution.name andURL:NO]];
    [xmlString appendFormat:@"<inmateInsAddress>%@</inmateInsAddress>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.institution.address andURL:NO]];
    [xmlString appendFormat:@"<inmateState>%@</inmateState>", _package.letter.recipient.institution.state.name];
    [xmlString appendFormat:@"<inmateZip>%@</inmateZip>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.recipient.institution.zipCode andURL:NO]];
    [xmlString appendFormat:@"<senderFirstName>%@</senderFirstName>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.sender.firstName andURL:NO]];
    [xmlString appendFormat:@"<senderLastName>%@</senderLastName>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.sender.lastName andURL:NO]];
    [xmlString appendFormat:@"<senderInsAddress>%@</senderInsAddress>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.sender.street andURL:NO]];
    [xmlString appendFormat:@"<senderState>%@</senderState>", _package.letter.sender.state.name];
    [xmlString appendFormat:@"<senderZip>%@</senderZip>", [NSString convertSpecialCharacterIntoHTMLEncoded:_package.letter.sender.zipCode andURL:NO]];
    
    [xmlString appendFormat:@"</com.inmate.model.Message>"];
    [xmlString appendFormat:@"</list>"];
    [xmlString appendFormat:@"</modelListWarapper>"];
    
    [xmlString appendFormat:@"</RequestCarrier>"];
    
    NSLog(@"Build XML:%@", xmlString);
    
    return xmlString;
}


+ (void)sendPackage:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    if (package == nil)
    {
        if (failure)
        {
            dispatch_async (dispatch_get_main_queue (), ^{
                failure ([NSError errorWithDescription:NSLocalizedString (@"Letter empty.", @"Letter empty.")]);
            });
        }
        
        return;
    }
    
    PDFHelper *pdfHelper = [[PDFHelper alloc] init];
    [pdfHelper createContactFromLetter:package withFileName:kDefContactName];
    
    NSString *xml = [self buildXMLFromPackage:package];
    
    if (xml.length == 0)
    {
        if (failure)
        {
            dispatch_async (dispatch_get_main_queue (), ^{
                failure ([NSError errorWithDescription:NSLocalizedString (@"Problem preparation letter.", @"Problem preparation letter.")]);
            });
        }
        
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/inmate/uploadInMateItem.html", HOST_URL]]];
    [request setHTTPBody:[xml dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Request (../inmate/uploadInMateItem.html) Success: %@ %@", responseObject,  [operation responseString]);
                                                                  [self requestPackage:package finishOperation:operation success:success failure:failure];
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Request (../inmate/uploadInMateItem.html) Error: %@", error);
                                                                  [self requestPackage:package finishOperation:operation success:success failure:failure];
                                                              }];
    
    [manager.operationQueue addOperation:operation];
}

+ (void)requestPackage:(JMPackage *)package finishOperation:(AFHTTPRequestOperation *)operation success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    NSLog (@"Request Send Letter Finish:%@", operation);
    
    NSString *responseString = [operation responseString];
    
    if (responseString == nil)
    {
        if (failure)
        {
            dispatch_async (dispatch_get_main_queue (), ^{
                failure ([NSError errorWithDescription:NSLocalizedString (@"Problem sending letter.", @"Problem sending letter.")]);
            });
        }
        
        return;
    }
    
    NSArray *token = [responseString componentsSeparatedByString:@":"];
    
    if ([token count] != 2)
    {
        if (failure)
        {
            dispatch_async (dispatch_get_main_queue (), ^{
                failure ([NSError errorWithDescription:NSLocalizedString (@"Problem sending letter.", @"Problem sending letter.")]);
            });
        }
        
        return;
    }
    
    package.letter.userID   = [token objectAtIndex:0];
    package.letter.letterID = [token objectAtIndex:1];
    
    PDFHelper *_pdfHelper = [[PDFHelper alloc] init];
    NSData *fileData      = [NSData dataWithContentsOfFile:[_pdfHelper defaltPathWithFileName:kDefLetterName]];
    NSString *fileName    = [NSString stringWithFormat:@"%@_%@_Letter.pdf", package.letter.userID, package.letter.letterID];
    NSString *mimeType    = @"jpeg/pdf";
    
    [self uploadFileData:fileData package:package withFileName:fileName mimeType:mimeType success:^(id response) {
        NSData *fileData = [NSData dataWithContentsOfFile:[_pdfHelper defaltPathWithFileName:kDefContactName]];
        NSString *fileName = [NSString stringWithFormat:@"%@_%@_OrderInfo.pdf", package.letter.userID, package.letter.letterID];
        
        [self uploadFileData:fileData package:package withFileName:fileName mimeType:mimeType success:^(id response){
            if (success)
            {
                dispatch_async (dispatch_get_main_queue (), ^{
                    success (@"Done");
                });
            }
        } failure:^(NSError *error) {
            if (failure)
            {
                dispatch_async (dispatch_get_main_queue (), ^{
                    failure ([NSError errorWithDescription:NSLocalizedString (@"Problem sending letter.", @"Problem sending letter.")]);
                });
            }
        }];
    } failure:^(NSError *error) {
        if (failure)
        {
            dispatch_async (dispatch_get_main_queue (), ^{
                failure ([NSError errorWithDescription:NSLocalizedString (@"Problem sending letter.", @"Problem sending letter.")]);
            });
        }
    }];
}


+ (void)uploadFileData:(NSData *)fileData
               package:(JMPackage *)package
          withFileName:(NSString *)fileName
              mimeType:(NSString *)mimeType
               success:(void (^)(id response))success
               failure:(void (^)(NSError *error))failure
{
    NSString *strURl = [NSString stringWithFormat:@"%@/inmate/multiupload.html?email=%@&userId=%@&messageId=%@&transactionId=%@",
                        HOST_URL,
                        package.letter.sender.email,
                        package.letter.userID,
                        package.letter.letterID,
                        @"-1"];
    
    NSLog(@"%@", strURl);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:strURl parameters:nil constructingBodyWithBlock:^(id < AFMultipartFormData > formData) {
        [formData appendPartWithFileData:fileData
                                    name:@"-1"
                                fileName:fileName
                                mimeType:mimeType];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog (@"Success: %@ %@", responseObject, [operation responseString]);
        
        if (success)
        {
            success ([operation responseString]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog (@"Error: %@", error);
        
        if (failure)
        {
            failure ([NSError errorWithDescription:NSLocalizedString (@"Problem upload file.", @"Problem upload file.")]);
        }
    }];
}

#pragma mark -


+ (void)updateCompletedPackage:(JMPackage *)package overrideCompletedStatusToUploaded:(BOOL)isOverride success:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    int uploadStatus    = 1;
    int uploadFileCount = 0;
    
    //    possible uploadStatus values 1 and 2
    //    1 means uploaded successfully with all files
    //    2 means partial upload and missing few files
    
    for (JMAttachments *item in package.letter.attachments)
    {
        if (item.state == JMAttachmentsStateSubmitted)
        {
            uploadFileCount++;
        }
        else{
            uploadStatus = 2;
        }
    }
    
    if (isOverride) {
        uploadStatus = 1;
    }
    
    NSString *URLString = [NSString stringWithFormat:@"%@/inmate/updateMessageStatus.html?messageId=%@&uploadStatus=%d&uploadedFileCount=%d",
                           HOST_URL,
                           package.letter.letterID,
                           uploadStatus,
                           uploadFileCount];
    
    NSLog(@"%@", URLString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    [request setHTTPMethod:@"GET"];
    
    NSOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  NSLog (@"Request (../inmate/updateMessageStatus.html..) Success: %@", [operation responseString]);
                                                                  
                                                                  if (success)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          success ([operation responseString]);
                                                                      });
                                                                  }
                                                              }
                                                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  NSLog (@"Request  (../inmate/updateMessageStatus.html..) Error: %@", error);
                                                                  
                                                                  if (failure)
                                                                  {
                                                                      dispatch_async (dispatch_get_main_queue (), ^{
                                                                          failure ([NSError errorWithDescription:NSLocalizedString (@"Problem update completed status.", @"Problem update completed status.")]);
                                                                      });
                                                                  }
                                                              }];
    [manager.operationQueue addOperation:operation];
}


@end