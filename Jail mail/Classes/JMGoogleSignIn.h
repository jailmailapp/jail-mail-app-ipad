//
//  JMGoogleSignIn.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/Nov/2015.
//  Copyright © 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GoogleSignIn/GoogleSignIn.h>

typedef void (^GoogleLogInCompletionBlock) (GIDGoogleUser *user, NSError *error);

@interface JMGoogleSignIn : NSObject

+ (instancetype)sharedInstance;

- (void)logInPresentViewController:(UIViewController *)viewController completionBlock:(GoogleLogInCompletionBlock)logInCompletionBlock;

@end