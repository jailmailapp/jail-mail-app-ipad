//
//  JMRecipientEditController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMRecipientEditController.h"

#import "JMCDEntityFactory+Inmate.h"
#import "JMCDRepository.h"
#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMInstitutionController.h"
#import "JMTextFieldCell.h"
#import "UIImageView+Avatar.h"
#import "UIViewController+ImageBackButton.h"
#import "PSTAlertController.h"

static NSString *const kFirstName   = @"First Name";
static NSString *const kLastName    = @"Last Name";
static NSString *const kInmateID    = @"Inmate ID";
static NSString *const kBed         = @"Bed Assignment";
static NSString *const kInstitution = @"Institution";

@interface JMRecipientEditController ()<JMInstitutionControllerDelegate>{
}

@property (nonatomic, retain) JMInmate *inmate;
@property (strong, nonatomic) IBOutlet UIButton *institutionButton;
@property (strong, nonatomic) IBOutlet UILabel *institutionName;
@property (strong, nonatomic) IBOutlet UILabel *institutionNote;

@end

@implementation JMRecipientEditController

+ (NSString *)storyboardID
{
    return @"idRecipientEditController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"New Recipient", @"New Recipient");
    
    self.institutionButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.institutionButton.layer.borderWidth = 1;
    
    self.headers = [NSArray arrayWithObjects:
                    kFirstName,
                    kLastName,
                    kInmateID,
                    kBed, nil];
    
    [self setCustomBackButton];
    
    [self setInstitution:_inmate.institution];
    [self setAvatarImage:_inmate.avatar];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_inmate)
    {
        self.navigationItem.title = NSLocalizedString(@"Edit Recipient", @"Edit Recipient");
    }
}


- (void)setRecipient:(id)recipient
{
    if (recipient && [recipient isKindOfClass:JMInmate.class])
    {
        _inmate = recipient;
        
        [self setInstitution:_inmate.institution];
        [self setAvatarImage:_inmate.avatar];
    }
}


- (JMInmate *)inmate
{
    if (_inmate == nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        _inmate = [[JMCDEntityFactory newFactory] newInmate];
        [repository addEntity:_inmate];
    }
    
    return _inmate;
}


- (IBAction)changeInstitution:(id)sender
{
    JMInstitutionController *institutionController = [JMInstitutionController controller];
    institutionController.delegate = self;
    [self.navigationController pushViewController:institutionController animated:YES];
}


- (IBAction)backButtonTap:(id)sender
{
    [self.tableView reloadData];
    
    if ([_inmate hasChanges])
    {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"Save changes", @"Save changes")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                if ([self save])
                                                                {
                                                                    [self.navigationController popViewControllerAnimated:YES];
                                                                }
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Rollback changes", @"Rollback changes")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                if ([self rollback])
                                                                {
                                                                    [self.navigationController popViewControllerAnimated:YES];
                                                                }
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
        [alertController showWithSender:sender controller:self animated:YES completion:NULL];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)doneTap:(id)sender
{
    [super doneTap:sender];
    
    [self.tableView reloadData];
    
    if ([self save])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (BOOL)save
{
    [self.tableView reloadData];
    
    if ([self validateAll] == NO)
    {
        return NO;
    }
    
    JMCDRepository *repository = [JMCDRepository newRepository];
    
    if (![repository save])
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)rollback
{
    if (_inmate != nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        [repository rollback];
        
        _inmate = nil;
    }
    
    return YES;
}


#pragma mark - Validate

- (NSArray *)wrongField
{
    NSMutableArray *wrongFields = [NSMutableArray new];
    
    if (_inmate == nil)
    {
        return wrongFields;
    }
    
    if (_inmate.firstName.length == 0)
    {
        [wrongFields addObject:kFirstName];
    }
    
    if (_inmate.lastName.length == 0)
    {
        [wrongFields addObject:kLastName];
    }
    
    if (_inmate.identifier.length == 0)
    {
        [wrongFields addObject:kInmateID];
    }
    
    if (_inmate.bed.length == 0)
    {
        _inmate.bed = @" ";
        ///[wrongFields addObject:kBed];
    }
    
    if (_inmate.institution == nil)
    {
        [wrongFields addObject:kInstitution];
    }
    
    return wrongFields;
}


- (BOOL)validateAll
{
    NSArray *wrongFields = [self wrongField];
    
    if ([wrongFields containsObject:kInstitution])
    {
        [self errorAlertWithText:NSLocalizedString(@"Select institution.", @"Select institution.")];
        
        self.institutionButton.layer.borderColor = [UIColor redColor].CGColor;
    }
    else {
        self.institutionButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
        
        if (wrongFields.count > 0)
        {
            [self errorAlertWithText:NSLocalizedString(@"Сorrect the fields marked in red.", @"Сorrect the fields marked in red.")];
        }
    }
    
    
    [self.tableView reloadData];
    
    return wrongFields.count == 0;
}


- (BOOL)validateRow:(NSInteger)row
{
    NSArray *wrongFields = [self wrongField];
    
    return ![wrongFields containsObject:[self.headers objectAtIndex:row]];
}


#pragma mark -

- (void)setAvatarImage:(UIImage *)avatarImage
{
    [super setAvatarImage:avatarImage];
    
    self.inmate.avatar = avatarImage;
}

- (void)setInstitution:(JMInstitution *)institution
{
    if ([institution isKindOfClass:JMInstitution.class])
    {
        self.institutionName.text = institution.name;
        self.institutionNote.text = institution.note;
        
        self.institutionButton.backgroundColor = [UIColor clearColor];
        [self.institutionButton setImage:nil forState:UIControlStateNormal];
        self.institutionButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
}


- (void)selectedInstitution:(id)aInstitution
{
    if ([aInstitution isKindOfClass:JMInstitution.class])
    {
        JMInstitution *institution = (JMInstitution *)aInstitution;
        
        self.inmate.institution = institution;
        [self setInstitution:self.inmate.institution];
    }
}


- (void)header:(NSString *)header didEndEditing:(NSString *)string
{
    NSString *text = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([header isEqualToString:kFirstName] && (text.length > 0))
    {
        self.inmate.firstName = text;
    }
    else if ([header isEqualToString:kLastName] && (text.length > 0)) {
        self.inmate.lastName = text;
    }
    else if ([header isEqualToString:kInmateID] && (text.length > 0)) {
        self.inmate.identifier = text;
    }
    else if ([header isEqualToString:kBed] && (text.length > 0)) {
        self.inmate.bed = text;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JMTextFieldCell *cell = (JMTextFieldCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    NSString *header = [self.headers objectAtIndex:indexPath.section];
    
    cell.field.keyboardType = UIKeyboardTypeDefault;
    
    if ([header isEqualToString:kFirstName])
    {
        cell.field.text = _inmate.firstName;
    }
    else if ([header isEqualToString:kLastName]) {
        cell.field.text = _inmate.lastName;
    }
    else if ([header isEqualToString:kInmateID]) {
        cell.field.text = _inmate.identifier;
        cell.field.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    else if ([header isEqualToString:kBed]) {
        cell.field.text = _inmate.bed;
        cell.field.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    
    return cell;
}


@end