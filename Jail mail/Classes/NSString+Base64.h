//
//  NSString+Base64.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 26/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)

+ (NSString *)base64forData:(NSData *)theData;

@end