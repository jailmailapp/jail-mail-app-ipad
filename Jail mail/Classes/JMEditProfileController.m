//
//  JMEditProfileController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 07/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEditProfileController.h"

#import "AXTableKeyboardChain.h"
#import "JMTextFieldCell.h"
#import "UIImageView+Avatar.h"
#import "PSTAlertController.h"

#import "QBAssetsViewController.h"
#import "TOCropViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>

@interface JMEditProfileController ()<
UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
QBImagePickerControllerDelegate,
TOCropViewControllerDelegate> {
    BOOL isDoneTap;
    QBAssetsViewController *controller;
}

@end

@implementation JMEditProfileController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isDoneTap = NO;
    
    [self observeKeyboardForTable:self.tableView];
}


- (BOOL)validateRow:(NSInteger)row
{
    return YES;
}


- (void)header:(NSString *)header changeString:(NSString *)string
{
    NSLog(@"%@ - %@", header, string);
}


- (void)header:(NSString *)header didEndEditing:(NSString *)string
{
    NSLog(@"%@ - %@", header, string);
}


- (void)errorAlertWithText:(NSString *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                    message:error
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    [alert show];
}


- (void)setAvatarImage:(UIImage *)avatarImage
{
    if (avatarImage != nil)
    {
        [self.avatarView setAvatarImage:avatarImage];
    }
    else {
        [self.avatarView setAvatarImage:[UIImage imageNamed:@"icon_add_personal"]];
    }
    
    [self.avatarView circle];
    [self.avatarView whiteBorder];
}


- (IBAction)doneTap:(id)sender
{
    isDoneTap = YES;
}

- (IBAction)changeAvatarTap:(id)sender
{
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate                    = self;
    imagePickerController.allowsMultipleSelection     = NO;
    imagePickerController.maximumNumberOfSelection    = 1;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    imagePickerController.filterType                  = QBImagePickerControllerFilterTypePhotos;
    
    [self.view.window.rootViewController presentViewController:imagePickerController animated:YES completion:NULL];
}

- (void)qb_imagePickerController:(QBImagePickerController *)pickerController controller:(QBAssetsViewController *)aController selectedAsset:(ALAsset *)asset
{
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    CGImageRef iref            = [rep fullResolutionImage];
    UIImage *image             = [UIImage imageWithCGImage:iref];
    
    controller = aController;
    
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    [pickerController presentViewController:cropViewController animated:YES completion:NULL];
}


- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    [imagePickerController dismissViewControllerAnimated:YES completion:NULL];
}


- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [imagePickerController dismissViewControllerAnimated:YES completion:NULL];
}


- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    NSLog(@"%@",image);
    
    [self setAvatarImage:image];
    
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        [controller dismissViewControllerAnimated:YES completion:NULL];
    }];
}


- (void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{

    [cropViewController dismissViewControllerAnimated:YES completion:^{
        //[controller dismissViewControllerAnimated:YES completion:NULL];
    }];
}


//- (IBAction)changeAvatarTap:(id)sender
//{
//    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
//    
//    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"From Library", @"From Library")
//                                                        style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
//                                                            [self presentImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
//                                                        }]];
//    
//    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Take Photo", @"Take Photo")
//                                                        style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
//                                                            [self presentImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
//                                                        }]];
//    
//    [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
//    
//    [alertController showWithSender:sender controller:self animated:YES completion:NULL];
//}
//- (void)presentImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType
//{
//    if ([UIImagePickerController isSourceTypeAvailable:sourceType])
//    {
//        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
//        imagePickerController.delegate      = self;
//        imagePickerController.allowsEditing = YES;
//        imagePickerController.sourceType    = sourceType;
//        [self presentViewController:imagePickerController animated:YES completion:nil];
//    }
//}
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
//    
//    [self setAvatarImage:editedImage];
//    
//    [picker dismissViewControllerAnimated:YES completion:^{}];
//}

- (void)valudateCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([self validateRow:indexPath.section] || isDoneTap == NO)
    {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.000 green:0.149 blue:0.294 alpha:1.000];
    }
    else {
        cell.contentView.backgroundColor = [UIColor redColor];
    }
}


#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (self.headers.count > section)?[self.headers objectAtIndex:section] : @"";
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = [UIColor colorWithRed:0.298 green:0.345 blue:0.427 alpha:1.000];
    header.textLabel.font      = [UIFont systemFontOfSize:14];
    header.textLabel.frame     = header.bounds;
    header.textLabel.text      = header.textLabel.text.capitalizedString;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.headers.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellIdentifier = @"CellIdentifier";
    JMTextFieldCell *cell            = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if (!cell)
    {
        [tableView registerClass:[JMTextFieldCell class] forCellReuseIdentifier:kCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    }
    
    cell.field.delegate    = self;
    cell.field.attachments = indexPath;
    
    if ([self validateRow:indexPath.section] || (isDoneTap == NO))
    {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.000 green:0.149 blue:0.294 alpha:1.000];
    }
    else {
        cell.contentView.backgroundColor = [UIColor redColor];
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - UITableView and UITextField delegate

- (void)observeKeyboardForTable:(UITableView *)tableView
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        id _obj = [note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect _keyboardFrame = CGRectNull;
        
        if ([_obj respondsToSelector:@selector(getValue:)])
        {
            [_obj getValue:&_keyboardFrame];
        }
        
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [tableView setContentInset:UIEdgeInsetsMake (0.f, 0.f, _keyboardFrame.size.height+10, 0.f)];
        } completion:nil];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [tableView setContentInset:UIEdgeInsetsZero];
        } completion:nil];
    }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (AXTableKeyboardChainGetNextField(self.tableView, textField))
    {
        [textField setReturnKeyType:UIReturnKeyNext];
    }
    else {
        [textField setReturnKeyType:UIReturnKeyDefault];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    AXTableKeyboardChainGoToNext(self.tableView, textField);
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)aString
{
    if ([textField isKindOfClass:JMTextField.class])
    {
        JMTextField *field     = (JMTextField *)textField;
        NSIndexPath *indexPath = field.attachments;
        
        NSString *header = (self.headers.count > indexPath.section)?[self.headers objectAtIndex:indexPath.section] : @"";
        NSString *string = [textField.text stringByReplacingCharactersInRange:range withString:aString];
        
        [self header:header changeString:string];
    }
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isKindOfClass:JMTextField.class])
    {
        JMTextField *field     = (JMTextField *)textField;
        NSIndexPath *indexPath = field.attachments;
        
        NSString *header = (self.headers.count > indexPath.section)?[self.headers objectAtIndex:indexPath.section] : @"";
        NSString *string = textField.text;
        
        [self header:header didEndEditing:string];
        
        [self valudateCellForRowAtIndexPath:indexPath];
    }
}


@end