//
//  UITextView+DoneButton.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 23/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Toolbar)

- (void)keyboardToolbarSetup;

@end
