//
//  InboxesController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 31/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInboxesController.h"

#import "JMCDRepository+Letter.h"
#import "JMInboxesCell.h"
#import "JMInmate.h"
#import "JMUser.h"
#import "UIImageView+Avatar.h"
#import "UIViewController+SideMenu.h"

#import "JMComposeController.h"
#import "JMCoreDataManager.h"
#import "JMPostcardController.h"

@interface JMInboxesController ()<UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate> {
    NSArray *_letters;
    NSArray *_searchedLetters;
    NSDateFormatter *_dateFormatter;
}
@property (strong, nonatomic) IBOutlet UITableView *letterTable;
@property (strong, nonatomic) IBOutlet UIView *menuView;

@end

@implementation JMInboxesController

+ (NSString *)storyboardID
{
    return @"idInboxesController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 44)];
    [logoView setImage:[UIImage imageNamed:@"logo_Jail_Mail"]];
    [logoView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = logoView;

    [self addMenuBarButton];
    [self searchBarHide:YES];

    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"MM/dd/YY"];

    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                                                  target:self
                                                                                  action:@selector(showSearchTap)];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_Add"]
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(optionsMenuTap)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:addButton, searchButton, nil];

    self.letterTable.allowsMultipleSelectionDuringEditing = NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self optionsMenuHide:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self reloadLetters];

    [UIView animateWithDuration:0.4 animations:^{
        self.letterTable.contentOffset = CGPointMake (0, self.searchDisplayController.searchBar.frame.size.height);
    } completion:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadLetters)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:[[JMCoreDataManager sharedManager] context]];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)reloadLetters
{
    _letters = [[JMCDRepository newRepository] allLetters];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:NO];
    _letters = [_letters sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

    if (_letters.count == 0)
    {
        [self plaseholderImageHide:NO];
    }
    else {
        [self plaseholderImageHide:YES];
    }

    [self.letterTable reloadData];
}


- (void)plaseholderImageHide:(BOOL)hide
{
    if (hide)
    {
        self.letterTable.backgroundView = nil;
    }
    else {
        UIView *backgroundView = [[UIView alloc] init];
        CGFloat size           = self.view.bounds.size.width/2;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(size, 5, size, size)];
        [imageView setImage:[UIImage imageNamed:@"Baner"]];
        [backgroundView addSubview:imageView];
        self.letterTable.backgroundView = backgroundView;
    }
}


- (void)searchBarHide:(BOOL)hide
{
    if (hide)
    {
        [self.searchDisplayController setActive:NO animated:YES];
    }
    else {
        [self.searchDisplayController setActive:YES animated:YES];
    }
}


- (void)showSearchTap
{
    [self searchBarHide:NO];
}


- (void)optionsMenuTap
{
    [self optionsMenuHide:(self.menuView.alpha == 1)];
}


- (void)optionsMenuHide:(BOOL)hide
{
    if (hide)
    {
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.menuView.alpha = 0;
        } completion:nil];
    }
    else {
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.menuView.alpha = 1;
        } completion:nil];
    }
}


- (BOOL)isVisibleController:(UIViewController *)controller
{
    return [self isViewLoaded] && self.view.window;
}


- (void)pushViewController:(UIViewController *)controller
{
    [self optionsMenuHide:YES];

    if ([self isVisibleController:self.detail])
    {
        [self.detail.navigationController popToRootViewControllerAnimated:NO];
        [self.detail.navigationController pushViewController:controller animated:NO];
    }

    NSLog(@"%@", self.detail.navigationController.viewControllers);
}


#pragma mark Compose

- (IBAction)goToComposeController
{
    JMComposeController *compose = [JMComposeController controller];
    [self pushViewController:compose];
}


- (void)goToComposeControllerWithLetter:(JMLetter *)letter
{
    if ([self.detail.navigationController.topViewController isKindOfClass:JMComposeController.class])
    {
        JMComposeController *compose = (JMComposeController *)self.detail.navigationController.topViewController;
        [compose editLetter:letter];
    }
    else {
        JMComposeController *compose = [JMComposeController controller];
        [compose editLetter:letter];
        [self pushViewController:compose];
    }
}


#pragma mark PostCard

- (IBAction)goToPostCardController
{
    JMPostcardController *postCard = [JMPostcardController controller];
    [self pushViewController:postCard];
}


- (void)goToPostCardControllerWithLetter:(JMLetter *)letter
{
    JMPostcardController *postCard = [JMPostcardController controller];
    [postCard setLetter:letter];
    [self pushViewController:postCard];
}


#pragma mark Photo

- (IBAction)goToPhotoController
{
    JMComposeController *compose = [JMComposeController controller];
    [compose setPhotoMode:YES];
    [self pushViewController:compose];
}


- (void)goToPhotoControllerWithLetter:(JMLetter *)letter
{
    JMComposeController *compose = [JMComposeController controller];
    [compose setPhotoMode:YES];
    [compose editLetter:letter];
    [self pushViewController:compose];
}


#pragma mark - UISearchDisplayDelegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
            objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];

    return YES;
}


- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"message contains[c] %@", searchText];
    _searchedLetters = [_letters filteredArrayUsingPredicate:resultPredicate];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [_searchedLetters count];
    }
    else {
        return [_letters count];
    }
}


static NSString *kCellIdentifier    = @"CellIdentifier";
static NSString *kDefCellIdentifier = @"DefCellIdentifier";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = nil;

    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        identifier = kDefCellIdentifier;
    }
    else {
        identifier = kCellIdentifier;
    }

    JMInboxesCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    if (!cell)
    {
        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            cell = (JMInboxesCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kDefCellIdentifier];
        }
        else {
            [tableView registerClass:[JMInboxesCell class] forCellReuseIdentifier:kCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        }
    }

    JMLetter *letter = nil;

    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        letter                    = [_searchedLetters objectAtIndex:indexPath.row];
        cell.textLabel.text       = letter.message;
        cell.textLabel.font       = [UIFont systemFontOfSize:13];
        cell.detailTextLabel.text = [_dateFormatter stringFromDate:letter.createdAt];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:8];
    }
    else {
        letter = [_letters objectAtIndex:indexPath.row];

        if (letter.recipient.avatar != nil)
        {
            [cell.avatar setImage:letter.recipient.avatar];
        }
        else {
            [cell.avatar setImage:[UIImage imageNamed:@"person_stroke"]];
        }

        [cell.avatar circle];
        [cell.avatar whiteBorder];

        if (letter.type == JMLetterTypePhoto)
        {
            cell.messageLabel.text      = NSLocalizedString(@"Jail Mail Photos Order", @"Jail Mail Photos Order");
            cell.messageLabel.textColor = [UIColor darkGrayColor];
        }
        else {
            cell.messageLabel.text = letter.message;

            if (cell.messageLabel.text.length == 0)
            {
                cell.messageLabel.text      = NSLocalizedString(@"Compose a letter...", @"Compose a letter...");
                cell.messageLabel.textColor = [UIColor grayColor];
            }
            else {
                cell.messageLabel.textColor = [UIColor blackColor];
            }
        }

        cell.dateLabel.text = [_dateFormatter stringFromDate:letter.createdAt];

        if (letter.state == JMLetterStateDraft)
        {
            cell.draftLabel.text      = @"Draft";
            cell.draftLabel.textColor = [UIColor redColor];
        }
        else if ((letter.state == JMLetterStateInProcess) || (letter.state == JMLetterStateError)) {
            cell.draftLabel.text      = @"In process";
            cell.draftLabel.textColor = [UIColor darkGrayColor];
        }
        else if (letter.state == JMLetterStateSubmitted) {
            cell.draftLabel.text      = @"Submitted";
            cell.draftLabel.textColor = [UIColor darkGrayColor];
        }

        if (letter.type == JMLetterTypeMail)
        {
            [cell.type setImage:[UIImage imageNamed:@"message_envelop"]];
        }
        else if (letter.type == JMLetterTypePostcard) {
            [cell.type setImage:[UIImage imageNamed:@"message_postcard"]];
        }
        else if (letter.type == JMLetterTypePhoto) {
            [cell.type setImage:[UIImage imageNamed:@"message_photo"]];
        }
    }

    if (indexPath.row % 2)
    {
        cell.backgroundColor = [UIColor colorWithRed:0.937 green:0.953 blue:0.969 alpha:1.000];
    }
    else {
        cell.backgroundColor = [UIColor colorWithRed:0.965 green:0.969 blue:0.988 alpha:1.000];
    }

    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        JMLetter *letter = nil;

        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            letter = [_searchedLetters objectAtIndex:indexPath.row];
        }
        else {
            letter = [_letters objectAtIndex:indexPath.row];
        }

        JMCDRepository *repository = [JMCDRepository newRepository];
        [repository deleteEntity:letter];
        [repository save];

        [self reloadLetters];

        [self.detail.navigationController popToRootViewControllerAnimated:NO];
    }
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return 34;
    }

    return 83;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    JMLetter *letter = nil;

    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        letter = [_searchedLetters objectAtIndex:indexPath.row];
    }
    else {
        letter = [_letters objectAtIndex:indexPath.row];
    }

    if (letter.type == JMLetterTypeMail)
    {
        [self goToComposeControllerWithLetter:letter];
    }
    else if (letter.type == JMLetterTypePostcard) {
        [self goToPostCardControllerWithLetter:letter];
    }
    else if (letter.type == JMLetterTypePhoto) {
        [self goToPhotoControllerWithLetter:letter];
    }
}


@end