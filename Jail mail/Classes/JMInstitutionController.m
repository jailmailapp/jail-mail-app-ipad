//
//  JMInstitutionController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMInstitutionController.h"

#import "JMCDRepository+Institution.h"
#import "JMInstitution.h"
#import "JMInstitutionEditController.h"
#import "UIViewController+EditButtons.h"
#import "UIViewController+ImageBackButton.h"
#import "JMProfileCell.h"
#import "PSTAlertController.h"

@interface JMInstitutionController ()<EditableCellDelegate> {
    NSArray *_institutions;
}

@property (strong, nonatomic) IBOutlet UITableView *institutionsTable;

@end

@implementation JMInstitutionController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Institutions", @"Institutions");
    self.prompt.text = NSLocalizedString(@"Choose saved institution", @"Choose saved institution");
    [self.createButton setTitle:NSLocalizedString(@"Add new institution", @"Add new institution") forState:UIControlStateNormal];
    
    [self setCustomBackButton];
    [self updateEditButtonsWithEditing:NO];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self reloadItems];
}


- (void)setEditing:(BOOL)editing
{
    [super setEditing:editing];
    
    [self.institutionsTable reloadData];
}


- (void)reloadItems
{
    NSArray *items = [[JMCDRepository newRepository] allInstitution];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    self.items = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    [self.galleryCollection reloadData];
    
    [super reloadItems];
}


- (IBAction)createTap:(id)sender
{
    JMInstitutionEditController *institutionController = [JMInstitutionEditController controller];
    [self.navigationController pushViewController:institutionController animated:YES];
}


- (void)deleteCellAttachments:(id)attachments
{
    PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        JMInstitution *institution = (JMInstitution *)attachments;
        
        if (institution && [institution isKindOfClass:JMInstitution.class])
        {
            NSLog (@"Delete");
            
            JMCDRepository *repository = [JMCDRepository newRepository];
            [repository deleteEntity:institution];
            [repository save];
            
            [self reloadItems];
        }
    }]];
    
    [alertController addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
    [alertController showWithSender:nil controller:self animated:YES completion:NULL];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JMProfileCell *photoCell = (JMProfileCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JMInstitution *institution = [self.items objectAtIndex:indexPath.row];
    [photoCell.profileView setAvatar:[UIImage imageNamed:@"institution"]];
    [photoCell.profileView setName:institution.name];
    [photoCell setAttachments:institution];
    [photoCell setDelegate:self];
    photoCell.profileView.circle = NO;
    
    return photoCell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
     JMInstitution *institution = [self.items objectAtIndex:indexPath.row];
    
    if (self.editing)
    {
        JMInstitutionEditController *institutionController = [JMInstitutionEditController controller];
        [institutionController editInstitution:institution];
        [self.navigationController pushViewController:institutionController animated:YES];
    }
    else {
        if (_delegate != nil)
        {
            [_delegate selectedInstitution:institution];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


@end