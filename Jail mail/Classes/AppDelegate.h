//
//  AppDelegate.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 25/03/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMRootViewController.h"
#import "JMNavigationController.h"

#define JMAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JMRootViewController *rootController;
@property (strong, nonatomic) JMNavigationController *detailNavigation;


@end

