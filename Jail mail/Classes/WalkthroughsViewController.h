//
//  TSWalkthroughsViewController.h
//
//
//  Created by Alexander Shvetsov on 24/Sep/2015.
//  Copyright © 2015 Yanpix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalkthroughsViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@end
