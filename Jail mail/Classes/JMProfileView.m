//
//  JMProfileView.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMProfileView.h"

#import "UIImageView+Avatar.h"

@interface JMProfileView () {
}

@property (nonatomic, retain) UIImageView *avatarView;
@property (nonatomic, retain) UIImageView *separatorView;
@property (nonatomic, retain) UILabel *nameLabel;

@end

@implementation JMProfileView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.avatarView             = [[UIImageView alloc] init];
        self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_avatarView];
        
        self.separatorView             = [[UIImageView alloc] init];
        self.separatorView.image       = [UIImage imageNamed:@"decorative_line_gray"];
        self.separatorView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_separatorView];
        
        self.nameLabel                           = [[UILabel alloc] init];
        self.nameLabel.tintColor                 = [UIColor colorWithRed:0.290 green:0.306 blue:0.333 alpha:1.000];
        self.nameLabel.textAlignment             = NSTextAlignmentCenter;
        self.nameLabel.numberOfLines             = 0;
        self.nameLabel.minimumScaleFactor        = 0.1;
        self.nameLabel.adjustsFontSizeToFitWidth = YES;
        [self addSubview:_nameLabel];
        
        self.circle = YES;
    }
    
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.circle)
    {
        [self.avatarView circle];
    }
}


- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    CGFloat heightAvatar    = frame.size.height/100*55;
    CGFloat heightSeparator = frame.size.height/100*6;
    CGFloat heightName      = frame.size.height/100*25;
    CGFloat padding         = frame.size.height/100*7;
    
    CGFloat value    = 0;
    CGFloat paddingX = 0;
    CGFloat paddingY = 0;
    
    if (frame.size.width >= heightAvatar)
    {
        paddingX = (frame.size.width - heightAvatar)/2;
        value    = frame.size.width - paddingX*2;
    }
    else {
        paddingY = (heightAvatar - frame.size.width)/2;
        value    = heightAvatar - paddingY*2;
    }
    
    self.avatarView.frame          = CGRectMake(paddingX, paddingY, value, value);
    self.separatorView.frame       = CGRectMake(0, paddingY+value+padding, frame.size.width, heightSeparator);
    self.nameLabel.frame           = CGRectMake(0, paddingY+value+heightSeparator+padding, frame.size.width, heightName);
}


- (void)setAvatar:(UIImage *)avatar
{
    [self.avatarView setAvatarImage:avatar];
}


- (void)setName:(NSString *)name
{
    self.nameLabel.text = (name != nil)?name : @"";
}


@end