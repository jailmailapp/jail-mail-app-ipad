//
//  JMLetterRequest.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JMPackage;

@interface JMLetterRequest : NSObject

+ (void)sendPackage:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;

+ (void)updateCompletedPackage:(JMPackage *)package overrideCompletedStatusToUploaded:(BOOL)isOverride success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;


@end
