//
//  JMInstitutionEditController.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 21/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMEditProfileController.h"

#import "UIViewController+CustomInit.h"

@interface JMInstitutionEditController : JMEditProfileController <StoryboardProtocol>

- (void)editInstitution:(id)institution;

@end
