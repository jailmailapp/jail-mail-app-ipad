//
//  JMPostCardController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 13/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMPostcardController.h"

#import "JMAttachments.h"
#import "JMCDEntityFactory+Attachments.h"
#import "JMCDEntityFactory+Letter.h"
#import "JMCDRepository.h"
#import "JMInmate.h"
#import "JMInstitution.h"
#import "JMLetter.h"
#import "JMRecipientsDelegate.h"
#import "JMSendersDelegate.h"
#import "JMUser.h"
#import "UIImageView+Avatar.h"
#import "UITextView+Placeholder.h"
#import "UIViewController+ImageBackButton.h"

#import "JMCardsController.h"
#import "JMRecipientsController.h"
#import "JMSendersController.h"
#import "PSTAlertController.h"
#import "QBImagePickerController.h"
#import "TOCropViewController.h"
#import "QBAssetsViewController.h"

static NSString *const kMessage     = @"Message";
static NSString *const kSender      = @"Sender";
static NSString *const kRecipient   = @"Recipient";
static NSString *const kAttachments = @"Attachments";

static const NSInteger kMaxMessage = 180;

@interface JMPostcardController ()
<JMSendersDelegate,
JMRecipientsDelegate,
UITextViewDelegate,
UIActionSheetDelegate,
TOCropViewControllerDelegate,
QBImagePickerControllerDelegate> {
    QBAssetsViewController *_controller;
}

@property (strong, nonatomic) IBOutlet UIImageView *senderAvatar;
@property (strong, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *senderButton;
@property (strong, nonatomic) IBOutlet UIButton *recipientButton;
@property (strong, nonatomic) IBOutlet UIButton *messageButton;
@property (strong, nonatomic) IBOutlet UIButton *imageButton;
@property (strong, nonatomic) IBOutlet UIImageView *recipientAvatar;
@property (strong, nonatomic) IBOutlet UILabel *recipientNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *textImageView;
@property (strong, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) IBOutlet UIImageView *textMarkImageView;
@property (strong, nonatomic) IBOutlet UIImageView *photoMarkImageView;
@property (strong, nonatomic) JMLetter *letter;

@property (strong, nonatomic) IBOutlet UIView *messagePopupView;
@property (strong, nonatomic) IBOutlet UITextView *messageView;
@property (weak, nonatomic) IBOutlet UILabel *messageTitleLabel;

@end

@implementation JMPostcardController

+ (NSString *)storyboardID
{
    return @"idPostcardController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Postcard", @"Postcard");
    
    [self setCustomBackButton];
    
    [self updateNavigationButtons];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateUI];
}


- (void)updateNavigationButtons
{
    UIImage *image = nil;
    
    if (_letter.state == JMLetterStateDraft)
    {
        image = [UIImage imageNamed:@"icon_send"];
    }
    else if ((_letter.state == JMLetterStateInProcess) || (_letter.state == JMLetterStateError)) {
        image = [UIImage imageNamed:@"icon_send_continue"];
    }
    else if (_letter.state == JMLetterStateSubmitted) {
        image = [UIImage imageNamed:@"icon_locked"];
    }
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(sendLetterTap:)];
    self.navigationItem.rightBarButtonItem = sendButton;
}


- (JMLetter *)letter
{
    if (_letter == nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        _letter = [[JMCDEntityFactory newFactory] newLetter];
        [repository addEntity:_letter];
    }
    
    return _letter;
}


- (NSArray *)wrongField
{
    NSMutableArray *wrongFields = [[NSMutableArray alloc] init];
    
    if ((_letter.attachments == nil) || (_letter.attachments.count == 0))
    {
        [wrongFields addObject:kAttachments];
    }
    
    if ((_letter.message == nil) || (_letter.message.length == 0))
    {
        [wrongFields addObject:kMessage];
    }
    
    if (_letter.sender == nil)
    {
        [wrongFields addObject:kSender];
    }
    
    if (_letter.recipient == nil)
    {
        [wrongFields addObject:kRecipient];
    }
    
    return wrongFields;
}


- (BOOL)validateLetter
{
    NSArray *wrongFields = [self wrongField];
    
    if ([wrongFields containsObject:kSender])
    {
        [self errorAlertWithText:NSLocalizedString(@"Select sender.", @"Select sender.")];
    }
    else if ([wrongFields containsObject:kRecipient]) {
        [self errorAlertWithText:NSLocalizedString(@"Select contact.", @"Select contact.")];
    }
    else if ([wrongFields containsObject:kMessage]) {
        [self errorAlertWithText:NSLocalizedString(@"Enter a message.", @"Enter a message.")];
    }
    else if ([wrongFields containsObject:kAttachments]) {
        [self errorAlertWithText:NSLocalizedString(@"Enter a photo.", @"Enter a photo.")];
    }
    
    return wrongFields.count == 0;
}


- (void)errorAlertWithText:(NSString *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                    message:error
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    [alert show];
}


- (void)blockUI:(BOOL)block
{
    self.senderButton.enabled    = !block;
    self.recipientButton.enabled = !block;
    self.messageButton.enabled   = !block;
    self.imageButton.enabled     = !block;
    self.editing                 = !block;
}


- (void)updateUI
{
    JMUser *user = _letter.sender;
    
    if (user != nil)
    {
        [self.senderAvatar setAvatarImage:user.avatar];
        [self.senderNameLabel setText:user.fullName];
        [self.senderButton setTitle:@"" forState:UIControlStateNormal];
        [self.senderAvatar blueBorder];
    }
    else {
        [self.senderAvatar setImage:[UIImage imageNamed:@"icon_Add_receiver"]];
        [self.senderNameLabel setText:@"Add sender"];
        [self.senderAvatar whiteBorder];
    }
    
    JMInmate *recipient = _letter.recipient;
    
    if (recipient != nil)
    {
        [self.recipientAvatar setAvatarImage:recipient.avatar];
        [self.recipientNameLabel setText:recipient.fullName];
        [self.recipientButton setTitle:@"" forState:UIControlStateNormal];
        [self.recipientAvatar blueBorder];
    }
    else {
        [self.recipientAvatar setImage:[UIImage imageNamed:@"icon_Add_receiver"]];
        [self.recipientNameLabel setText:@"Add contact"];
        [self.recipientAvatar whiteBorder];
    }
    
    [self.senderAvatar circle];
    [self.recipientAvatar circle];
    
    if (_letter.message)
    {
        self.textImageView.image     = [UIImage imageNamed:@"post_card_full"];
        self.textMarkImageView.image = [UIImage imageNamed:@"check_blue"];
        
        self.messageView.text = _letter.message;
        self.messageTitleLabel.text = [NSString stringWithFormat:@"%lu/%ld", (unsigned long)self.messageView.text.length, (long)kMaxMessage];
    }
    
    if ((_letter.attachments != nil) && (_letter.attachments.count > 0))
    {
        self.photoImageView.image     = [(JMAttachments *)[_letter.attachments anyObject] image];
        self.photoMarkImageView.image = [UIImage imageNamed:@"check_blue"];
    }
    
    if (_letter.state == JMLetterStateDraft)
    {
        [self blockUI:NO];
    }
    else {
        [self blockUI:YES];
    }
    
    [self updateNavigationButtons];
}


- (BOOL)saveLetter
{
    self.letter.type = JMLetterTypePostcard;
    
    JMCDRepository *repository = [JMCDRepository newRepository];
    
    if (![repository save])
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)deleteLetter
{
    if (_letter != nil)
    {
        JMCDRepository *repository = [JMCDRepository newRepository];
        [repository deleteEntity:_letter];
        
        if (![repository save])
        {
            return NO;
        }
        
        _letter = nil;
    }
    
    return YES;
}


#pragma mark - IBAction

- (IBAction)closeMessagePopupTap:(id)sender
{
    self.messageView.text = self.letter.message;
    
    [self.messageView resignFirstResponder];
    self.messagePopupView.hidden = YES;
}


- (IBAction)doneMessagePopupTap:(id)sender
{
    if ((self.messageView.text.length > 0) && (self.messageView.text.length <= kMaxMessage))
    {
        self.letter.message = self.messageView.text;
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                        message:[NSString stringWithFormat:@"Wrong text. The text must not exceed %ld characters.", (long)kMaxMessage]
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
        [alert show];
    }

    
    [self.messageView resignFirstResponder];
    self.messagePopupView.hidden = YES;
    
    [self updateUI];
}


- (IBAction)selectSenderTap:(id)sender
{
    JMSendersController *senderController = [JMSendersController controller];
    senderController.delegate = self;
    [self.navigationController pushViewController:senderController animated:YES];
}


- (IBAction)selectRecipientTap:(id)sender
{
    JMRecipientsController *recipientController = [JMRecipientsController controller];
    recipientController.delegate = self;
    [self.navigationController pushViewController:recipientController animated:YES];
}


- (IBAction)messageTap:(id)sender
{
    self.messagePopupView.hidden = NO;
    
    self.messagePopupView.layer.borderWidth   = 1.0f;
    self.messagePopupView.layer.borderColor   = [UIColor grayColor].CGColor;
    self.messagePopupView.layer.masksToBounds = YES;
    self.messagePopupView.layer.cornerRadius  = 10;
    
    self.messageView.placeholder = NSLocalizedString(@"Compose a postcard...", @"Compose a postcard...");
    [self.messageView becomeFirstResponder];
}


- (IBAction)photoTap:(id)sender
{
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate                    = self;
    imagePickerController.allowsMultipleSelection     = YES;
    imagePickerController.maximumNumberOfSelection    = 1;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    imagePickerController.filterType                  = QBImagePickerControllerFilterTypePhotos;
    
    [self.view.window.rootViewController presentViewController:imagePickerController animated:YES completion:NULL];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self addPhoto:image];
    
    [cropViewController dismissViewControllerAnimated:NO completion:nil];
}


- (void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    [_controller cancelLastSelectedItemIndexPath];
    
    [cropViewController dismissViewControllerAnimated:NO completion:^{
        ;
    }];
}


- (void)qb_imagePickerController:(QBImagePickerController *)pickerController controller:(QBAssetsViewController *)aController selectedAsset:(ALAsset *)asset
{
    
    
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    CGImageRef iref            = [rep fullResolutionImage];
    UIImage *image             = [UIImage imageWithCGImage:iref];
    
    _controller = aController;
    
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    [pickerController presentViewController:cropViewController animated:YES completion:^{
        
    }];
}


- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    [imagePickerController dismissViewControllerAnimated:YES completion:^{
    }];
}


- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [imagePickerController dismissViewControllerAnimated:YES completion:NULL];
}


- (void)addPhoto:(UIImage *)image
{
    JMAttachments *attachments = [[JMCDEntityFactory newFactory] newAttachments];
    [attachments setImage:image];
    
    JMCDRepository *repository = [JMCDRepository newRepository];
    
    if (self.letter.attachments.count > 0)
    {
        [self.letter removeAttachments:self.letter.attachments];
    }
    
    [repository addEntity:attachments];
    [self.letter addAttachmentsObject:attachments];
    
    [self updateUI];
}


- (IBAction)sendLetterTap:(id)sender
{
    if (_letter.state == JMLetterStateDraft)
    {
        if ([self validateLetter])
        {
            [self saveLetter];
            
            JMCardsController *controller = [JMCardsController controller];
            [controller setLetter:_letter];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
    else if ((_letter.state == JMLetterStateInProcess) || (_letter.state == JMLetterStateError)) {
        JMCardsController *controller = [JMCardsController controller];
        [controller setLetter:_letter];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (_letter.state == JMLetterStateSubmitted) {
        PSTAlertController *alertController = [PSTAlertController alertControllerWithTitle:nil message:@"Upadete to Draft" preferredStyle:PSTAlertControllerStyleActionSheet];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString(@"YES", @"YES")
                                                            style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                                _letter.state = JMLetterStateDraft;
                                                                _letter.letterID = nil;
                                                                _letter.userID = nil;
                                                                _letter.sentAt = nil;
                                                                
                                                                for (JMAttachments *attachments in _letter.attachments)
                                                                {
                                                                    [attachments setState:JMAttachmentsStateNew];
                                                                }
                                                                
                                                                [self saveLetter];
                                                                
                                                                [self updateUI];
                                                            }]];
        
        [alertController addAction:[PSTAlertAction actionWithTitle:NSLocalizedString (@"Cancel", @"Cancel") style:PSTAlertActionStyleCancel handler:nil]];
        [alertController showWithSender:sender controller:self animated:YES completion:NULL];    }
}

- (void)backButtonTap:(id)sender
{
    if ([_letter hasChanges])
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:
                                      NSLocalizedString(@"Save Draft", @"Save Draft"),
                                      NSLocalizedString(@"Delete Draft", @"Delete Draft"), nil];
        [actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
    }
    else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.messageTitleLabel.text = [NSString stringWithFormat:@"%lu/%ld", (unsigned long)self.messageView.text.length, (long)kMaxMessage];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //    if (self.messageView.text.length > kMaxMessage)
    //    {
    //        return NO;
    //    }
    //
    NSLog(@"%@ %@", NSStringFromRange(range), text);
    
    // error thrown here saying code will never execute
    if ((self.messageView.text.length >= kMaxMessage)
        && (self.messageView.text.length <= range.location))
    {
        return NO;
    }
    
    return YES;
}


#pragma mark - UIActionSheetDelegate

#define SAVE_DRAFT_INDEX 0
#define DELETE_DRAFT_INDEX 1
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == SAVE_DRAFT_INDEX)
    {
        if ([self saveLetter])
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    else if (buttonIndex == DELETE_DRAFT_INDEX) {
        if ([self deleteLetter])
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}


#pragma mark - JMSenderControllerDelegate

- (void)selectedSender:(id)sender
{
    if ([sender isKindOfClass:JMUser.class])
    {
        self.letter.sender = sender;
    }
    
    [self updateUI];
}


#pragma mark - JMRecipientControllerDelegate

- (void)selectedRecipient:(id)sender
{
    if ([sender isKindOfClass:JMInmate.class])
    {
        self.letter.recipient = sender;
    }
    
    [self updateUI];
}


@end