//
//  JMFingerprintRequest.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 01/07/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JMPackage;

@interface JMPaymentRequest : NSObject

#pragma mark - Card

+ (void)creditCardPaymentPackage:(JMPackage *)package deviceID:(NSString *)deviceID success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;

#pragma mark - Authorize.net

+ (void)fingerprintWithAmount:(NSString *)totalAmount success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;

+ (void)verifyAuthorizeNetTransaction:(NSString *)transactionID package:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;

#pragma mark - PayPal

+ (void)verifyPayPalTransaction:(NSString *)transactionID package:(JMPackage *)package success:(void (^)(id response))success failure:(void (^)(NSError *error))failure;

@end
