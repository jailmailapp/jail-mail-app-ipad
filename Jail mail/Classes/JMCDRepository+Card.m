//
//  JMCDRepository+Card.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 27/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+Card.h"

@implementation JMCDRepository (Card)

- (NSArray *)allCard
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMCard entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}

- (NSArray *)allCardForUser:(JMUser *)user
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user == %@",user];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMCard entityName]];
    [request setPredicate:predicate];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}

@end
