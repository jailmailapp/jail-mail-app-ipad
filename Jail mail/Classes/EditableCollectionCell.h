//
//  EditableCollectionCell.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 06/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditableCellDelegate<NSObject>

@optional
- (void)deleteCellAttachments:(id)attachments;

@end

@interface EditableCollectionCell : UICollectionViewCell

@property (nonatomic, retain) UIButton *buttonEdit;
@property (nonatomic, retain) id attachments;
@property (nonatomic, assign) id<EditableCellDelegate> delegate;

- (void)setStyle:(UITableViewCellEditingStyle)editingStyle animated:(BOOL)animated;

@end