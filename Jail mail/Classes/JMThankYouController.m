//
//  JMThankYouController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 10/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMThankYouController.h"

#import "JMFeedbackController.h"

@interface JMThankYouController ()

@end

@implementation JMThankYouController

+ (NSString *)storyboardID
{
    return @"idThankYouController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Thank You", @"Thank You");
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_left"]
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(backTap:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.hidesBackButton   = YES;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(backTap:)];
    self.navigationItem.leftBarButtonItem = doneButton;
    self.navigationItem.hidesBackButton   = YES;
}


- (void)backTap:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)shareTap:(id)sender
{
    JMFeedbackController *feedbackController = [JMFeedbackController controller];
    [self.navigationController pushViewController:feedbackController animated:YES];
}


@end