//
//  JMCDRepository+Institution.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 22/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository+Institution.h"

#import "JMInstitution.h"

@implementation JMCDRepository (Institution)

- (NSArray *)allInstitution
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[JMInstitution entityName]];
    NSError *error          = nil;
    NSArray *result         = [self.manager.context executeFetchRequest:request error:&error];
    
    return result;
}

@end
