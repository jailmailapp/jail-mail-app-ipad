//
//  JMGalleryController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 05/05/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMGalleryController.h"

#import "JMProfileCell.h"
#import "UIViewController+EditButtons.h"
#import "UIViewController+ImageBackButton.h"

@interface JMGalleryController () {
    BOOL isFirstAppear;
}

@end

@implementation JMGalleryController

+ (NSString *)xibID
{
    return @"JMGalleryController";
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.galleryCollection registerClass:[JMProfileCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    
    [self updateEditButtonsWithEditing:NO];
    [self setCustomBackButton];
    
    isFirstAppear = YES;
}

- (void)setEditing:(BOOL)editing
{
    [super setEditing:editing];
    
    [self.galleryCollection reloadData];
}

- (void)reloadItems
{
    if (isFirstAppear && self.items.count == 0)
    {
        isFirstAppear = NO;
        [self createTap:nil];
    }
}

- (IBAction)createTap:(id)sender
{
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.items.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kCellIdentifier = @"CellIdentifier";
    
    JMProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if (self.editing) {
        [cell setStyle:UITableViewCellEditingStyleDelete animated:YES];
    } else {
        [cell setStyle:UITableViewCellEditingStyleNone animated:NO];
    }
    
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(floorf(collectionView.frame.size.width/4), floorf(collectionView.frame.size.height/2.5));
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, floorf(collectionView.frame.size.width/2/3), 0, floorf(collectionView.frame.size.width/2/3));
}


@end