//
//  JMCDRepository+State.h
//  Jail mail
//
//  Created by Alexander Shvetsov on 20/04/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMCDRepository.h"

#import "JMState.h"

@interface JMCDRepository (State)

- (NSArray *)allState;

@end
