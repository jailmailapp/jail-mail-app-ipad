//
//  PageContentViewController.h
//  Tibetan Singing Bowls
//
//  Created by Alexander Shvetsov on 24/Sep/2015.
//  Copyright © 2015 Yanpix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController

@property (nonatomic) NSUInteger pageIndex;
@property (nonatomic) NSString *titleText;
@property (nonatomic) NSString *imageFile;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageContentView;
@end