//
//  JMDashboardController.m
//  Jail mail
//
//  Created by Alexander Shvetsov on 26/06/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "JMDashboardController.h"

#import "JMComposeController.h"
#import "JMPostcardController.h"

@interface JMDashboardController ()

@end

@implementation JMDashboardController

+ (NSString *)storyboardID
{
    return @"idDashboardController";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Dashboard", @"Dashboard");
}

- (IBAction)createMailTap:(id)sender
{
    JMComposeController *compose = [JMComposeController controller];
    [self.navigationController pushViewController:compose animated:NO];
}


- (IBAction)createPostcardTap:(id)sender
{
    JMPostcardController *postcard = [JMPostcardController controller];
    [self.navigationController pushViewController:postcard animated:NO];
}


- (IBAction)createPhotoTap:(id)sender
{
    JMComposeController *compose = [JMComposeController controller];
    [compose setPhotoMode:YES];
    [self.navigationController pushViewController:compose animated:NO];
}


@end